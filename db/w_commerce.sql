-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 05, 2021 at 08:17 PM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `w_commerce`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `admin_id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `admin_image` varchar(200) NOT NULL,
  `soft_delete` tinyint(1) DEFAULT NULL,
  `access_label` tinyint(4) NOT NULL COMMENT 'access_label=1 super admin,  access_label =2 admin',
  `is_actvie` tinyint(1) DEFAULT '1' COMMENT '0-is draft, 1-is active',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`admin_id`, `name`, `email`, `password`, `phone`, `admin_image`, `soft_delete`, `access_label`, `is_actvie`, `created_at`, `modified_at`) VALUES
(1, 'Gm Abbas Uddin', 'gmabbas44@gmail.com', '12345', '', '', 0, 1, 1, '2020-07-03 17:03:52', '2020-07-03 17:03:52');

-- --------------------------------------------------------

--
-- Table structure for table `brand`
--

CREATE TABLE `brand` (
  `brand_id` int(11) NOT NULL,
  `brand_name` varchar(255) NOT NULL,
  `status` tinyint(4) NOT NULL COMMENT '0-is draft, 1-is active, 2-is delete',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `modified_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `brand`
--

INSERT INTO `brand` (`brand_id`, `brand_name`, `status`, `created_at`, `modified_at`) VALUES
(1, 'Calvin Klein', 0, '2021-01-30 17:35:31', '0000-00-00 00:00:00'),
(2, 'Diesel', 1, '2021-01-29 14:58:57', '0000-00-00 00:00:00'),
(3, 'Polo', 1, '2021-01-29 14:57:33', '0000-00-00 00:00:00'),
(4, 'Tommy Hilfiger', 1, '2020-07-22 05:37:10', '2020-06-15 13:39:33'),
(7, 'King', 0, '2021-01-30 17:36:44', '2021-01-29 14:08:25');

-- --------------------------------------------------------

--
-- Table structure for table `carts`
--

CREATE TABLE `carts` (
  `cart_id` int(11) NOT NULL,
  `sid` varchar(255) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `picture` varchar(255) DEFAULT NULL,
  `product_title` varchar(255) DEFAULT NULL,
  `qty` int(11) DEFAULT NULL,
  `unite_price` int(11) DEFAULT NULL,
  `total_price` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `carts`
--

INSERT INTO `carts` (`cart_id`, `sid`, `product_id`, `picture`, `product_title`, `qty`, `unite_price`, `total_price`, `created_at`) VALUES
(253, 'o7tdeunmhej8kan708bi43gj3t3mjlkj', 3, 'man-1.jpg', 'Canverse coat', 1, 1500, 1500, '2020-08-02 14:09:36'),
(254, 'o7tdeunmhej8kan708bi43gj3t3mjlkj', 1, 'product-1.jpg', 'Pure Pineapple', 2, 1100, 2200, '2020-08-02 14:09:36'),
(255, 'o7tdeunmhej8kan708bi43gj3t3mjlkj', 2, 'product-3.jpg', 'Guangzhou sweater', 2, 1000, 2000, '2020-08-02 14:17:15'),
(256, '0i75jsh9cis541sdfvmcnaj5ngir72fh', 4, 'man-2.jpg', 'Shoppers can view size', 2, 1000, 2000, '2020-08-02 14:17:53'),
(257, '0i75jsh9cis541sdfvmcnaj5ngir72fh', 3, 'man-1.jpg', 'Canverse coat', 1, 1500, 1500, '2020-08-02 14:17:58'),
(258, '0i75jsh9cis541sdfvmcnaj5ngir72fh', 1, 'product-1.jpg', 'Pure Pineapple', 2, 1100, 2200, '2020-08-02 14:24:00'),
(259, '4o0k7p8eo6cng2ufubc1g8sm37f3bs8j', 1, 'product-1.jpg', 'Pure Pineapple', 2, 1100, 2200, '2020-08-03 13:01:35'),
(260, 'n87rg1cmtgv3dtori48asq9pk5st7klp', 1, 'product-1.jpg', 'Pure Pineapple', 2, 1100, 2200, '2020-08-05 18:12:14'),
(261, 'n87rg1cmtgv3dtori48asq9pk5st7klp', 4, 'man-2.jpg', 'Shoppers can view size', 2, 1000, 2000, '2020-08-05 18:12:20'),
(262, 'im687ijjbjcpeiafmhfudq7jl316593h', 3, 'man-1.jpg', 'Canverse coat', 1, 1500, 1500, '2020-08-07 04:11:53'),
(263, '5r4ipgjh92b1540p3erbsfrgikgmmid9', 4, 'man-2.jpg', 'Shoppers can view size', 2, 1000, 2000, '2020-08-09 04:23:59'),
(264, 's841lvfp6lbrkbkr3jaendskfgoglvgk', 4, 'man-2.jpg', 'Shoppers can view size', 1, 1000, 1000, '2020-08-09 05:12:31'),
(265, 'p8jqjg3n0t64nrvvrqssfubn92drjf5e', 2, 'product-3.jpg', 'Guangzhou sweater', 2, 1000, 2000, '2020-08-09 05:16:57'),
(266, '8ekos5kpsn8r9h910n1ig4c01np9jv7t', 4, 'man-2.jpg', 'Shoppers can view size', 1, 1000, 1000, '2020-08-09 05:26:03'),
(267, 'fvi69n7omkefl95h6ap56s9ock11mfic', 1, 'product-1.jpg', 'Pure Pineapple', 2, 1100, 2200, '2020-08-10 11:14:43'),
(268, 'fvi69n7omkefl95h6ap56s9ock11mfic', 2, 'product-3.jpg', 'Guangzhou sweater', 2, 1000, 2000, '2020-08-10 11:14:50'),
(269, '69d2gmtruuj7esgo8e5cqeu26vnf2571', 2, 'product-3.jpg', 'Guangzhou sweater', 2, 1000, 2000, '2020-08-10 11:17:33'),
(270, '69d2gmtruuj7esgo8e5cqeu26vnf2571', 1, 'product-1.jpg', 'Pure Pineapple', 2, 1100, 2200, '2020-08-10 11:17:44'),
(271, 'dl556v1iushlkcm8j9h7c8m2krc71bp3', 1, 'product-1.jpg', 'Pure Pineapple', 2, 1100, 2200, '2020-08-13 17:21:34'),
(272, 'dl556v1iushlkcm8j9h7c8m2krc71bp3', 2, 'product-3.jpg', 'Guangzhou sweater', 2, 1000, 2000, '2020-08-13 17:21:45'),
(273, '8t8h7thfvibe52fosl2e35sn14tit5hs', 3, 'man-1.jpg', 'Canverse coat', 1, 1500, 1500, '2020-08-14 17:08:11'),
(274, 'l8vpnhcd0h0s4cal5gotrj9j7tfl4ngp', 1, 'product-1.jpg', 'Pure Pineapple', 2, 1100, 2200, '2020-08-20 12:51:33'),
(275, 'hsi2e54fjidko3fkpl4reb7i7vl3snos', 2, 'product-3.jpg', 'Guangzhou sweater', 2, 1000, 2000, '2020-08-21 15:37:50'),
(276, 'hsi2e54fjidko3fkpl4reb7i7vl3snos', 1, 'product-1.jpg', 'Pure Pineapple', 2, 1100, 2200, '2020-08-21 15:37:54'),
(277, '31v1gsrbv2mevk4ntn76io5k2de8bkjm', 1, 'product-1.jpg', 'Pure Pineapple', 2, 1100, 2200, '2020-10-16 15:13:44'),
(278, '31v1gsrbv2mevk4ntn76io5k2de8bkjm', 2, 'product-3.jpg', 'Guangzhou sweater', 2, 1000, 2000, '2020-10-16 15:13:53'),
(279, 't7iitvaggmdscdmv6et06vj8gb01cmo5', 1, 'product-1.jpg', 'Pure Pineapple', 2, 1100, 2200, '2020-11-10 16:47:32'),
(280, 't7iitvaggmdscdmv6et06vj8gb01cmo5', 9, '990dfc895a3e80a262a42132b0e425cc.jpg', 'Best shart images | Mens outfits, Mens shirts, Menswear', 1, NULL, 0, '2020-11-10 16:47:41'),
(281, 't7iitvaggmdscdmv6et06vj8gb01cmo5', 3, 'man-1.jpg', 'Canverse coat', 1, 1500, 1500, '2020-11-10 16:48:47'),
(282, 't7iitvaggmdscdmv6et06vj8gb01cmo5', 5, 'mens-stand-collar-plain-shirt.jpg', 'Dudalina Men Shirt', 2, 1120, 2240, '2020-11-10 16:48:59'),
(283, 't7iitvaggmdscdmv6et06vj8gb01cmo5', 2, 'product-3.jpg', 'Guangzhou sweater', 2, 1000, 2000, '2020-11-10 16:50:12'),
(284, 'sctb6t3gi85vmbcam9ge6pi7qerpmdiu', 1, 'product-1.jpg', 'Pure Pineapple', 2, 1100, 2200, '2021-01-27 04:25:40'),
(285, 'pa0s5knknjvsqln1m00ikbevagt41pb3', 9, '990dfc895a3e80a262a42132b0e425cc.jpg', 'Best shart images | Mens outfits, Mens shirts, Menswear', 1, NULL, 0, '2021-01-27 13:30:58'),
(286, 'u8omesiidnv5gfus5e1b8f37sag1tjln', 10, 'e8d0343fbfc82925da3e463d597e5947.jpg', 'Best shart images | Mens outfits, Mens shirts, Menswear', 2, 1100, 2200, '2021-01-27 13:37:53'),
(287, 'u8omesiidnv5gfus5e1b8f37sag1tjln', 1, 'product-1.jpg', 'Pure Pineapple', 2, 1100, 2200, '2021-01-27 13:42:54'),
(288, 'sn29n7ojg3v6j06nuqnorncjh8b4q7jb', 1, 'product-1.jpg', 'Pure Pineapple', 2, 1100, 2200, '2021-02-04 20:25:04'),
(289, '0p77p1iquhkmikv2iqhs1919c0sutpd2', 1, 'product-1.jpg', 'Pure Pineapple', 2, 1100, 2200, '2021-02-05 18:20:41'),
(290, '3tqeu89gtg1s8brv8svps0qbhu2kd8v8', 1, 'product-1.jpg', 'Pure Pineapple', 2, 1100, 2200, '2021-02-05 18:27:21'),
(291, '3tqeu89gtg1s8brv8svps0qbhu2kd8v8', 2, 'product-3.jpg', 'Guangzhou sweater', 2, 1000, 2000, '2021-02-05 18:27:26'),
(292, '3tqeu89gtg1s8brv8svps0qbhu2kd8v8', 3, 'man-1.jpg', 'Canverse coat', 1, 1500, 1500, '2021-02-05 18:27:38'),
(293, 'kfe6pn8oq0kjfnc3gcj1719eidvjn4hj', 2, 'product-3.jpg', 'Guangzhou sweater', 2, 1000, 2000, '2021-02-05 18:48:06'),
(294, 'kfe6pn8oq0kjfnc3gcj1719eidvjn4hj', 1, 'product-1.jpg', 'Pure Pineapple', 2, 1100, 2200, '2021-02-05 18:48:22'),
(295, 'jelc3ms4npja6220ni8j983fgapaobob', 1, 'product-1.jpg', 'Pure Pineapple', 2, 1100, 2200, '2021-02-05 18:51:07'),
(296, 'jelc3ms4npja6220ni8j983fgapaobob', 2, 'product-3.jpg', 'Guangzhou sweater', 2, 1000, 2000, '2021-02-05 18:51:13'),
(297, 'jelc3ms4npja6220ni8j983fgapaobob', 3, 'man-1.jpg', 'Canverse coat', 1, 1500, 1500, '2021-02-05 18:51:17'),
(299, 'tv8je471v5tuujku69pknn18gr96eti5', 2, 'product-3.jpg', 'Guangzhou sweater', 1, 1000, 1000, '2021-02-05 18:57:49'),
(300, 'tv8je471v5tuujku69pknn18gr96eti5', 3, 'man-1.jpg', 'Canverse coat', 1, 1500, 1500, '2021-02-05 18:57:52'),
(305, 'ou7j1uh8d9bjaqq402f6r8mubffho7nm', 1, 'product-1.jpg', 'Pure Pineapple', 1, 1100, 1100, '2021-02-05 19:06:03'),
(306, 'ou7j1uh8d9bjaqq402f6r8mubffho7nm', 10, 'e8d0343fbfc82925da3e463d597e5947.jpg', 'Best shart images | Mens outfits, Mens shirts, Menswear', 1, 1100, 1100, '2021-02-05 19:06:09'),
(308, 'tem4khin6jp7rh279sjq4hltgsa30tmi', 9, '990dfc895a3e80a262a42132b0e425cc.jpg', 'Best shart images | Mens outfits, Mens shirts, Menswear', 1, 1000, 1000, '2021-02-05 19:08:38');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `cat_id` int(11) NOT NULL,
  `category_name` varchar(255) NOT NULL,
  `collection_type` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL COMMENT '0-is draft, 1-is active, 2-is delete',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`cat_id`, `category_name`, `collection_type`, `status`, `created_at`, `modified_at`) VALUES
(1, 'Towel', 0, 1, '2020-06-15 13:40:28', '2020-06-15 13:40:28'),
(2, 'Shoes', 0, 1, '2020-06-15 13:40:28', '2020-06-15 13:40:28'),
(3, 'Coat', 0, 1, '2020-06-15 13:40:28', '2020-06-15 13:40:28'),
(4, 'Dresses', 0, 1, '2020-06-19 09:30:26', '2020-06-19 09:30:26'),
(5, 'Trousers', 0, 1, '2020-06-19 09:30:26', '2020-06-19 09:30:26'),
(6, 'Men\'s hats', 1, 1, '2020-06-19 09:30:26', '2020-06-19 09:30:26'),
(7, 'Bag', 0, 1, '2020-06-19 09:31:41', '2020-06-19 09:31:41'),
(8, 'Shoot', 0, 0, '2021-01-27 05:55:21', '2021-01-27 05:55:21'),
(9, 'as', 0, 0, '2021-01-27 17:00:30', '2021-01-27 17:00:30');

-- --------------------------------------------------------

--
-- Table structure for table `collection`
--

CREATE TABLE `collection` (
  `coll_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '0-is draft, 1-is active, 2-is delete',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `collection`
--

INSERT INTO `collection` (`coll_id`, `name`, `image`, `status`, `created_at`, `modified_at`) VALUES
(1, 'Men\'s', 'men.jpg', 1, '2020-06-19 06:24:38', '2020-06-19 06:24:38'),
(2, 'Women\'s', 'women.jpg', 1, '2020-06-19 06:24:38', '2020-06-19 06:24:38'),
(3, 'Kid\'s', 'kid.jpg', 1, '2020-06-19 06:24:38', '2020-06-19 06:24:38');

-- --------------------------------------------------------

--
-- Table structure for table `compare`
--

CREATE TABLE `compare` (
  `com_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `product_title` varchar(255) NOT NULL,
  `product_price` float NOT NULL,
  `img_name` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `department`
--

CREATE TABLE `department` (
  `dep_id` int(11) NOT NULL,
  `dep_name` varchar(255) NOT NULL,
  `status` tinyint(4) NOT NULL COMMENT '0-is draft, 1-is active, 2-is delete',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `department`
--

INSERT INTO `department` (`dep_id`, `dep_name`, `status`, `created_at`, `modified_at`) VALUES
(1, 'Men’s Clothing', 1, '2020-06-15 14:02:14', '2020-06-15 14:02:14'),
(2, 'Underwear', 1, '2020-06-15 14:02:14', '2020-06-15 14:02:14'),
(3, 'Kid\'s Clothing', 1, '2020-06-15 14:02:14', '2020-06-15 14:02:14'),
(4, 'Brand Fashion', 1, '2020-06-15 14:02:14', '2020-06-15 14:02:14'),
(5, 'Accessories', 1, '2020-06-15 14:03:21', '2020-06-15 14:03:21'),
(6, 'Shoes', 1, '2020-06-15 14:03:21', '2020-06-15 14:03:21'),
(8, 'Hangbag', 1, '2020-06-15 14:13:40', '2020-06-15 14:13:40'),
(9, 'Wemen’s Clothing', 1, '2020-06-18 20:03:09', '2020-06-18 20:03:09');

-- --------------------------------------------------------

--
-- Table structure for table `discount_codes`
--

CREATE TABLE `discount_codes` (
  `discount_id` int(11) NOT NULL,
  `discount_type` varchar(255) NOT NULL,
  `discount_code` varchar(255) NOT NULL,
  `amount` varchar(255) NOT NULL,
  `valid_from_date` datetime NOT NULL,
  `valid_to_date` datetime NOT NULL,
  `status` tinyint(4) NOT NULL COMMENT '0-is_draft, 1-is_active',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `districts`
--

CREATE TABLE `districts` (
  `id` int(3) NOT NULL,
  `name` varchar(255) CHARACTER SET latin1 NOT NULL,
  `shipping_cost` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `districts`
--

INSERT INTO `districts` (`id`, `name`, `shipping_cost`) VALUES
(1, 'Comilla', '100'),
(2, 'Feni', '110'),
(3, 'Brahmanbaria', '120'),
(4, 'Rangamati', '130'),
(5, 'Noakhali', '140'),
(6, 'Chandpur', '150'),
(7, 'Lakshmipur', '160'),
(8, 'Chattogram', '170'),
(9, 'Coxsbazar', '180'),
(10, 'Khagrachhari', '190'),
(11, 'Bandarban', '200'),
(12, 'Sirajganj', '210'),
(13, 'Pabna', '220'),
(14, 'Bogura', '230'),
(15, 'Rajshahi', '240'),
(16, 'Natore', '250'),
(17, 'Joypurhat', '260'),
(18, 'Chapainawabganj', '270'),
(19, 'Naogaon', '280'),
(20, 'Jashore', '290'),
(21, 'Satkhira', '300'),
(22, 'Meherpur', '310'),
(23, 'Narail', '320'),
(24, 'Chuadanga', '330'),
(25, 'Kushtia', '340'),
(26, 'Magura', '350'),
(27, 'Khulna', '360'),
(28, 'Bagerhat', '370'),
(29, 'Jhenaidah', '380'),
(30, 'Jhalakathi', '390'),
(31, 'Patuakhali', '400'),
(32, 'Pirojpur', '410'),
(33, 'Barisal', '420'),
(34, 'Bhola', '430'),
(35, 'Barguna', '440'),
(36, 'Sylhet', '450'),
(37, 'Moulvibazar', '460'),
(38, 'Habiganj', '470'),
(39, 'Sunamganj', '480'),
(40, 'Narsingdi', '490'),
(41, 'Gazipur', '500'),
(42, 'Shariatpur', '510'),
(43, 'Narayanganj', '520'),
(44, 'Tangail', '530'),
(45, 'Kishoreganj', '540'),
(46, 'Manikganj', '550'),
(47, 'Dhaka', '560'),
(48, 'Munshiganj', '570'),
(49, 'Rajbari', '580'),
(50, 'Madaripur', '590'),
(51, 'Gopalganj', '600'),
(52, 'Faridpur', '610'),
(53, 'Panchagarh', '620'),
(54, 'Dinajpur', '630'),
(55, 'Lalmonirhat', '640'),
(56, 'Nilphamari', '650'),
(57, 'Gaibandha', '660'),
(58, 'Thakurgaon', '670'),
(59, 'Rangpur', '680'),
(60, 'Kurigram', '690'),
(61, 'Sherpur', '700'),
(62, 'Mymensingh', '710'),
(63, 'Jamalpur', '720'),
(64, 'Netrokona', '730');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `ord_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `shipping_id` int(11) NOT NULL,
  `payment_type` varchar(100) NOT NULL,
  `vat` float NOT NULL,
  `shipping_cost` int(4) NOT NULL,
  `total_price` float NOT NULL,
  `order_status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0-pending, 1-shipping, 2-complete',
  `shipping_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `order_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`ord_id`, `user_id`, `shipping_id`, `payment_type`, `vat`, `shipping_cost`, `total_price`, `order_status`, `shipping_date`, `order_date`, `modified_at`) VALUES
(1, 1, 3, 'bKash', 550, 50, 0, 1, '2021-01-30 19:00:19', '2020-07-14 12:08:25', '2020-07-14 12:08:25'),
(2, 1, 4, 'bKash', 2800, 50, 5600, 1, '2021-01-30 19:00:19', '2020-07-14 12:18:43', '2020-07-14 12:18:43'),
(3, 1, 5, 'bKash', 3100, 50, 6200, 1, '2021-01-30 19:00:19', '2020-07-16 07:21:38', '2020-07-16 07:21:38'),
(4, 1, 6, 'bKash', 2350, 50, 4700, 1, '2021-01-30 19:00:19', '2020-07-16 11:46:23', '2020-07-16 11:46:23'),
(5, 1, 7, 'cash', 10500, 50, 21000, 1, '2021-01-30 19:00:19', '2020-07-17 19:40:22', '2020-07-17 19:40:22'),
(6, 1, 8, 'bKash', 3550, 150, 7100, 0, '2021-01-30 19:00:19', '2020-07-18 08:58:46', '2020-07-18 08:58:46'),
(7, 1, 9, 'bKash', 2150, 50, 4300, 0, '2021-01-30 19:00:19', '2020-07-21 04:39:30', '2020-07-21 04:39:30'),
(8, 6, 10, 'bKash', 500, 150, 1000, 0, '2021-01-30 19:00:19', '2020-07-23 17:07:08', '2020-07-23 17:07:08'),
(9, 1, 11, 'cash', 500, 50, 1000, 0, '2021-01-30 19:00:19', '2020-08-03 14:11:46', '2020-08-03 14:11:46'),
(10, 1, 12, 'cash', 500, 50, 1000, 0, '2021-01-30 19:00:19', '2020-08-14 14:03:54', '2020-08-14 14:03:54'),
(11, 1, 13, 'cash', 500, 50, 1000, 1, '2021-01-30 19:00:19', '2020-08-14 14:05:59', '2020-08-14 14:05:59'),
(12, 1, 14, 'cash', 1100, 50, 2200, 0, '2021-01-30 19:00:19', '2020-08-14 14:21:03', '2020-08-14 14:21:03'),
(13, 1, 15, 'cash', 1650, 50, 3300, 0, '2021-01-30 19:00:19', '2020-08-14 14:21:55', '2020-08-14 14:21:55'),
(14, 1, 16, 'cash', 1100, 50, 2200, 0, '2021-01-30 19:00:19', '2020-08-14 14:24:41', '2020-08-14 14:24:41'),
(15, 1, 17, 'cash', 2100, 50, 4200, 0, '2021-01-30 19:00:19', '2020-08-14 14:26:30', '2020-08-14 14:26:30'),
(16, 1, 18, 'cash', 1650, 50, 3300, 0, '2021-01-30 19:00:19', '2020-08-20 12:55:07', '2020-08-20 12:55:07'),
(17, 9, 19, 'cash', 2310, 560, 4620, 0, '2021-01-30 19:00:19', '2021-01-27 13:56:09', '2021-01-27 13:56:09'),
(18, 1, 20, 'bKash', 2950, 100, 5900, 0, '2021-01-30 19:00:19', '2021-01-30 18:16:33', '2021-01-30 18:16:33'),
(19, 1, 21, 'bKash', 1100, 170, 2200, 0, '2021-02-04 20:39:45', '2021-02-04 20:39:45', '2021-02-04 20:39:45');

-- --------------------------------------------------------

--
-- Table structure for table `order_details`
--

CREATE TABLE `order_details` (
  `product_id` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `price` float NOT NULL,
  `order_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `order_details`
--

INSERT INTO `order_details` (`product_id`, `qty`, `price`, `order_id`, `created_at`, `modified_at`) VALUES
(1, 1, 1100, 1, '2020-07-14 12:08:25', '2020-07-14 12:08:25'),
(1, 1, 1100, 2, '2020-07-14 12:18:43', '2020-07-14 12:18:43'),
(2, 3, 3000, 2, '2020-07-14 12:18:43', '2020-07-14 12:18:43'),
(3, 1, 1500, 2, '2020-07-14 12:18:43', '2020-07-14 12:18:43'),
(2, 4, 4000, 3, '2020-07-16 07:21:38', '2020-07-16 07:21:38'),
(1, 2, 2200, 3, '2020-07-16 07:21:38', '2020-07-16 07:21:38'),
(1, 2, 2200, 4, '2020-07-16 11:46:23', '2020-07-16 11:46:23'),
(2, 1, 1000, 4, '2020-07-16 11:46:23', '2020-07-16 11:46:23'),
(3, 1, 1500, 4, '2020-07-16 11:46:23', '2020-07-16 11:46:23'),
(2, 3, 3000, 5, '2020-07-17 19:40:22', '2020-07-17 19:40:22'),
(1, 5, 5500, 5, '2020-07-17 19:40:22', '2020-07-17 19:40:22'),
(4, 5, 5000, 5, '2020-07-17 19:40:22', '2020-07-17 19:40:22'),
(3, 5, 7500, 5, '2020-07-17 19:40:22', '2020-07-17 19:40:22'),
(1, 1, 1100, 6, '2020-07-18 08:58:46', '2020-07-18 08:58:46'),
(2, 1, 1000, 6, '2020-07-18 08:58:46', '2020-07-18 08:58:46'),
(3, 2, 3000, 6, '2020-07-18 08:58:46', '2020-07-18 08:58:46'),
(4, 2, 2000, 6, '2020-07-18 08:58:46', '2020-07-18 08:58:46'),
(1, 3, 3300, 7, '2020-07-21 04:39:30', '2020-07-21 04:39:30'),
(2, 1, 1000, 7, '2020-07-21 04:39:30', '2020-07-21 04:39:30'),
(4, 1, 1000, 8, '2020-07-23 17:07:08', '2020-07-23 17:07:08'),
(4, 1, 1000, 9, '2020-08-03 14:11:46', '2020-08-03 14:11:46'),
(2, 1, 1000, 10, '2020-08-14 14:03:54', '2020-08-14 14:03:54'),
(2, 1, 1000, 11, '2020-08-14 14:05:59', '2020-08-14 14:05:59'),
(1, 2, 2200, 12, '2020-08-14 14:21:03', '2020-08-14 14:21:03'),
(1, 3, 3300, 13, '2020-08-14 14:21:55', '2020-08-14 14:21:55'),
(1, 2, 2200, 14, '2020-08-14 14:24:41', '2020-08-14 14:24:41'),
(2, 2, 2000, 15, '2020-08-14 14:26:30', '2020-08-14 14:26:30'),
(1, 2, 2200, 15, '2020-08-14 14:26:30', '2020-08-14 14:26:30'),
(1, 3, 3300, 16, '2020-08-20 12:55:07', '2020-08-20 12:55:07'),
(2, 1, 1000, 17, '2021-01-27 13:56:09', '2021-01-27 13:56:09'),
(3, 1, 1500, 17, '2021-01-27 13:56:09', '2021-01-27 13:56:09'),
(5, 1, 1120, 17, '2021-01-27 13:56:10', '2021-01-27 13:56:10'),
(9, 1, 1000, 17, '2021-01-27 13:56:10', '2021-01-27 13:56:10'),
(1, 2, 2200, 18, '2021-01-30 18:16:33', '2021-01-30 18:16:33'),
(10, 2, 2200, 18, '2021-01-30 18:16:33', '2021-01-30 18:16:33'),
(9, 1, 1000, 18, '2021-01-30 18:16:33', '2021-01-30 18:16:33'),
(11, 1, 500, 18, '2021-01-30 18:16:33', '2021-01-30 18:16:33'),
(1, 1, 1100, 19, '2021-02-04 20:39:45', '2021-02-04 20:39:45'),
(10, 1, 1100, 19, '2021-02-04 20:39:45', '2021-02-04 20:39:45');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `product_id` int(11) NOT NULL,
  `brand_id` int(11) DEFAULT NULL,
  `cat_id` int(11) DEFAULT NULL,
  `dep_id` int(11) NOT NULL,
  `coll_id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `short_description` text,
  `description` text,
  `image` varchar(255) NOT NULL,
  `color` varchar(100) NOT NULL,
  `size` varchar(100) NOT NULL,
  `total_sales` int(11) DEFAULT NULL,
  `in_stock` int(11) DEFAULT NULL,
  `product_type` tinyint(2) DEFAULT NULL COMMENT '0-regular product, 1-new product, 2-feature product, 3-coming soon',
  `cost` float DEFAULT NULL,
  `mrp` float DEFAULT NULL,
  `price` float DEFAULT NULL,
  `soft_delete` tinyint(1) DEFAULT '0',
  `is_active` tinyint(2) DEFAULT '0' COMMENT '0-is draft, 1-is active',
  `coming_soon_time` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`product_id`, `brand_id`, `cat_id`, `dep_id`, `coll_id`, `title`, `short_description`, `description`, `image`, `color`, `size`, `total_sales`, `in_stock`, `product_type`, `cost`, `mrp`, `price`, `soft_delete`, `is_active`, `coming_soon_time`, `created_at`, `modified_at`) VALUES
(1, 4, 4, 9, 2, 'Pure Pineapple', 'Lorem ipsum dolor sit amet, consectetur ing elit, sed do eiusmod tempor sum dolor sit amet, consectetur adipisicing elit, sed do mod tempor', 'Lorem ipsum dolor sit amet, consectetur ing elit, sed do eiusmod tempor sum dolor sit amet, consectetur adipisicing elit, sed do mod temporLorem ipsum dolor sit amet, consectetur ing elit, sed do eiusmod tempor sum dolor sit amet, consectetur adipisicing elit, sed do mod tempor', 'product-1.jpg', '', '', 20, 2, 0, 100, 1100, 1200, 0, 1, NULL, '2020-06-18 20:04:29', '2020-06-18 20:04:29'),
(2, 1, 3, 8, 2, 'Guangzhou sweater', 'sum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.Ut enim ad minim veniam,', 'sum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.Ut enim ad minim veniam,', 'product-3.jpg', '', '', 8, 12, 1, 100, 1000, 1200, 0, 1, NULL, '2020-06-19 05:55:03', '2020-06-19 05:55:03'),
(3, 3, 7, 1, 1, 'Canverse coat', 'Shoppers can view size and fit information for each shoe, as well as use the site’s visualization tools to view shoes in different colors.', 'Shoppers can view size and fit information for each shoe, as well as use the site’s visualization tools to view shoes in different colors.Shoppers can view size and fit information for each shoe, as well as use the site’s visualization tools to view shoes in different colors.', 'man-1.jpg', '', '', 11, 11, 1, 100, 1500, 1800, 0, 1, NULL, '2020-06-19 09:38:53', '2020-06-19 09:38:53'),
(4, 4, 2, 1, 1, 'Shoppers can view size', 'Shoppers can view size and fit information for each shoe, as well as use the site’s visualization tools to view shoes in different colors.', 'Shoppers can view size and fit information for each shoe, as well as use the site’s visualization tools to view shoes in different colors.Shoppers can view size and fit information for each shoe, as well as use the site’s visualization tools to view shoes in different colors.', 'man-2.jpg', '', '', 10, 15, 1, 100, 1000, 1200, 0, 1, NULL, '2020-06-19 09:38:53', '2020-06-19 09:38:53'),
(5, 1, 4, 1, 1, 'Dudalina Men Shirt', 'Gender: Men Item Type: Shirts Style: Smart Casual Collar: Turn-down Collar Closure Type: Single Breasted Sleeve Style: Regular Shirts Type: Casual ...', 'Gender: Men Item Type: Shirts Style: Smart Casual Collar: Turn-down Collar Closure Type: Single Breasted Sleeve Style: Regular Shirts Type: Casual ...Gender: Men Item Type: Shirts Style: Smart Casual Collar: Turn-down Collar Closure Type: Single Breasted Sleeve Style: Regular Shirts Type: Casual ...', 'mens-stand-collar-plain-shirt.jpg', '', '', 1, 14, 1, 100, 1120, 1220, 0, 1, '0000-00-00 00:00:00', '2020-08-12 06:23:13', '2020-08-12 06:23:13'),
(7, 2, 1, 1, 2, 'Pure Pineapple', 'Lorem ipsum dolor sit amet, consectetur ing elit, sed do eiusmod tempor sum dolor sit amet, consectetur adipisicing elit, sed do mod tempor', NULL, '12222.jpg', '', '', NULL, 10, 1, NULL, NULL, NULL, 0, 0, NULL, '2020-08-15 13:10:04', '2020-08-15 13:10:04'),
(8, 1, 2, 3, 1, 'Pure Pineapple', 'Lorem ipsum dolor sit amet, consectetur ing elit, sed do eiusmod tempor sum dolor sit amet, consectetur adipisicing elit, sed do mod tempor', NULL, 'aba378a94aaf556e113ce40e212aaa62.jpg', '', '', NULL, 10, 2, NULL, NULL, NULL, 0, 0, NULL, '2020-08-21 14:22:10', '2020-08-21 14:22:10'),
(9, 1, 4, 1, 1, 'Best shart images | Mens outfits, Mens shirts, Menswear', 'Lorem ipsum dolor sit amet, consectetur ing elit, sed do eiusmod tempor sum dolor sit amet, consectetur adipisicing elit, sed do mod tempor', NULL, '7d65989650741c9cea4fd1f7527ea41a.jpg', '', '', 2, 3, 1, NULL, 1000, 1500, 0, 0, NULL, '2020-08-21 14:34:49', '2020-08-21 14:34:49'),
(10, 1, 4, 4, 2, 'Best shart images | Mens outfits, Mens shirts, Menswear', 'Lorem ipsum dolor sit amet, consectetur ing elit, sed do eiusmod tempor sum dolor sit amet, consectetur adipisicing elit, sed do mod tempor', NULL, '77502aa49a5130d6199518ed04d774c7.jpg', '', '', 3, 2, 1, NULL, 1100, 1600, 0, 0, NULL, '2021-01-27 05:50:54', '2021-01-27 05:50:54'),
(11, 2, 1, 1, 1, 'This is product title ', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s,', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s,Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s,Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s,', 'f1de543a7d96188c993ed8d65df3e027.jpg', '', '', 1, 4, 1, NULL, 500, 800, 0, 1, NULL, '2021-01-27 14:02:35', '2021-01-27 14:02:35'),
(12, 1, 4, 3, 3, 'Best shart images | Mens outfits, Mens shirts, Menswear', 'Lorem ipsum dolor sit amet, consectetur ing elit, sed do eiusmod tempor sum dolor sit amet, consectetur adipisicing elit, sed do mod tempor', 'Lorem ipsum dolor sit amet, consectetur ing elit, sed do eiusmod tempor sum dolor sit amet, consectetur adipisicing elit, sed do mod temporLorem ipsum dolor sit amet, consectetur ing elit, sed do eiusmod tempor sum dolor sit amet, consectetur adipisicing elit, sed do mod temporLorem ipsum dolor sit amet, consectetur ing elit, sed do eiusmod tempor sum dolor sit amet, consectetur adipisicing elit, sed do mod tempor', 'a7a7ac90f60381eeeea07c8436e9b1b2.jpg', '', '', NULL, 10, 1, NULL, 1200, 1400, 0, 1, NULL, '2021-02-04 20:32:02', '2021-02-04 20:32:02'),
(13, 2, 6, 3, 3, 'Pure Pineapple', 'Lorem ipsum dolor sit amet, consectetur ing elit, sed do eiusmod tempor sum dolor sit amet, consectetur adipisicing elit, sed do mod tempor', 'Lorem ipsum dolor sit amet, consectetur ing elit, sed do eiusmod tempor sum dolor sit amet, consectetur adipisicing elit, sed do mod tempor Lorem ipsum dolor sit amet, consectetur ing elit, sed do eiusmod tempor sum dolor sit amet, consectetur adipisicing elit, sed do mod tempor Lorem ipsum dolor sit amet, consectetur ing elit, sed do eiusmod tempor sum dolor sit amet, consectetur adipisicing elit, sed do mod tempor', 'cc0ab8180361f476afb5b8f8cec6916f.jpg', '', '', NULL, 12, 1, NULL, 100, 1200, 0, 1, NULL, '2021-02-04 20:33:16', '2021-02-04 20:33:16');

-- --------------------------------------------------------

--
-- Table structure for table `product_image`
--

CREATE TABLE `product_image` (
  `img_id` int(11) NOT NULL,
  `img_name` varchar(255) NOT NULL,
  `product_id` int(11) NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1-yes, 0-no',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `modified_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product_image`
--

INSERT INTO `product_image` (`img_id`, `img_name`, `product_id`, `is_active`, `created_at`, `modified_at`) VALUES
(1, 'product-1.jpg', 1, 1, '2020-06-18 20:30:50', '2020-06-18 20:30:50'),
(2, 'product-2.jpg', 1, 1, '2020-06-19 05:31:03', '2020-06-19 05:31:03'),
(3, 'product-3.jpg', 2, 1, '2020-06-19 15:44:06', '2020-06-19 05:31:03'),
(4, 'man-1.jpg', 3, 1, '2020-06-19 09:50:16', '2020-06-19 09:50:16'),
(5, 'man-2.jpg', 4, 1, '2020-06-19 09:50:16', '2020-06-19 09:50:16'),
(6, 'man-3.jpg', 3, 1, '2020-06-19 09:50:16', '2020-06-19 09:50:16'),
(7, 'man-4.jpg', 4, 1, '2020-06-19 09:50:16', '2020-06-19 09:50:16'),
(8, 'mens-stand-collar-plain-shirt.jpg', 5, 1, '2020-08-12 06:50:44', '2020-08-12 06:34:25'),
(9, '12222.jpg', 5, 1, '2020-08-12 06:46:29', '2020-08-12 06:34:25'),
(10, 'men-shirts.jpg', 5, 1, '2020-08-12 06:34:25', '2020-08-12 06:34:25'),
(11, '990dfc895a3e80a262a42132b0e425cc.jpg', 9, 1, '2020-08-21 14:34:49', '2020-08-21 14:34:49'),
(12, '8f19a650f56153a4572aa300eb2a97d5.jpg', 9, 1, '2020-08-21 14:34:50', '2020-08-21 14:34:50'),
(13, 'e8d0343fbfc82925da3e463d597e5947.jpg', 10, 1, '2021-01-27 05:50:54', '2021-01-27 05:50:54'),
(14, 'c65f536e839b3e7d9e36c699d302acb8.jpg', 10, 1, '2021-01-27 05:50:54', '2021-01-27 05:50:54'),
(15, 'ece0a4e07ee68f8f446aa2fb3996a56f.jpg', 10, 1, '2021-01-27 05:50:54', '2021-01-27 05:50:54'),
(16, 'addcc4c11a652a3681fad7975d47dbc8.jpg', 11, 1, '2021-01-27 14:02:36', '2021-01-27 14:02:36'),
(17, '7c8d54deb9d37ee788ce4d0040d63898.jpg', 11, 1, '2021-01-27 14:02:36', '2021-01-27 14:02:36'),
(18, '0a29c8345580cf64cd54efe21017ca6f.jpg', 11, 1, '2021-01-27 14:02:36', '2021-01-27 14:02:36'),
(19, '3343b8a9ce9325c9278d0bf85a39cc45.jpg', 11, 1, '2021-01-27 14:02:36', '2021-01-27 14:02:36'),
(20, '69625a0a80720ddca0a3594a342b47a8.jpg', 12, 1, '2021-02-04 20:32:02', '2021-02-04 20:32:02'),
(21, 'b74940908a7812bbc23402bfcb3ef30d.jpg', 12, 1, '2021-02-04 20:32:02', '2021-02-04 20:32:02'),
(22, '9b183e97f9dd565394decd270cccb0b2.jpg', 13, 1, '2021-02-04 20:34:38', '2021-02-04 20:33:16'),
(23, '9b183e97f9dd565394decd270cccb0b2.jpg', 13, 1, '2021-02-04 20:33:16', '2021-02-04 20:33:16');

-- --------------------------------------------------------

--
-- Table structure for table `shipping_info`
--

CREATE TABLE `shipping_info` (
  `shipping_id` int(11) NOT NULL,
  `districts_id` int(11) NOT NULL,
  `shipping_fullname` varchar(255) NOT NULL,
  `shipping_email` varchar(255) NOT NULL,
  `shipping_phone` varchar(100) NOT NULL,
  `shipping_address` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `shipping_info`
--

INSERT INTO `shipping_info` (`shipping_id`, `districts_id`, `shipping_fullname`, `shipping_email`, `shipping_phone`, `shipping_address`, `created_at`, `modified_at`) VALUES
(1, 8, 'Gm Abbas Uddin', 'gmabbas44@gmail.com', '01845109044', 'madam bibir hat, bhatiary, sitakunda, chattogorm', '2020-07-14 11:14:15', '2020-07-14 11:14:15'),
(2, 8, 'Gm Abbas Uddin', 'gmabbas44@gmail.com', '01845109044', 'madam bibir hat, bhatiary, sitakunda, chattogorm', '2020-07-14 12:06:59', '2020-07-14 12:06:59'),
(3, 8, 'Gm Abbas Uddin', 'gmabbas44@gmail.com', '01845109044', 'madam bibir hat, bhatiary, sitakunda, chattogorm', '2020-07-14 12:08:25', '2020-07-14 12:08:25'),
(4, 8, 'Gm Abbas Uddin', 'gmabbas44@gmail.com', '01845109044', 'madam bibir hat, bhatiary, sitakunda, chattogorm', '2020-07-14 12:18:43', '2020-07-14 12:18:43'),
(5, 8, 'Gm Abbas Uddin', 'gmabbas44@gmail.com', '01845109044', 'madam bibir hat, bhatiary, sitakunda, chattogorm', '2020-07-16 07:21:38', '2020-07-16 07:21:38'),
(6, 8, 'Gm Abbas Uddin', 'gmabbas44@gmail.com', '01845109044', 'madam bibir hat, bhatiary, sitakunda, chattogorm', '2020-07-16 11:46:22', '2020-07-16 11:46:22'),
(7, 8, 'Gm Abbas Uddin', 'gmabbas44@gmail.com', '01845109044', 'madam bibir hat, bhatiary, sitakunda, chattogorm', '2020-07-17 19:40:22', '2020-07-17 19:40:22'),
(8, 1, 'Khaleda Akter Nodi', 'khaledaakter382616@gmail.com', '01814953905', 'chittagong', '2020-07-18 08:58:46', '2020-07-18 08:58:46'),
(9, 8, 'Gm Abbas Uddin', 'gmabbas44@gmail.com', '01845109044', 'madam bibir hat, bhatiary, sitakunda, chattogorm', '2020-07-21 04:39:29', '2020-07-21 04:39:29'),
(10, 1, 'Khaleda Akter', 'khaledaakter382616@gmail.com', '01814953905', 'chittagong', '2020-07-23 17:07:08', '2020-07-23 17:07:08'),
(11, 8, 'Gm Abbas Uddin', 'gmabbas44@gmail.com', '01845109044', 'madam bibir hat, bhatiary, sitakunda, chattogorm', '2020-08-03 14:11:46', '2020-08-03 14:11:46'),
(12, 8, 'Gm Abbas Uddin', 'gmabbas44@gmail.com', '01845109044', 'madam bibir hat, bhatiary, sitakunda, chattogorm', '2020-08-14 14:03:54', '2020-08-14 14:03:54'),
(13, 8, 'Gm Abbas Uddin', 'gmabbas44@gmail.com', '01845109044', 'madam bibir hat, bhatiary, sitakunda, chattogorm', '2020-08-14 14:05:59', '2020-08-14 14:05:59'),
(14, 8, 'Gm Abbas Uddin', 'gmabbas44@gmail.com', '01845109044', 'madam bibir hat, bhatiary, sitakunda, chattogorm', '2020-08-14 14:21:03', '2020-08-14 14:21:03'),
(15, 8, 'Gm Abbas Uddin', 'gmabbas44@gmail.com', '01845109044', 'madam bibir hat, bhatiary, sitakunda, chattogorm', '2020-08-14 14:21:55', '2020-08-14 14:21:55'),
(16, 8, 'Gm Abbas Uddin', 'gmabbas44@gmail.com', '01845109044', 'madam bibir hat, bhatiary, sitakunda, chattogorm', '2020-08-14 14:24:41', '2020-08-14 14:24:41'),
(17, 8, 'Gm Abbas Uddin', 'gmabbas44@gmail.com', '01845109044', 'madam bibir hat, bhatiary, sitakunda, chattogorm', '2020-08-14 14:26:30', '2020-08-14 14:26:30'),
(18, 8, 'Gm Abbas Uddin', 'gmabbas44@gmail.com', '01845109044', 'madam bibir hat, bhatiary, sitakunda, chattogorm', '2020-08-20 12:55:07', '2020-08-20 12:55:07'),
(19, 47, 'Nodi', 'nodi@gmail.com', '01814953905', 'chattogram', '2021-01-27 13:56:09', '2021-01-27 13:56:09'),
(20, 1, 'Gm Abbas Uddin', 'gmabbas44@gmail.com', '01845109044', 'madam bibir hat, bhatiary, sitakunda, chattogorm', '2021-01-30 18:16:33', '2021-01-30 18:16:33'),
(21, 8, 'Gm Abbas Uddin', 'gmabbas44@gmail.com', '01845109044', 'madam bibir hat, bhatiary, sitakunda, chattogorm', '2021-02-04 20:39:45', '2021-02-04 20:39:45');

-- --------------------------------------------------------

--
-- Table structure for table `tags`
--

CREATE TABLE `tags` (
  `tag_id` int(11) NOT NULL,
  `tag_name` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `modified_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tags`
--

INSERT INTO `tags` (`tag_id`, `tag_name`, `created_at`, `modified_at`) VALUES
(1, 'Towel', '2020-06-15 13:49:13', '0000-00-00 00:00:00'),
(2, 'Shoes', '2020-06-15 13:50:45', '2020-06-15 13:50:45'),
(3, 'Coat', '2020-06-15 13:50:45', '2020-06-15 13:50:45'),
(4, 'Dresses', '2020-06-15 13:50:45', '2020-06-15 13:50:45'),
(5, 'Trousers', '2020-06-15 13:50:45', '2020-06-15 13:50:45'),
(6, 'Men\'s hats', '2020-06-15 13:50:45', '2020-06-15 13:50:45'),
(7, 'Backpack', '2020-06-15 13:50:45', '2020-06-15 13:50:45');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `user_id` int(11) NOT NULL,
  `fullname` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `email` varchar(100) NOT NULL,
  `mobile` varchar(100) NOT NULL,
  `address` text NOT NULL,
  `city` int(11) NOT NULL,
  `country` varchar(255) NOT NULL,
  `zip` int(11) NOT NULL,
  `password` varchar(255) NOT NULL,
  `ip` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`user_id`, `fullname`, `username`, `email`, `mobile`, `address`, `city`, `country`, `zip`, `password`, `ip`, `created_at`, `modified_at`) VALUES
(1, 'Gm Abbas Uddin', 'gmabbas', 'gmabbas44@gmail.com', '01845109044', 'madam bibir hat, bhatiary, sitakunda, chattogorm', 8, 'bangladesh', 4315, '7c222fb2927d828af22f592134e8932480637c0d', '::1', '2020-06-12 08:34:33', '2020-06-12 08:34:33'),
(6, 'Khaleda Akter', 'Khaledacse', 'khaledaakter382616@gmail.com', '01814953905', '', 1, '', 0, '7c222fb2927d828af22f592134e8932480637c0d', '::1', '2020-06-12 10:30:50', '2020-06-12 10:30:50'),
(7, '', 'gmabbas44', 'gmabbas@gmail.com', '01845109044', '', 8, '', 0, '7c222fb2927d828af22f592134e8932480637c0d', '::1', '2020-06-13 10:56:05', '2020-06-13 10:56:05'),
(9, '', 'Nodi', 'nodi@gmail.com', '01814953905', '', 47, '', 0, '7c222fb2927d828af22f592134e8932480637c0d', '::1', '2021-01-27 13:17:54', '2021-01-27 13:17:54');

-- --------------------------------------------------------

--
-- Table structure for table `wishlist`
--

CREATE TABLE `wishlist` (
  `ws_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `product_title` varchar(255) NOT NULL,
  `product_img` varchar(255) NOT NULL,
  `mrp` float NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `modified_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `wishlist`
--

INSERT INTO `wishlist` (`ws_id`, `user_id`, `product_id`, `product_title`, `product_img`, `mrp`, `created_at`, `modified_at`) VALUES
(5, 6, 1, 'Pure Pineapple', 'product-1.jpg', 1100, '2020-07-27 17:12:35', '2020-07-27 17:12:35'),
(58, 6, 1, 'Pure Pineapple', 'product-1.jpg', 1100, '2020-07-27 17:12:35', '2020-07-27 17:12:35'),
(59, 1, 3, 'Canverse coat', 'man-1.jpg', 1500, '2020-08-14 14:37:30', '2020-08-14 14:37:30'),
(60, 1, 2, 'Guangzhou sweater', 'product-3.jpg', 1000, '2020-08-14 17:09:51', '2020-08-14 17:09:51');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`admin_id`);

--
-- Indexes for table `brand`
--
ALTER TABLE `brand`
  ADD PRIMARY KEY (`brand_id`);

--
-- Indexes for table `carts`
--
ALTER TABLE `carts`
  ADD PRIMARY KEY (`cart_id`),
  ADD KEY `product_id` (`product_id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`cat_id`),
  ADD KEY `collection_type` (`collection_type`);

--
-- Indexes for table `collection`
--
ALTER TABLE `collection`
  ADD PRIMARY KEY (`coll_id`);

--
-- Indexes for table `compare`
--
ALTER TABLE `compare`
  ADD PRIMARY KEY (`com_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `product_id` (`product_id`);

--
-- Indexes for table `department`
--
ALTER TABLE `department`
  ADD PRIMARY KEY (`dep_id`);

--
-- Indexes for table `discount_codes`
--
ALTER TABLE `discount_codes`
  ADD PRIMARY KEY (`discount_id`);

--
-- Indexes for table `districts`
--
ALTER TABLE `districts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`ord_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `shipping_id` (`shipping_id`);

--
-- Indexes for table `order_details`
--
ALTER TABLE `order_details`
  ADD KEY `product_id` (`product_id`),
  ADD KEY `order_id` (`order_id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`product_id`),
  ADD KEY `brand_id` (`brand_id`),
  ADD KEY `cat_id` (`cat_id`),
  ADD KEY `dep_id` (`dep_id`),
  ADD KEY `coll_id` (`coll_id`);

--
-- Indexes for table `product_image`
--
ALTER TABLE `product_image`
  ADD PRIMARY KEY (`img_id`),
  ADD KEY `product_id` (`product_id`);

--
-- Indexes for table `shipping_info`
--
ALTER TABLE `shipping_info`
  ADD PRIMARY KEY (`shipping_id`),
  ADD KEY `districts_id` (`districts_id`);

--
-- Indexes for table `tags`
--
ALTER TABLE `tags`
  ADD PRIMARY KEY (`tag_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`user_id`),
  ADD KEY `city` (`city`);

--
-- Indexes for table `wishlist`
--
ALTER TABLE `wishlist`
  ADD PRIMARY KEY (`ws_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `product_id` (`product_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `admin_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `brand`
--
ALTER TABLE `brand`
  MODIFY `brand_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `carts`
--
ALTER TABLE `carts`
  MODIFY `cart_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=312;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `cat_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `collection`
--
ALTER TABLE `collection`
  MODIFY `coll_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `compare`
--
ALTER TABLE `compare`
  MODIFY `com_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `department`
--
ALTER TABLE `department`
  MODIFY `dep_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `discount_codes`
--
ALTER TABLE `discount_codes`
  MODIFY `discount_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `districts`
--
ALTER TABLE `districts`
  MODIFY `id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=65;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `ord_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `product_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `product_image`
--
ALTER TABLE `product_image`
  MODIFY `img_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `shipping_info`
--
ALTER TABLE `shipping_info`
  MODIFY `shipping_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `tags`
--
ALTER TABLE `tags`
  MODIFY `tag_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `wishlist`
--
ALTER TABLE `wishlist`
  MODIFY `ws_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=61;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `carts`
--
ALTER TABLE `carts`
  ADD CONSTRAINT `carts_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `products` (`product_id`);

--
-- Constraints for table `compare`
--
ALTER TABLE `compare`
  ADD CONSTRAINT `compare_ibfk_2` FOREIGN KEY (`product_id`) REFERENCES `products` (`product_id`),
  ADD CONSTRAINT `compare_ibfk_3` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`);

--
-- Constraints for table `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `orders_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`),
  ADD CONSTRAINT `orders_ibfk_3` FOREIGN KEY (`shipping_id`) REFERENCES `shipping_info` (`shipping_id`);

--
-- Constraints for table `order_details`
--
ALTER TABLE `order_details`
  ADD CONSTRAINT `order_details_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `products` (`product_id`),
  ADD CONSTRAINT `order_details_ibfk_2` FOREIGN KEY (`order_id`) REFERENCES `orders` (`ord_id`);

--
-- Constraints for table `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `products_ibfk_1` FOREIGN KEY (`brand_id`) REFERENCES `brand` (`brand_id`),
  ADD CONSTRAINT `products_ibfk_2` FOREIGN KEY (`cat_id`) REFERENCES `categories` (`cat_id`),
  ADD CONSTRAINT `products_ibfk_3` FOREIGN KEY (`dep_id`) REFERENCES `department` (`dep_id`),
  ADD CONSTRAINT `products_ibfk_4` FOREIGN KEY (`coll_id`) REFERENCES `collection` (`coll_id`);

--
-- Constraints for table `product_image`
--
ALTER TABLE `product_image`
  ADD CONSTRAINT `product_image_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `products` (`product_id`);

--
-- Constraints for table `shipping_info`
--
ALTER TABLE `shipping_info`
  ADD CONSTRAINT `shipping_info_ibfk_1` FOREIGN KEY (`districts_id`) REFERENCES `districts` (`id`);

--
-- Constraints for table `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `user_ibfk_1` FOREIGN KEY (`city`) REFERENCES `districts` (`id`);

--
-- Constraints for table `wishlist`
--
ALTER TABLE `wishlist`
  ADD CONSTRAINT `wishlist_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`),
  ADD CONSTRAINT `wishlist_ibfk_2` FOREIGN KEY (`product_id`) REFERENCES `products` (`product_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
