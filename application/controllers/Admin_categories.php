<?php 


class Admin_categories extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('Admin_categories_model');
	}
	private function logged_in() {

		if ( ! $this->session->userdata('admin_name')) {
			redirect('admin-login');
		}
	} 

	public function index()
	{
		$this->logged_in();
		$this->layout->setSiteTitle('-Categories');
		$this->layout->admin_layout('admin/show_categories');
	}

	public function cat_view()
	{
		if ( $this->input->post('get_cat') && $this->input->post('get_cat') == 'get_all') {
			$all_cat = $this->Admin_categories_model->get_all( 'categories' );
			
			echo json_encode($all_cat);
		}else {
			redirect('Ad_categories');
		}
	}

	public function insert_category() {
		if ( $this->input->post('action')  && $this->input->post('action') == 'add_category' ) {
			$data['category_name'] = $this->input->post('add_Categories');
			$add = $this->Admin_categories_model->add('categories', $data);
			if ( $add ) {
				$response['status'] = 'success';
				$response['msg'] = 'Category add Successfully!';
			}
			else {
				$response['status'] = 'warning';
				$response['msg'] = 'Category add not Successfully!';
			}
			echo json_encode($response);
		}else {
			redirect('Admin_categories');
		}
	}







}

 ?>