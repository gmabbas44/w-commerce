<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

class Product extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model( 'Product_model' );
	}

	public function details( $id )
	{
		$id = decript_id($id);
		
		$getProBYId = $this->Product_model->getProdeuct( $id );
		$prodeuctImg = $this->Product_model->getProdeuctImg( $id );

		$data = ['getProBYId'=> $getProBYId, 'prodeuctImg' => $prodeuctImg ] ;

		$this->layout->setSiteTitle( 'Product Details' );
		$this->layout->view_layout( 'product_details', $data );
	}

	public function quickView()
	{
		if ( $this->input->post('action') == 'quickView' ) {
			$id = decript_id($this->input->post('product_id'));

			$response['getProBYId'] = $this->Product_model->getProdeuct( $id );
			$response['prodeuctImg'] = $this->Product_model->getProdeuctImg( $id );
			echo json_encode( $response );

		} else {
			redirect('');
		}
	}

}