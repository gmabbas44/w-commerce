<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

class Api extends CI_Controller
{
	private $sid;
	function __construct()
	{
		parent::__construct();
		$this->load->model( 'Api_model' );
		$this->sid = $this->session->userdata('session_id');
	}

	public function index()
	{
		$data = $this->Api_model->getCartPro( $this->sid );
		echo json_encode( $data->result_array());
	}

}