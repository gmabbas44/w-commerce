<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Shop extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model( 'Shop_model' );
		$this->load->model( 'HF_model' );
	}

	public function index()
	{
		$allProducts 	= $this->Shop_model->gelAllProduct();
		$getCollection 	= $this->HF_model->getCollection();

		$data = ['allProducts' => $allProducts,  'getCollection' => $getCollection] ;

		$this->layout->setSiteTitle( 'Shop' );
		$this->layout->view_layout( 'shop', $data );
	}

	public function brand()
	{
		if ( $this->input->post('action') ) 
		{
			$getBrand = $this->HF_model->getBrand();
			echo json_encode($getBrand);

		} else {
			redirect('');
		}
	}

	public function product()
	{
		if ( $this->input->post('action') && $this->input->post('action') == 'getProduct' ) 
		{
			$coll_id 	= $this->input->post('coll_id');
			$brand_id 	= $this->input->post('brand_id');

			if ( $coll_id == null || $brand_id == null ) {
				$allProducts = $this->Shop_model->gelAllProduct();
			} else {
				$allProducts = $this->Shop_model->gelProduct( $coll_id, $brand_id );
			}
			echo json_encode($allProducts);

		} else {
			redirect('shop');
		}
	}
}