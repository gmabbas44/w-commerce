<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model( 'Home_model' );

		$data = ['session_id' => session_id()];
		$this->session->set_userdata( $data );

	}

	public function index() 
	{

		$womenProducts 		= $this->Home_model->getWomenProducts();
		$bestSeller 		= $this->Home_model->bestSeller();
		$menProducts 		= $this->Home_model->getMenProducts();
		$womenCategories 	= $this->Home_model->getCategories(2);
		$menCategories 		= $this->Home_model->getCategories(1);
		$getCollection 		= $this->HF_model->getCollection();
		$coming_soon_product = $this->Home_model->coming_soon_product();

		if ($coming_soon_product) {

			$countTime 	=  strtotime($coming_soon_product->coming_soon_time);
			if ( time() >  $countTime ) {
				$this->Home_model->update_coming_soon($coming_soon_product->product_id);
			}
		}
		
		$data 	= [
				'womenProducts' => $womenProducts, 
				'getCollection' => $getCollection, 
				'menProducts' 	=> $menProducts,
				'womenCat'		=> $womenCategories,
				'menCat'		=> $menCategories,
				'bestSeller'	=> $bestSeller,
				'coming_soon_product' => $coming_soon_product
			] ;
		$this->layout->setSiteTitle('Home');
		$this->layout->view_layout('index', $data);
	}
	
}


 ?>