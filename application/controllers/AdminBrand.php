<?php
defined('BASEPATH') or exit('No direct script access allowed');

class AdminBrand extends CI_Controller
{

  function __construct()
  {
    parent::__construct();
    $this->load->model('AdminBrand_model');
    $this->logged_in();
  }
  private function logged_in()
  {
    if (!$this->session->userdata('admin_name')) {
      redirect('admin-login');
    }
  }

  public function index()
  {
    $all = $this->AdminBrand_model->getAll();
    $this->layout->setSiteTitle('-Categories');
    $this->layout->admin_layout('admin/show_brand');
  }

  public function brandView()
  {
    if ($this->input->post('brand_view') && $this->input->post('brand_view') == 'get_all') {
      $brands = $this->AdminBrand_model->getAll();

      echo json_encode($brands);
    } else {
      redirect('AdminBrand');
    }
  }

  public function add_brand()
  {
    if ($this->input->post('action') && $this->input->post('action') == 'add_brand') {
      $d['brand_name'] = $this->input->post('brand_name');
      $d['status'] = $this->input->post('status');
      $addBrand = $this->AdminBrand_model->add($d);

      if ($addBrand) {
        $response['status'] = 'success';
        $response['msg'] = 'Brand add Successfully!';
      } else {
        $response['status'] = 'warning';
        $response['msg'] = 'Brand add not Successfully!';
      }
      echo json_encode($response);
    } else {
      redirect('AdminBrand');
    }
  }

  public function getUpdate()
  {
    if ($this->input->post('action') && $this->input->post('action') == 'updateBrand') {
      $id = $this->input->post('id');
      $data = $this->AdminBrand_model->getUpdateData($id);
      echo json_encode($data);
    } else {
      redirect('AdminBrand');
    }
  }

  public function updateBrand()
  {
    if ($this->input->post('action') && $this->input->post('action') == 'update_brand') {
      $id = $this->input->post('brand_id');
      $d['brand_name'] = $this->input->post('brand_name');
      $d['status'] = $this->input->post('status');
      $result = $this->AdminBrand_model->update($id, $d);
      if ($result) {
        $response['status'] = 'success';
        $response['msg'] = 'Brand add Successfully!';
      } else {
        $response['status'] = 'warning';
        $response['msg'] = 'Brand add not Successfully!';
      }
      echo json_encode($d);
    } else {
      redirect('AdminBrand');
    }
  }
}
