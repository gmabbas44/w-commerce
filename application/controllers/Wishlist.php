<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

class Wishlist extends CI_Controller
{
	private $user_id, $sid;
	function __construct()
	{
		parent::__construct();
		$this->load->model( 'Wishlist_model' );
		$this->user_id = $this->session->userdata('user_id');
		$this->sid = $this->session->userdata('session_id');
		$this->session->set_userdata('request_url', trim($_SERVER['PATH_INFO'], '/'));
	}

	private function loggedIn(){
		if ( ! $this->session->userdata('username') ) {
			redirect('login');
		}
	}

	public function index()
	{
		$this->loggedIn();
		$this->layout->setSiteTitle( 'Shop' );
		$this->layout->view_layout( 'wishlist');
	}

	public function add_wishlist()
	{
		if ( $this->input->post('action') && $this->input->post('action') == 'wishlist' ) {
			
			if ( $this->session->userdata('username') ) 
			{
				$product_id 	= decript_id($this->input->post('id'));

				$check_wishlist = $this->Wishlist_model->check_wishlist($product_id, $this->user_id);

				if ( $check_wishlist ) 
				{
					$response['status'] = 'warning';
					$response['message'] = "Opps! Already Added in Wishlist!" ;
				}else {
					$getProductinfo = $this->Wishlist_model->getProductinfo($product_id);

					$data['user_id'] 		= $this->user_id;
					$data['product_id'] 	= $product_id;
					$data['product_title'] 	= $getProductinfo->title;
					$data['product_img'] 	= $getProductinfo->img_name;
					$data['mrp']			= $getProductinfo->mrp;


					$insert_wishlist = $this->Wishlist_model->add_wishlist( $data );

					if ( $insert_wishlist ) {
						$response['status'] = 'success';
						$response['message'] = "Added Wishlist successfully!" ;
					} else {
						$response['status'] = 'warning';
						$response['message'] = "Opps! Something wrong!" ;
					}
				}

			} else {
				$response['status'] = 'nologin';
			}
			echo json_encode( $response );
		} else {
			redirect(''); 
		}
	}

	public function wishlistCount()
	{
		if ( $this->input->post('action') && $this->input->post('action') == 'wishlistCount' && $this->session->userdata('username') ) 
		{
			echo $this->Wishlist_model->wishlistCount($this->user_id);
			 
		} else{
			redirect('');
		}
	}

	public function show_wishlist()
	{
		if ( $this->input->post('action') && $this->input->post('action') == 'show_wishlist' ) {
			$getCustomerWishlist = $this->Wishlist_model->getCustomerWishlist( $this->user_id );
			echo json_encode( $getCustomerWishlist );
		
		} else{
			redirect('');
		}
	}

	public function del_wishlist()
	{
		if ( $this->input->post('action') && $this->input->post('action') == 'del_wishlist' ) {

			$del_id = decript_id( $this->input->post('del_id') );
			
			$del_wishlist = $this->Wishlist_model->del_wishlist( $del_id , $this->user_id );

			if ( $del_wishlist ) {
				$response['status'] = 'success';
				$response['msg'] = 'Product successfully delete!';
			} else {
				$response['status'] = 'warning';
				$response['msg'] = 'Product are not deleted!';
			}
			echo json_encode( $response );
		
		} else{
			redirect('wishlist');
		}
	}

	public function balk()
	{
		if ( $this->input->post('action') && $this->input->post('action') == 'balk' ) {

			$id_array = $this->input->post('id');
			$this->load->model( 'Cart_model' );
			
			foreach ( $id_array as $product_id ) {
				$product_id = decript_id( $product_id );
				$balk = $this->input->post('balk');

				switch ( $balk ) {
					case 1:

						$checkaddedPro 	= $this->Cart_model->checkaddedPro( $this->sid , $product_id);

						if ( $checkaddedPro ) 
						{
							$udata_qty = $checkaddedPro->qty + 1;
							$this->Cart_model->updateQty( $checkaddedPro->product_id, $udata_qty, $checkaddedPro->unite_price );
							$this->Cart_model->del_wishlist( $product_id, $this->user_id );
							$response['msg'] = 'Product already added!';
							$response['status'] = "warning";
						} 
						else {
							$product_item = $this->Cart_model->getProductInfo( $product_id );

							$data = [
								'sid'			=> $this->sid,
								'product_id'	=> $product_item->product_id,
					            'picture'       => $product_item->img_name,
					            'product_title' => $product_item->title,
					            'qty'           => 1,
					            'unite_price'   => $product_item->mrp,
					            'total_price'   => $product_item->mrp * 1
							];

							$add = $this->Cart_model->add( $data );
							$this->Cart_model->del_wishlist( $product_id, $this->user_id );
							if ( $add ) {

								$response['msg'] = "Product added successfully!";
								$response['status'] = "success";
							}
							else {
								$response['msg'] = "Product not added successfully!";
								$response['status'] = "warning";
							}
						}

					break;

					case 2:
						$delete = $this->Wishlist_model->del_wish( $this->user_id, $product_id);
						if ( $delete ) {
							$response['status'] = 'success';
							$response['msg'] = 'Product successfully delete!';
						} else {
							$response['status'] = 'warning';
							$response['msg'] = 'Product are not deleted!';
						}
						break;
				}
			}

			echo json_encode( $response );
		} else{
			redirect('wishlist');
		}
	}

}