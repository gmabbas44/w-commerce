<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Collection extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model( 'Collection_model' );
	}
	
	public function index( $id )
	{
		$id = decript_id($id);

		$allProducts = $this->Collection_model->getById( $id );
		$data = ['allProducts' => $allProducts ] ;

		$this->layout->setSiteTitle('Collection');
		$this->layout->view_layout('collection', $data );
	}
}