<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Cart extends CI_Controller
{
  private $sid, $user_id;

  function __construct()
  {
    parent::__construct();
    $this->load->model('Cart_model');
    $this->sid = $this->session->userdata('session_id');
    $this->user_id = $this->session->userdata('user_id');

    $this->session->set_userdata('request_url', trim($_SERVER['PATH_INFO'], '/'));
  }

  private function loggedIn()
  {
    if (!$this->session->userdata('username')) {
      redirect('login');
    }
  }

  public function index()
  {
    // $getCartPro = $this->Cart_model->getCartPro( $this->sid );
    // if ( ! $getCartPro > 0 ){
    // 	redirect('shop');
    // }
    $this->layout->setSiteTitle('Cart');
    $this->layout->view_layout('Cart');
  }

  public function action()
  {
    if ($this->input->post('data_action')) {

      $result = $this->Cart_model->getCartPro($this->sid);

      if (count($result) > 0) {
        $response = $result;
      } else {
        $response = 0;
      }

      echo json_encode($response);
    } else {
      redirect('cart');
    }
  }

  public function addToCart()
  {
    if ($this->input->post('action') && $this->input->post('action') == 'addToCart') {
      $product_id = decript_id($this->input->post('product_id'));
      $qty     = $this->input->post('qty');

      $checkaddedPro   = $this->Cart_model->checkaddedPro($this->sid, $product_id);

      if ($checkaddedPro) {
        $udata_qty = $checkaddedPro->qty + 1;
        $this->Cart_model->updateQty($checkaddedPro->product_id, $udata_qty, $checkaddedPro->unite_price);
        $response['message'] = 'Product already added!';
        $response['status'] = "warning";
      } else {
        $product_item = $this->Cart_model->getProductInfo($product_id);

        $data = [
          'sid'           => $this->sid,
          'product_id'    => $product_item->product_id,
          'picture'       => $product_item->img_name,
          'product_title' => $product_item->title,
          'qty'           => $qty,
          'unite_price'   => $product_item->mrp,
          'total_price'   => $product_item->mrp * $qty
        ];

        $add = $this->Cart_model->add($data);
        $this->Cart_model->del_wishlist($product_id, $this->user_id);
        if ($add) {

          $response['message'] = "Product added successfully!";
          $response['status'] = "success";
        } else {
          $response['message'] = "Product not added successfully!";
          $response['status'] = "warning";
        }
      }
      echo json_encode($response);
    } else {
      redirect('');
    }
  }

  public function InStockPro()
  {
    if ($this->input->post('data_action') && $this->input->post('data_action') == 'update_qty') {

      $response = array(
        'csrfName' => $this->security->get_csrf_token_name(),
        'csrfHash' => $this->security->get_csrf_hash()
      );
      $product_id = $this->input->post('product_id');
      $qty     = $this->input->post('qty');

      $checkInStock = $this->Cart_model->checkInStock($product_id);

      $InStock = $checkInStock->in_stock;
      $mrp = $checkInStock->mrp;

      if ($qty <=  $InStock) {
        $updateQty = $this->Cart_model->updateQty($product_id, $qty, $mrp);
        $response['message'] = "Update product quantity!";
        $response['status'] = "success";
      } else {
        $response['message'] = "Sorry! Product stock in $InStock ";
        $response['status'] = "warning";
      }
      echo json_encode($response);
    } else {
      redirect('cart');
    }
  }

  public function removePro()
  {
    if ($this->input->post('data_action') && $this->input->post('data_action') == 'remove_product') {

      $response = array(
        'csrfName' => $this->security->get_csrf_token_name(),
        'csrfHash' => $this->security->get_csrf_hash()
      );
      $cart_id = $this->input->post('cart_id');
      $rem_pro = $this->Cart_model->removePro($cart_id);

      if ($rem_pro) {
        $response['message'] = "Product remove from cart successfully!";
        $response['status'] = "success";
      } else {
        $response['message'] = "Opps! Product are not remove from cart !";
        $response['status'] = "warning";
      }
      echo json_encode($response);
    } else {
      redirect('cart');
    }
  }

  public function checkOut()
  {
    $this->loggedIn();

    // get shipping information form customer
    $s['districts_id']     = $this->input->post('city');
    $s['shipping_fullname'] = $this->input->post('shipping_fullname');
    $s['shipping_email']   = $this->input->post('shipping_email');
    $s['shipping_phone']   = $this->input->post('shipping_phone');
    $s['shipping_address']   = $this->input->post('shipping_address');

    $this->form_validation->set_rules('shipping_fullname', 'Full name', 'required|trim');
    $this->form_validation->set_rules('city', 'Town / City', 'required');
    $this->form_validation->set_rules('shipping_email', 'Email', 'required|trim|valid_email');
    $this->form_validation->set_rules('shipping_phone', 'Mobile', 'required|trim|min_length[10]|max_length[14]');
    $this->form_validation->set_rules('shipping_address', 'Address', 'required');
    $this->form_validation->set_rules('payment', 'Payment', 'required');

    if ($this->form_validation->run() == false) {

      $data['getDistricts'] = $this->Cart_model->getDistricts();
      $this->layout->setSiteTitle('Check Out');
      $this->layout->view_layout('check-out', $data);
    } else {

      // for shipping information
      $shipping_id = $this->Cart_model->shipping_info($s);

      // for carts total price .
      $tp = $this->Cart_model->sum($this->sid);

      // for shipping cost 
      $s_cost = $this->Cart_model->getShippingCost($s['districts_id']);

      // vat count
      $vat = $tp->total_price * .5;

      // get order information for server
      $order['user_id']        = $this->session->userdata('user_id');
      $order['shipping_id']    = $shipping_id;
      $order['payment_type']  = $this->input->post('payment');
      $order['vat']            = $vat;
      $order['shipping_cost']  = $s_cost->shipping_cost;
      $order['total_price']    = $tp->total_price;
      $order['order_status']  = 'Pending';

      $ord_id = $this->Cart_model->order($order);

      $this->session->set_userdata('ord_id', $ord_id);

      // for orders information 
      $getCartPro = $this->Cart_model->getCartPro($this->sid);

      foreach ($getCartPro as $cartPro) {

        $data['product_id'] = $cartPro->product_id;
        $data['qty'] = $cartPro->qty;
        $data['price'] = $cartPro->total_price;
        $data['order_id'] = $ord_id;

        $this->Cart_model->order_details($data);
        $this->Cart_model->update_product_qty($cartPro->product_id, $cartPro->qty);
      }
      $delete = $this->Cart_model->deleteCartProduct($this->sid);

      if ($delete) {
        redirect('invoice');
      }
    }
  }

  public function getShippingCost()
  {
    $district_id = $this->input->post('district_id');
    echo json_encode($this->Cart_model->getShippingCost($district_id));
  }

  public function countQtyPrice()
  {
    if ($this->input->post('data_action') && $this->input->post('data_action') == 'count') {

      $response = array(
        'csrfName' => $this->security->get_csrf_token_name(),
        'csrfHash' => $this->security->get_csrf_hash()
      );
      $qty = 0;
      $total_price = 0;
      $result = $this->Cart_model->countCartPro($this->sid);
      foreach ($result as $row) {
        $qty += $row->qty;
        $total_price += $row->total_price;
      }
      $response['qty'] = $qty;
      $response['total_price'] = $total_price;
      echo json_encode($response);
    } else {
      redirect('');
    }
  }

  public function order()
  {

    if (!empty($this->input->post())) {
      dd($this->input->post());
    } else {
      redirect('cart');
    }
  }

  public function getOrderCart()
  {
    if ($this->input->post('get_order') == 'get_order_cart') {
      $data = $this->Cart_model->getCartPro($this->sid);
      echo json_encode($data);
    } else {
      redirect('cart');
    }
  }

  public function invoice()
  {
    $this->loggedIn();
    //echo $this->user_id;
    $ord_id = $this->session->userdata('ord_id');

    if (!$ord_id) redirect('Shop');

    $order = $this->Cart_model->order_info($ord_id);
    //dd($order);

    $data['order_info'] = $order;

    $data['customer_info'] = $this->Cart_model->get_customer_info($order->user_id);
    $data['shipping_info'] = $this->Cart_model->get_shipping_info($order->shipping_id);
    $data['order_details'] = $this->Cart_model->get_order_details($order->ord_id);

    $this->layout->setSiteTitle('Invoice');
    $this->layout->view_layout('invoice', $data);
  }
}
