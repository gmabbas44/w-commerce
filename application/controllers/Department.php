<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Department extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model( 'Department_model' );
	}

	public function index( $id )
	{
		$id = decript_id( $id );
		$allProducts = $this->Department_model->getDepById( $id );
		$data = ['allProducts' => $allProducts ];

		$this->layout->setSiteTitle( 'Department' );
		$this->layout->view_layout( 'department', $data );

	}
}