<?php
defined('BASEPATH') or exit('No direct script access allowed');

class adminProduct extends CI_Controller
{

	function __construct()
	{
		parent::__construct();
		$this->load->model('AdminProduct_model');
		$this->logged_in();
	}
	private function logged_in()
	{
		if (!$this->session->userdata('admin_name')) {
			redirect('admin-login');
		}
	}

	public function index()
	{
		$allProduct = $this->AdminProduct_model->allProduct();
		$data = ['allProduct' => $allProduct];
		$this->layout->setSiteTitle('-Products');
		$this->layout->admin_layout('admin/show_product', $data);
	}

	private function set_upload_options()
	{
		//upload an image options
		$config['upload_path'] = './assets/img/products/';
		$config['allowed_types'] = 'jpg|png|jpeg';
		$config['encrypt_name'] = TRUE;

		return $config;
	}

	private function imageUpload($image_name)
	{
		// $config['upload_path'] = base_url('assets/img/products/');
		$config['upload_path'] = './assets/img/products/';
		$config['allowed_types'] = 'jpg|png|jpeg';
		$config['encrypt_name'] = TRUE;

		$this->load->library('upload', $config);

		if ($this->upload->do_upload($image_name)) {
			$data = $this->upload->data();

			return $data['file_name'];
		} else {
			$error = $this->upload->display_errors();
			print_r($error);
		}
	}

	public function add_product()
	{
		$categories = $this->AdminProduct_model->all('categories');
		$brand 		= $this->AdminProduct_model->all('brand');
		$department = $this->AdminProduct_model->all('department');
		$collection = $this->AdminProduct_model->all('collection');


		$data = ['categories' => $categories, 'brand' => $brand, 'department' => $department, 'collection' => $collection];
		$d['cat_id'] 			= $this->input->post('category');
		$d['brand_id'] 			= $this->input->post('brand');
		$d['dep_id'] 			= $this->input->post('department');
		$d['coll_id'] 			= $this->input->post('collection');
		$d['title'] 			= $this->input->post('title');
		$d['short_description'] = $this->input->post('short_description');
		$d['product_type'] 		= $this->input->post('product_type');
		$d['in_stock'] 			= $this->input->post('in_stock');
		$d['mrp'] 				= $this->input->post('mrp');
		$d['price'] 			= $this->input->post('price');
		$d['is_active'] 		= $this->input->post('is_active');
		$d['description'] 		= $this->input->post('description');


		$this->form_validation->set_rules('category', 'Category', 'required');
		$this->form_validation->set_rules('brand', 'Brand', 'required');
		$this->form_validation->set_rules('department', 'Department', 'required');
		$this->form_validation->set_rules('collection', 'Collection', 'required');
		$this->form_validation->set_rules('title', 'Title', 'required');
		$this->form_validation->set_rules('short_description', 'Short description', 'required');
		$this->form_validation->set_rules('product_type', 'Product type', 'required');
		$this->form_validation->set_rules('mrp', 'In stock', 'required');
		$this->form_validation->set_rules('price', 'Price', 'required');
		$this->form_validation->set_rules('is_active', 'Is active', 'required');
		$this->form_validation->set_rules('description', 'Description', 'required');

		if ($this->form_validation->run() == false) {
			$this->layout->setSiteTitle('-Add-Product');
			$this->layout->admin_layout('admin/add_product', $data);
		} else {

			$d['image'] = $this->imageUpload('userfile');

			$count  = count($_FILES['gellery_images']['name']);

			$last_id = $this->AdminProduct_model->add_product($d);

			if ($last_id) {
				$files = $_FILES;

				if ($count > 0) {

					for ($i = 0; $i < $count; $i++) {
						$_FILES['gellery_image']['name'] = $files['gellery_images']['name'][$i];
						$_FILES['gellery_image']['type'] = $files['gellery_images']['type'][$i];
						$_FILES['gellery_image']['tmp_name'] = $files['gellery_images']['tmp_name'][$i];
						$_FILES['gellery_image']['error'] = $files['gellery_images']['error'][$i];
						$_FILES['gellery_image']['size'] = $files['gellery_images']['size'][$i];

						$this->upload->initialize($this->set_upload_options());
						$this->upload->do_upload('gellery_image');
						$filename = $this->upload->data();
						// $d['images'] .= $filename['file_name'];
						$img_data['img_name'] = $filename['file_name'];
						$img_data['product_id'] = $last_id;
						$this->AdminProduct_model->gellery_image($img_data);
					}
				}
			}
			redirect('adminProduct');
		}
	}

	// admin update product 
	public function deleteProduct($id)
	{
		echo $id;
	}
	// admin delete product 
	public function updateProduct($id)
	{

		$product 		=  $this->AdminProduct_model->getProductById($id);
		$categories = $this->AdminProduct_model->all('categories');
		$brand 			= $this->AdminProduct_model->all('brand');
		$department = $this->AdminProduct_model->all('department');
		$collection = $this->AdminProduct_model->all('collection');

		$d['cat_id'] 			= $this->input->post('category');
		$d['brand_id'] 		= $this->input->post('brand');
		$d['dep_id'] 			= $this->input->post('department');
		$d['coll_id'] 		= $this->input->post('collection');
		$d['title'] 			= $this->input->post('title');
		$d['short_description'] = $this->input->post('short_description');
		$d['product_type'] 		= $this->input->post('product_type');
		$d['in_stock'] 			= $this->input->post('in_stock');
		$d['mrp'] 				= $this->input->post('mrp');
		$d['price'] 			= $this->input->post('price');
		$d['is_active'] 		= $this->input->post('is_active');
		$d['description'] 		= $this->input->post('description');

		$this->form_validation->set_rules('category', 'Category', 'required');
		$this->form_validation->set_rules('brand', 'Brand', 'required');
		$this->form_validation->set_rules('department', 'Department', 'required');
		$this->form_validation->set_rules('collection', 'Collection', 'required');
		$this->form_validation->set_rules('title', 'Title', 'required');
		$this->form_validation->set_rules('short_description', 'Short description', 'required');
		$this->form_validation->set_rules('product_type', 'Product type', 'required');
		$this->form_validation->set_rules('in_stock', 'In stock', 'required');
		$this->form_validation->set_rules('mrp', 'MRP', 'required');
		$this->form_validation->set_rules('price', 'Price', 'required');
		$this->form_validation->set_rules('is_active', 'Is active', 'required');
		$this->form_validation->set_rules('description', 'Description', 'required');

		$data = [
			'categories' => $categories, 
			'brand' => $brand, 
			'department' => $department, 
			'collection' => $collection,
			'product' => $product
		];
		

		if ($this->form_validation->run() == false) {
			$this->layout->setSiteTitle('-Update-Product');
			$this->layout->admin_layout('admin/update_product', $data);
		} else {
			$this->AdminProduct_model->updateProduct($id, $d);
			redirect(base_url().'adminProduct');
		}
	}
}
