<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

class Register extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model( 'Register_model' );
	}

	private function logged_in() {

		if ( ! $this->session->userdata('username')) {
			redirect('login');
		}
	}

	public function index()
	{
		if ( $this->session->userdata('username')) {
			redirect('login');
		}

		$this->layout->setSiteTitle( 'Registration' );
		$this->layout->view_layout( 'register');	
	}

	public function singup()
	{
		$formAry['username']	= $this->input->post( 'username' );
		$formAry['email']			= $this->input->post( 'email' );
		$formAry['mobile']		= $this->input->post( 'mobile' );
		$formAry['password'] 	= sha1( $this->input->post( 'password' ) );
		$formAry['ip']				= $this->input->ip_address();
		$formAry['city']			= 47;

		$this->form_validation->set_rules( 'username', 'Username', 'required|trim|is_unique[user.username]|min_length[4]' );
		$this->form_validation->set_rules( 'email', 'Email', 'required|trim|valid_email|is_unique[user.email]' );
		$this->form_validation->set_rules( 'mobile', 'Mobile', 'required|trim|min_length[10]|max_length[14]' );
		$this->form_validation->set_rules( 'password', 'Password', 'required|alpha_numeric|min_length[8]' );
		$this->form_validation->set_rules( 'conf_pass', 'Confirm Password', 'required|alpha_numeric|min_length[8]|matches[password]' );

		if ( $this->form_validation->run() == false ) 
		{
			if ( $this->session->userdata('username')) {
				redirect('login');
			}
			$this->layout->setSiteTitle( 'Registration' );
			$this->layout->view_layout( 'register');	
		}
		else 
		{
			$last_id = $this->Register_model->singupModel( $formAry );

			if ( $last_id ) 
			{
				$this->session->set_flashdata( 'singup', 'You have register successfully! please <a class="font-weight-bold" href="'.base_url().'login">Login!</a>' );
				redirect( 'register' );	
			}
		}
	}

	public function getCustomerInfo()
	{
		if ($this->session->userdata( 'username' ) && $this->input->post( 'user_id' )) {
			$user_id = $this->input->post( 'user_id' );
			$response = $this->Register_model->getCustomerInfo( $user_id );
			echo json_encode( $response );
		} else {
			redirect('');
		}
	}
	public function login()
	{
		if ( $this->session->userdata('username')) {
			redirect('customer-profile');
		}

		$this->layout->setSiteTitle( 'Login' );
		$this->layout->view_layout( 'login');	

		$user 		= $this->security->xss_clean( $this->input->post( 'user' ) );
		$password 	= $this->security->xss_clean( $this->input->post( 'password' ) );

		$this->form_validation->set_rules( 'user', 'Username Or Email', 'required|trim' ) ;
		$this->form_validation->set_rules( 'password', 'Password', 'required|trim' ) ;

		if ( $this->form_validation->run() == true ) {
			$password = sha1( $password );
			$row = $this->Register_model->singIn( $user, $password );

			$data = ['username' => $row->username, 'user_id' => $row->user_id];
			if ( $row  ) {
				$this->session->set_userdata( $data );
				redirect( $this->session->userdata('request_url') );
				// redirect('');
			} else {
				redirect( 'login' );
			}
		} 
	}

	public function customerProfile()
	{
		$this->logged_in();
		$customer_id = $this->session->userdata('user_id');
		$data['customer_info'] = $this->Register_model->customerProfile($customer_id);

		$this->layout->setSiteTitle( 'User Profile' );
		$this->layout->view_layout( 'user-profile', $data );
	}

	public function logout()
	{
		// if ( $this->session->userdata('username') ) {
			$this->session->sess_destroy();
			redirect('');
		//}
	}


}

 ?>