<?php
defined('BASEPATH') or exit('No direct script access allowed');

class AdminDepartment extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('AdminDepartment_model');
		$this->logged_in();
	}
	private function logged_in()
	{
		if (!$this->session->userdata('admin_name')) {
			redirect('admin-login');
		}
	}

	public function index()
	{
		$this->layout->setSiteTitle('-Department');
		$this->layout->admin_layout('admin/show_department');
	}

	public function depView()
	{
		if ($this->input->post('action') && $this->input->post('action') == 'get_all') {
			$all = $this->AdminDepartment_model->getAll();
			echo json_encode($all);
		} else {
			redirect('AdminDepartment');
		}
	}

	public function add_department()
	{
		if ($this->input->post('action') && $this->input->post('action') == 'add_department') {
			$d['dep_name'] = $this->input->post('dep_name');
			$d['status'] = $this->input->post('status');
			$add = $this->AdminDepartment_model->add($d);

			if ($add) {
				$response['status'] = 'success';
				$response['msg'] = 'Department add Successfully!';
			} else {
				$response['status'] = 'warning';
				$response['msg'] = 'Department add not Successfully!';
			}
			echo json_encode($response);
		} else {
			redirect('AdminDepartment');
		}
	}
	public function getUpdate()
	{
		if ($this->input->post('action') && $this->input->post('action') == 'updateDepartment') {
			$id = $this->input->post('id');
			$data = $this->AdminDepartment_model->getUpdateData($id);
			echo json_encode($data);
		} else {
			redirect('AdminDepartment');
		}
	}

	public function update_department()
	{
		if ($this->input->post('action') && $this->input->post('action') == 'update_department') {
			$id = $this->input->post('dep_id');
			$d['dep_name'] = $this->input->post('dep_name');
			$d['status'] = $this->input->post('status');
			$data = $this->AdminDepartment_model->update($id, $d);
			if ($data) {
				$response['status'] = 'success';
				$response['msg'] = 'Department Update Successfully!';
			} else {
				$response['status'] = 'warning';
				$response['msg'] = 'Department Update not Successfully!';
			}
			echo json_encode($response);
		} else {
			redirect('AdminDepartment');
		}
	}
}
