<?php 


class AdminOrder extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
    $this->load->model('AdminOrder_model');
    $this->logged_in();
	}
	private function logged_in() {

		if ( ! $this->session->userdata('admin_name')) {
			redirect('admin-login');
		}
  }

  public function index()
  {
    $data['data'] = $this->AdminOrder_model->getOrder();
    $this->layout->setSiteTitle('-Order');
		$this->layout->admin_layout('admin/order', $data );
  }

  public function viewOrderDetails($ord_id)
  {
    $this->load->model('Cart_model');

    $getOrderById = $this->AdminOrder_model->getOrderById($ord_id);
	
		$data['order_info'] = $getOrderById;

		$data['customer_info'] = $this->Cart_model->get_customer_info($getOrderById->user_id);
		$data['shipping_info'] = $this->Cart_model->get_shipping_info($getOrderById->shipping_id);
		$data['order_details'] = $this->Cart_model->get_order_details($ord_id);
    
    $this->layout->setSiteTitle('-Order-Details');
		$this->layout->admin_layout('admin/orderDetail', $data);
  }

  public function viewOrder()
  {
    if( $this->input->post('action') && $this->input->post('action') == 'get_all') {
      $result = $this->AdminOrder_model->getOrder();
      echo json_encode($result);
    } else {
      redirect('AdminOrder');
    }
  }
  
}