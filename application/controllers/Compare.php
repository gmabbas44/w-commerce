<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

class Compare extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model( 'Compare_model' );
		$this->user_id = $this->session->userdata('user_id');
		$this->session->set_userdata('request_url', trim($_SERVER['PATH_INFO'], '/'));
	}

	private function loggedIn(){
		if ( ! $this->session->userdata('username') ) {
			redirect('login');
		}
	}

	public function index( $cat_id )
	{
		$cat_id = decript_id($cat_id);
		$category_products = $this->Compare_model->category_products( $cat_id );
		$data = ['category_products' => $category_products];

		$this->layout->setSiteTitle('Compare');
		$this->layout->view_layout( 'compare', $data );
	}



}