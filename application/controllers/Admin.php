<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('Admin_model');
		//$this->logged_in();
	}

	private function logged_in() {

		if ( ! $this->session->userdata('admin_name')) {
			redirect('admin-login');
		}
	} 
	public function logout()
	{
		$this->session->sess_destroy();
		redirect('admin-login');
	}

	public function index()
	{
		$this->logged_in();

		$this->layout->admin_layout('admin/index');
		
	}
	public function login()
	{
		if ($this->session->userdata('admin_name')) {
			redirect('admin');
		}
		$email 		= $this->security->xss_clean( $this->input->post( 'email' ) );
		$password 	= $this->security->xss_clean( $this->input->post( 'password' ) );

		$this->form_validation->set_rules( 'email', 'Email', 'required|trim|valid_email' );
		$this->form_validation->set_rules( 'password', 'Password', 'required' );

		if ( $this->form_validation->run() == false ) {
			$this->load->view('admin/login');
		}else {
			$row = $this->Admin_model->admin_login( $email, $password );
			if ( $row ) {

				$s_data = ['admin_id' => $row->admin_id, 'admin_name' => $row->name, 'admin_email' => $row->emial ];
				$this->session->set_userdata( $s_data );
				redirect('admin');
			} else{
				redirect('admin-login');
			}
		}
	}

	public function categories() 
	{
		$this->logged_in();
		$this->layout->setSiteTitle('-Categories');
		$this->layout->admin_layout('admin/show_categories');
	}

	public function cat_view()
	{
		if ( $this->input->post('get_cat') && $this->input->post('get_cat') == 'get_all') {
			$all_cat = $this->Admin_model->get_all( 'categories' );
			$output = '';
			$sl = 0;
			foreach ( $all_cat as $cat ) {

				$sl++;
				if ( $cat->status == 0 ) {
					$status = '<i title="Inactive" class="text-warning fa fa-times" aria-hidden="true"> Inactive</i> ';
				} elseif ( $cat->status == 1 ) {
					$status = '<i title="Active" class="text-success fa fa-check" aria-hidden="true"> Active</i> ';
				} else {
					$status = '<i title="Deleted" class="text-danger far fa-trash-alt"> Deleted <i>';
				}

				$output .= '
				<tr>
                	<td>'. $sl .'</td>
	                <td>'. $cat->category_name .'</td>
	                <td>'. $status .'</td>
	                <td width="10%" class="text-center align-middle">
	                    <a href="#" id="" class="btn-update" data-toggle="modal" data-target="#update-box" ><i title="Edit"  class="fas fa-user-edit"></i></a>
	                    <a href="#" id="" class="text-danger delete-btn" title="Delete  "><i class="far fa-trash-alt"></i></i></a>
	                </td>
	            </tr>';
			}
			echo $output;
		}else {
			redirect('categories');
		}
	}

	public function collection() 
	{
		$this->logged_in();
		$this->layout->setSiteTitle('-Collection');
		$this->layout->admin_layout('admin/show_collection');
	}

	public function add_product()
	{
		$this->logged_in();
		$this->layout->setSiteTitle('-Add-Product');
		$this->layout->admin_layout('admin/add_product');
	}


}
