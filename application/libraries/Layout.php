<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 *	for custom layout
 */
class Layout 
{
	
	private $_CI;
	public $_site_title = '';

	public function __construct() {
		$this->_CI = &get_instance() ;
		$this->_CI->load->model('HF_model');
	}

	public function setSiteTitle($title)
	{
		$this->_site_title = $title;
	}
	public function view_layout($view_name, $params = [] )
	{
		$this->header();
		$this->_CI->load->view( $view_name, $params );
		$this->footer();
	}
	private function header() 
	{
		$getCollection = $this->_CI->HF_model->getCollection();
		$getDepartment = $this->_CI->HF_model->getDepartment();
		$data = ['getCollection' => $getCollection,  'getDepartment' => $getDepartment, 'site_title' => $this->_site_title] ;
		$this->_CI->load->view( 'inc/header' ,  $data );
	}
	private function footer() 
	{
		$this->_CI->load->view( 'inc/footer' );
	}

	public function admin_layout( $view_name, $params = [] )
	{
		$this->admin_header();
		$this->_CI->load->view( $view_name, $params );
		$this->admin_footer();
	}
	private function admin_header()
	{
		$this->_CI->load->view('admin/inc/admin_header', ['site_title' => $this->_site_title]);
	}
	private function admin_footer()
	{
		$this->_CI->load->view('admin/inc/admin_footer');
	}

}