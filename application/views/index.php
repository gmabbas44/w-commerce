

<!-- Hero Section Begin -->
<section class="hero-section">
    <div class="hero-items owl-carousel">
        <div class="single-hero-items set-bg" data-setbg="<?= ASSETS ?>img/hero-1.jpg">
            <div class="container">
                <div class="row">
                    <div class="col-lg-5">
                        <span>Bag,kids</span>
                        <h1>Black friday</h1>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor
                            incididunt ut labore et dolore</p>
                        <a href="#" class="primary-btn">Shop Now</a>
                    </div>
                </div>
                <div class="off-card">
                    <h2>Sale <span>50%</span></h2>
                </div>
            </div>
        </div>
        <div class="single-hero-items set-bg" data-setbg="<?= ASSETS ?>img/hero-2.jpg">
            <div class="container">
                <div class="row">
                    <div class="col-lg-5">
                        <span>Bag,kids</span>
                        <h1>Black friday</h1>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor
                            incididunt ut labore et dolore</p>
                        <a href="#" class="primary-btn">Shop Now</a>
                    </div>
                </div>
                <div class="off-card">
                    <h2>Sale <span>50%</span></h2>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Hero Section End -->

<!-- Banner Section Begin -->
<div class="banner-section spad">
    <div class="container-fluid">
        <div class="row">

            <?php foreach ($getCollection as $collection) : ?>
            <div class="col-lg-4">
            <a href="<?= base_url('collection/'.encript_id($collection->coll_id) ) ;?>">
                <div class="single-banner">
                    <img src="<?= ASSETS ?>img/collection/<?= $collection->image ;?>" alt="">
                    <div class="inner-text">
                        <h4><?= $collection->name ;?></h4>
                    </div>
                </div>
            </a>
            </div>
            <?php endforeach; ?>

        </div>
    </div>
</div>
<!-- Banner Section End -->

<!-- Women Banner Section Begin -->
<section class="women-banner spad">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-3">
                <div class="product-large set-bg" data-setbg="<?= ASSETS ?>img/products/women-large.jpg">
                    <h2>Women’s</h2>
                    <a href="#">Discover More</a>
                </div>
            </div>
            <div class="col-lg-8 offset-lg-1">
                <div class="filter-control">
                    <ul>
                        <li class="active womenfilter" data-filter="all">All</li>
                        <?php foreach ($womenCat as $category) { ?> 
                            <li class="womenfilter" data-filter="<?= str_replace(["'"," "],'-',$category->category_name) ; ?>"><?= $category->category_name ; ?></li>
                        <?php } ?>
                    </ul>
                </div>
                <div class="product-slider owl-carousel">
                    <?php 

                        foreach ( $womenProducts as $womenProduct ):
                    ?>
                    <div class="product-item womenProduct <?= str_replace(["'"," "],'-',$womenProduct->category_name) ;?>">
                        <div class="pi-pic">
                            <a href="<?= base_url().'product-details/'.encript_id($womenProduct->product_id) ; ?>"><img src="<?= ASSETS ?>img/products/<?= $womenProduct->img_name ?>" alt=""></a>
                            <div class="sale pp-sale">Sale</div>
                            <div class="discount"><?= discount( $womenProduct->price, $womenProduct->mrp );?>%</div>
                            <div class="icon wishlist" id="<?= encript_id($womenProduct->product_id) ; ?>">
                                <i class="icon_heart_alt"></i>
                            </div>
                            <ul>
                                <li class="w-icon active"><a 
                                    productId="<?= encript_id($womenProduct->product_id); ?>"
                                    class="addToCart" href="#"><i class="icon_bag_alt"></i></a></li>

                                <li class="quick-view" id="<?= encript_id($womenProduct->product_id) ;?>"><a href="#" data-toggle="modal" data-target="#product_details">+ Quick View</a></li>
                                <li class="w-icon"><a title="Add to compare" class="compare" href="<?= base_url('compare/'.encript_id($womenProduct->cat_id))?>"><i class="fa fa-random"></i></a></li>
                            </ul>
                        </div>
                        <div class="pi-text">
                            <div class="catagory-name"><?= $womenProduct->category_name ;?></div>
                            <a href="#">
                                <h5><?= $womenProduct->title; ?></h5>
                            </a>
                            <div class="product-price">
                                $<?= $womenProduct->mrp; ?>
                                <span>$<?= $womenProduct->price; ?></span>
                            </div>
                        </div>
                    </div>

                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Women Banner Section End -->

<!-- Deal Of The Week Section Begin-->
<?php 
    
    if ( $coming_soon_product  ) : 
?>

<section class="deal-of-week set-bg spad" data-setbg="">
    <div class="container">
        <div class="row">
            <div class="col-md-6 text-center mb-5">
                <div class="section-title">
                    <h2>Coming Soon</h2>
                    <p><?= $coming_soon_product->short_description ; ?></p>
                    <div class="product-price">
                        Tk. <?= $coming_soon_product->mrp ; ?>
                        <span>/ <?= $coming_soon_product->category_name ; ?></span>
                    </div>

                </div>

                <div class="countdown-timer" id="countdown" getTime="<?=$coming_soon_product->coming_soon_time?>">
                    <div class="cd-item">
                        <span>56</span>
                        <p>Days</p>
                    </div>
                    <div class="cd-item">
                        <span>12</span>
                        <p>Hrs</p>
                    </div>
                    <div class="cd-item">
                        <span>40</span>
                        <p>Mins</p>
                    </div>
                    <div class="cd-item">
                        <span>52</span>
                        <p>Secs</p>
                    </div>
                </div>
                <a href="#" class="primary-btn">Order Now</a>
            </div>
            <div class="col-md-6">
                <img src="<?= ASSETS ?>img/products/<?= $coming_soon_product->img_name ; ?>" alt="">
            </div>
        </div>
        
    </div>
</section>
<?php endif ; ?>
<!-- Deal Of The Week Section End -->

<!-- Man Banner Section Begin -->
<section class="man-banner spad">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-8">
                <div class="filter-control">
                    <ul>
                        <li class="active menfilter" data-filter="all">All</li>
                        <?php foreach ($menCat as $category) { ?> 
                            <li class="menfilter" data-filter="<?= str_replace(["'"," "], '-', $category->category_name) ; ?>"><?= $category->category_name ; ?></li>
                        <?php } ?>
                    </ul>
                </div>
                <div class="product-slider owl-carousel">
                    <?php 
                        foreach ( $menProducts as $menProduct ) :
                           
                    ?>
                    <div class="product-item menProduct <?= str_replace(["'"," "], '-', $menProduct->category_name) ; ?>">
                        <div class="pi-pic">
                            <a href="<?= base_url().'product-details/'.encript_id($menProduct->product_id) ; ?>"><img src="<?= ASSETS ?>img/products/<?= $menProduct->img_name ?>" alt=""></a>
                            <div class="sale pp-sale">Sale</div>
                            <div class="discount"><?= discount( $menProduct->price, $menProduct->mrp )?>%</div>
                            <div class="icon wishlist" id="<?= encript_id($menProduct->product_id) ; ?>">
                                <i class="icon_heart_alt"></i>
                            </div>
                            <ul>
                                <li class="w-icon active"><a 
                                    productId="<?= encript_id($menProduct->product_id); ?>" class="addToCart" href="#"><i class="icon_bag_alt"></i></a></li>
                                <li class="quick-view" id="<?= encript_id($menProduct->product_id) ;?>"><a href="#" data-toggle="modal" data-target="#product_details">+ Quick View</a></li>
                                <li class="w-icon"><a title="Add to compare" class="compare" href="<?= base_url('compare/'.encript_id($menProduct->cat_id))?>"><i class="fa fa-random"></i></a></li>
                            </ul>
                        </div>
                        <div class="pi-text">
                            <div class="catagory-name"><?= $menProduct->category_name ;?></div>
                            <a href="#">
                                <h5><?= $menProduct->title; ?></h5>
                            </a>
                            <div class="product-price">
                                $<?= $menProduct->mrp; ?>
                                <span>$<?= $menProduct->price; ?></span>
                            </div>
                        </div>
                    </div>
                    <?php endforeach; ?>
                    
                </div>
            </div>
            <div class="col-lg-3 offset-lg-1">
                <div class="product-large set-bg m-large" data-setbg="<?= ASSETS ?>img/products/man-large.jpg">
                    <h2>Men’s</h2>
                    <a href="#">Discover More</a>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Man Banner Section End -->

<!-- future product -->

<!-- future product end -->

<section class="latest-blog spad">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="section-title">
                    <h2>Feature Product</h2>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="product-slider owl-carousel">
                <div class="product-item">
                    <div class="pi-pic">
                        <img src="<?= ASSETS ?>img/products/women-1.jpg" alt="">
                            <div class="sale pp-sale">Sale</div>
                            <div class="discount">10%</div>
                        <div class="icon">
                            <i class="icon_heart_alt"></i>
                        </div>
                        <ul>
                            <li class="w-icon active"><a href="#"><i class="icon_bag_alt"></i></a></li>
                            <li class="quick-view"><a href="#" data-toggle="modal" data-target="#product_details">+ Quick View</a></li>
                            <li class="w-icon"><a href="#"><i class="fa fa-random"></i></a></li>
                        </ul>
                    </div>
                    <div class="pi-text">
                        <div class="catagory-name">Coat</div>
                        <a href="#">
                            <h5>Pure Pineapple</h5>
                        </a>
                        <div class="product-price">
                            $14.00
                            <span>$35.00</span>
                        </div>
                    </div>
                </div>
                <div class="product-item">
                    <div class="pi-pic">
                        <img src="<?= ASSETS ?>img/products/women-2.jpg" alt="">
                        <div class="icon">
                            <i class="icon_heart_alt"></i>
                        </div>
                        <ul>
                            <li class="w-icon active"><a href="#"><i class="icon_bag_alt"></i></a></li>
                            <li class="quick-view"><a href="#" data-toggle="modal" data-target="#product_details">+ Quick View</a></li>
                            <li class="w-icon"><a href="#"><i class="fa fa-random"></i></a></li>
                        </ul>
                    </div>
                    <div class="pi-text">
                        <div class="catagory-name">Shoes</div>
                        <a href="#">
                            <h5>Guangzhou sweater</h5>
                        </a>
                        <div class="product-price">
                            $13.00
                        </div>
                    </div>
                </div>
                <div class="product-item">
                    <div class="pi-pic">
                        <img src="<?= ASSETS ?>img/products/women-3.jpg" alt="">
                        <div class="icon">
                            <i class="icon_heart_alt"></i>
                        </div>
                        <ul>
                            <li class="w-icon active"><a href="#"><i class="icon_bag_alt"></i></a></li>
                            <li class="quick-view"><a href="#" data-toggle="modal" data-target="#product_details">+ Quick View</a></li>
                            <li class="w-icon"><a href="#"><i class="fa fa-random"></i></a></li>
                        </ul>
                    </div>
                    <div class="pi-text">
                        <div class="catagory-name">Towel</div>
                        <a href="#">
                            <h5>Pure Pineapple</h5>
                        </a>
                        <div class="product-price">
                            $34.00
                        </div>
                    </div>
                </div>
                <div class="product-item">
                    <div class="pi-pic">
                        <img src="<?= ASSETS ?>img/products/women-4.jpg" alt="">
                        <div class="icon">
                            <i class="icon_heart_alt"></i>
                        </div>
                        <ul>
                            <li class="w-icon active"><a href="#"><i class="icon_bag_alt"></i></a></li>
                            <li class="quick-view"><a href="#" data-toggle="modal" data-target="#product_details">+ Quick View</a></li>
                            <li class="w-icon"><a href="#"><i class="fa fa-random"></i></a></li>
                        </ul>
                    </div>
                    <div class="pi-text">
                        <div class="catagory-name">Towel</div>
                        <a href="#">
                            <h5>Converse Shoes</h5>
                        </a>
                        <div class="product-price">
                            $34.00
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- best seller -->
<section class="latest-blog spad">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="section-title">
                    <h2>Best Seller Product</h2>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="product-slider owl-carousel">

            <?php foreach ($bestSeller as $product_item) : ?>
                <div class="product-item">
                    <div class="pi-pic">
                        <a href="<?= base_url().'product-details/'.encript_id($product_item->product_id) ; ?>"><img src="<?= ASSETS ?>img/products/<?= $product_item->img_name ; ?>" alt=""></a>
                            <!-- <div class="sale pp-sale">Sale</div> -->
                            <div class="discount"><?= discount( $product_item->price, $product_item->mrp )?>%</div>
                         <div class="icon wishlist" id="<?= encript_id( $product_item->product_id ) ; ?>">
                                <i class="icon_heart_alt"></i>
                            </div>
                        <ul>
                            <li class="w-icon active"><a 
                                productId="<?= encript_id($product_item->product_id); ?>" class="addToCart" href="#"><i class="icon_bag_alt"></i></a></li>
                            <li class="quick-view" id="<?= encript_id($product_item->product_id) ;?>"><a href="#" data-toggle="modal" data-target="#product_details">+ Quick View</a></li>
                            <li class="w-icon"><a title="Add to compare" class="compare" href="<?= base_url('compare/'.encript_id($product_item->cat_id))?>"><i class="fa fa-random"></i></a></li>
                        </ul>
                    </div>
                    <div class="pi-text">
                        <div class="catagory-name"><?= $product_item->category_name ;?></div>
                            <a href="#">
                                <h5><?= $product_item->title; ?></h5>
                            </a>
                            <div class="product-price">
                                $<?= $product_item->mrp; ?>
                                <span>$<?= $product_item->price; ?></span>
                            </div>
                    </div>
                </div>
            <?php endforeach; ?>

            </div>
        </div>
    </div>
</section>
<!--/ best seller -->

<!-- Latest Blog Section End -->

<script>

    $( document ).ready( function() {

        // CountDown
        setInterval(function() {
            let couwnDown, rel, distance, now, days, hours, minutes, seconds, html ;
            couwnDown = $('.countdown-timer').attr('getTime');
            rel = new Date(couwnDown);
            now = new Date().getTime();
            distance = rel - now;

            days = Math.floor(distance / (1000 * 60 * 60 * 24));
            hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
            minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
            seconds = Math.floor((distance % (1000 * 60)) / 1000);

            html = `<div class="cd-item">
                        <span>${days}</span>
                        <p>Days</p>
                    </div>
                    <div class="cd-item">
                        <span>${hours}</span>
                        <p>Hrs</p>
                    </div>
                    <div class="cd-item">
                        <span>${minutes}</span>
                        <p>Mins</p>
                    </div>
                    <div class="cd-item">
                        <span>${seconds}</span>
                        <p>Secs</p>
                    </div>`;
            $('#countdown').html(html);
            
        }, 1000);

        // product filtering for women
        $('.womenfilter').click(function() {
            var value = $(this).data('filter');

            if ( value == 'all' ) {
                $('.womenProduct').show('1000');
                $('.womenfilter').removeClass('active');
                $(this).addClass('active');
            }else {
                $(".womenProduct").not('.' + value).hide('3000');
                $('.womenProduct').filter('.' + value).show('3000');
                $('.womenfilter').removeClass('active');
                $(this).addClass('active');
            }
        });

        // product filtering for men 
        $('.menfilter').click( function () {
            var value = $(this).data('filter');

            if ( value == 'all' ) {
                $('.menProduct').show('1000');
            } else {
                $('.menProduct').not('.' + value).hide('3000');
                $('.menProduct').filter('.' + value).show('3000');
                $('.menfilter').removeClass('active');
                $(this).addClass('active');
            }
        });

    });

</script>
