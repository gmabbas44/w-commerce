<style>
	.filter-widget .filter-catagories > li a .active{
		color: #e7ab3c;
	}
</style>


<!-- Breadcrumb Section Begin -->
<div class="breacrumb-section">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="breadcrumb-text">
                    <a href="<?= base_url() ?>"><i class="fa fa-home"></i> Home</a>
                    <span>Shop</span>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Breadcrumb Section Begin -->

<!-- Product Shop Section Begin -->
<section class="product-shop spad">
    <div class="container">
        <div class="row">
            
            <div class="col-lg-3 col-md-6 col-sm-8 order-2 order-lg-1 produts-sidebar-filter">
			    <div class="filter-widget">
			        <h4 class="fw-title">Collection</h4>
			        <ul class="filter-catagories">
			        <?php foreach( $getCollection as $collection ) :?>
			            <li><a href="#" class="collection_btn" id="<?= encript_id($collection->coll_id);?>"><?= $collection->name ;?></a></li>
			        <?php endforeach; ?>
			            
			        </ul>
			    </div>
			    <div class="filter-widget">
			        <h4 class="fw-title">Brand</h4>
			        <div class="fw-brand-check">
						<!-- ajax data -->
			        </div>
			    </div>
			    <div class="filter-widget">
			        <h4 class="fw-title">Price</h4>
			        <div class="filter-range-wrap">
			            <div class="range-slider">
			                <div class="price-input">
			                    <input type="text" id="minamount">
			                    <input type="text" id="maxamount">
			                </div>
			            </div>
			            <div class="price-range ui-slider ui-corner-all ui-slider-horizontal ui-widget ui-widget-content"
			                data-min="33" data-max="98">
			                <div class="ui-slider-range ui-corner-all ui-widget-header"></div>
			                <span tabindex="0" class="ui-slider-handle ui-corner-all ui-state-default"></span>
			                <span tabindex="0" class="ui-slider-handle ui-corner-all ui-state-default"></span>
			            </div>
			        </div>
			        <a href="#" class="filter-btn">Filter</a>
			    </div>
			</div>

            <div class="col-lg-9 order-1 order-lg-2">
                <div class="product-show-option">
                    <div class="row justify-content-end">
                        <div class="col-lg-5 col-md-5 text-right">
                            <p>Show 01- 09 Of 36 Product</p>
                        </div>
                    </div>
                </div>
                <div class="product-list">
                    <div class="row" id="shop">
                    
                    <!-- ajax data -->
                        
                    </div>
                </div>
                
            </div>
        </div>
    </div>
</section>
<!-- Product Shop Section End -->

<script>

$( function () {

	let coll_id;
	$('body').on('click', '.collection_btn', function( e ) {
		e.preventDefault();

		let coll_id = $(this).attr('id');
		$(this).addClass('active');

		brand();
	});

	$('body').on('click', '.brand_btn', function( e ) {
		e.preventDefault();

		let brand_id = $(this).val();

		product_list( coll_id = null, brand_id );
		
	});


	function product_list( coll_id = null , brand_id = null ) {

		let html = '', i = 0;

		$.ajax({
			url 	: '<?= base_url('shop/product') ; ?>',
			method 	: 'POST',
			data 	: { action : 'getProduct', coll_id : coll_id, brand_id : brand_id },
			success : function ( response ) {
				data = JSON.parse( response );
				// console.log(data);

				for ( i; i < data.length; i++) {
					html += `
						<div class="col-lg-4 col-sm-6">
                            <div class="product-item">
                                <div class="pi-pic">
                                    <a href="<?= base_url('product-details/')?>${encript(data[i].product_id) }"><img src="<?= ASSETS ;?>img/products/${data[i].img_name}" alt=""></a>
                                    <div class="sale pp-sale">Sale</div>
                                    <div class="discount">${discount( data[i].price, data[i].mrp )}%</div>
                                    <div class="icon wishlist" id="${encript( data[i].product_id )}">
                                        <i class="icon_heart_alt"></i>
                                    </div>
                                    <ul>
                                        <li class="w-icon active">
                                        <a 
                                        productId="${encript( data[i].product_id )}" class="addToCart" href="#"><i class="icon_bag_alt"></i></a></li>
                                        <!-- <li class="w-icon active"><a href="#"><i class="icon_bag_alt"></i></a></li> -->
                                        <li class="quick-view" id="${encript(data[i].product_id) }"><a href="#" data-toggle="modal" data-target="#product_details">+ Quick View</a></li>
                                        <li class="w-icon"><a title="Add to compare" class="compare" href="<?= base_url('compare/')?>${encript( data[i].cat_id )}"><i class="fa fa-random"></i></a></li>
                                    </ul>
                                </div>
                                <div class="pi-text">
                                    <div class="catagory-name">${data[i].category_name}</div>
                                    <a href="#">
                                        <h5>${data[i].title}</h5>
                                    </a>
                                    <div class="product-price">
                                        $${data[i].mrp}
                                        <span>$${data[i].price}</span>
                                    </div>
                                </div>
                            </div>
                        </div>
					`;
				}

				$('#shop').html(html);

			}
			
		});
	}
	product_list( );
	
	function brand() {
		let html = '', i = 0, str, b_name;
		
		$.ajax({
			url 	: '<?= base_url('shop/brand') ; ?>',
			method 	: 'POST',
			data 	: { action : 'fetch_brand' },
			success	: function ( response ) {
				data = JSON.parse( response );

				for (i ; i < data.length; i++ ) {
					str = data[i].brand_name;
					b_name = str.replace(' ', '_');

					html += `
						<div class="bc-item">
			                <label for="${b_name}">
			                    ${data[i].brand_name}
			                    <input class="brand_btn" value="${encript( data[i].product_id )}" type="checkbox" id="${b_name}">
			                    <span class="checkmark"></span>
			                </label>
			            </div>
					`;
				}
				$('.fw-brand-check').html(html);
			}
		});
	}


});



</script>

