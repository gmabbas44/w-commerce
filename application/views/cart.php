<style>
  .btn-warning:hover {
    background-color: #252525;
    color: #fff;
  }
</style>

<!-- Breadcrumb Section Begin -->
<div class="breacrumb-section">
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <div class="breadcrumb-text product-more">
          <a href="<?= base_url(''); ?>"><i class="fa fa-home"></i> Home</a>
          <a href="<?= base_url('shop'); ?>">Shop</a>
          <span>Shopping Cart</span>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Breadcrumb Section Begin -->

<!-- Shopping Cart Section Begin -->
<section class="shopping-cart spad">
  <div class="container">
    <div class="row">
      <div class="col-lg-12">

        <!-- <h3 class="text-center text-warning" style="margin-top:50px;">Loading....</h3> -->
        <div class="cart-table">
          <table>
            <thead>
              <tr>
                <th>SL</th>
                <th>Image</th>
                <th class="p-name">Product Name</th>
                <th>Price</th>
                <th>Quantity</th>
                <th>Total</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody id="cart_product">

            </tbody>
          </table>
        </div>
        <div class="row" id="total_price">
          <!-- received json data; -->

        </div>

        <div class="row" id="total">
        </div>
      </div>
    </div>
  </div>
</section>
<!-- Shopping Cart Section End -->
<script>
  $(document).ready(function() {

    let csrfName = '<?php echo $this->security->get_csrf_token_name(); ?>',
      csrfHash = '<?php echo $this->security->get_csrf_hash(); ?>';

    function fetch_cart() {
      let i = 0,
        sl = 0,
        cart_product = '',
        cart_product_total = '',
        sum = 0,
        qty = 0,
        vat, total;
      $.ajax({
        url: '<?= base_url('cart/action'); ?>',
        method: 'POST',
        data: {
          data_action: 'fetch_cart'
        },
        success: function(response) {
          data = JSON.parse(response);
          if (data) {
            for (i; i < data.length; i++) {
              sl++;
              sum = parseFloat(sum) + parseFloat(data[i].total_price);
              qty = parseInt(qty) + parseInt(data[i].qty);
              cart_product +=
                `<tr>
                    <td>${sl}</td>
                    <td class="cart-pic first-row"><img src="<?= ASSETS ?>img/products/${data[i].picture}" alt="${data[i].product_title}"></td>
                    <td class="cart-title first-row">
                        <h5 class="title">${data[i].product_title}</h5>
                    </td>
                    <td class="p-price first-row">${data[i].unite_price}</td>
                    <td class="qua-col first-row">
                        <div class="quantity">
                            <div class="pro-qty">
                                <input type="number" min="1" product_title="${data[i].product_title}" product_id="${data[i].product_id}" class="product_qty" value="${data[i].qty}">
                            </div>
                        </div>
                    </td>
                    <td class="total-price first-row">${data[i].total_price}</td>
                    <td class="close-td first-row"><a class="delete_porduct btn btn-warning btn-sm text-white" cart_id="${data[i].cart_id}" href="#"><i class="ti-close"></i></a></td>
                </tr>`;
            }
          } else {
            //cart_product += `<tr><td>Cart Empty please Go to <a href="<?= base_url('shop'); ?>">shop</a></td></tr>;`
            window.location.href = "<?= base_url('shop'); ?>";
          }
          vat = parseFloat(sum) * .05;
          total = parseFloat(sum) + vat;
          cart_product_total = `
            <div class="col-lg-4">
                <div class="cart-buttons">
                    <a href="<?= base_url("shop"); ?>" class="primary-btn continue-shop">Continue shopping</a>
                </div>
            </div>
              <div class="col-lg-4 offset-lg-4">
                <div class="proceed-checkout">
                  <ul>
                      <li class="subtotal">Subtotal <span>$${sum}</span></li>
                      <li class="subtotal">Total Product <span>
                          ${qty}
                      </span></li>
                      <li class="subtotal">Vat (5%) <span>$
                          ${vat}
                      </span></li>
                      <li class="cart-total">Total <span>${total}</span></li>
                  </ul>
                  <a href="<?= base_url("checkout"); ?>" class="proceed-btn">PROCEED TO CHECK OUT</a>
                </div>`;
          $('#cart_product').html(cart_product);
          $('#total').html(cart_product_total);
          // console.log(html);
        }
      });
    }

    fetch_cart();

    $('body').on('change', '.product_qty', function(e) {
      e.preventDefault();
      var qty = $(this).val();
      var product_id = $(this).attr('product_id');
      var title = $(this).attr('product_title');

      $.ajax({
        url: '<?= base_url('cart/InStockPro'); ?>',
        method: 'POST',
        data: {
          data_action: 'update_qty',
          product_id: product_id,
          qty: qty,
          [csrfName]: csrfHash
        },
        success: function(response) {
          data = JSON.parse(response);
          csrfName = data.csrfName;
          csrfHash = data.csrfHash;
          fetch_cart();
          // Swal.fire({
          //     title: title,
          //     text: data.message,
          //     icon: data.status,
          //     confirmButtonText: 'OK',
          //     timer: 1500
          // });

          popup(data.status, data.message);

          countProductAndPrice();
        }
      });
    });

    $('body').on('click', '.delete_porduct', function(e) {
      e.preventDefault();

      var cart_id = $(this).attr('cart_id');
      var title = $('.title').html();
      console.log( cart_id );

      Swal.fire({
        title: title,
        text: "Are you confirm remove from Cart?",
        icon: 'warning',
        showCancelButton: true,
        cancelButtonColor: '#252525',
        confirmButtonText: 'Yes',
        cancelButtonText: 'No'
      }).then((result) => {
        if (result.value) {

          $.ajax({
            url: '<?= base_url('cart/removePro'); ?>',
            method: 'POST',
            data: {
              data_action: 'remove_product',
              cart_id: cart_id,
              [csrfName]: csrfHash
            },
            success: function(response) {
              data = JSON.parse(response);
              csrfName = data.csrfName;
              csrfHash = data.csrfHash;

              Swal.fire({
                title: title,
                text: data.message,
                icon: data.status
              });
              fetch_cart();
              countProductAndPrice();
            }
          });
        } else {
          Swal.fire({
            title: title,
            text: "You Product is not remove from Cart",
            icon: 'error'
          });
        }
      });
    });

    function countProductAndPrice() {
      $.ajax({
        url: '<?= base_url('cart/countQtyPrice') ?>',
        method: 'POST',
        data: {
          data_action: 'count',
          [csrfName]: csrfHash
        },
        success: function(response) {
          data = JSON.parse(response);
          csrfName = data.csrfName;
          csrfHash = data.csrfHash;
          $('#countCartProduct').text(data.qty);
          $('.cart-price').text('$' + parseFloat(data.total_price));
        }
      });
    }
    countProductAndPrice();


  });
</script>