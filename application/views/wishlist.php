<!-- Breadcrumb Section Begin -->
<div class="breacrumb-section">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="breadcrumb-text product-more">
                    <a href="<?= base_url(''); ?>"><i class="fa fa-home"></i> Home</a>
                    <a href="./shop.html">Wishlist</a>
                    <span>View</span>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Breadcrumb Section Begin --> 

<!-- Wishlist Section Begin -->
<section class="shopping-cart spad">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="cart-table">
                    <table>
                        <thead>
                            <tr>
                                <th><input type="checkbox" id="selectAll"></th>
                                <th>Image</th>
                                <th >Product Name</th>
                                <th>Price</th>
                                <th>Add to Cart</th>
                                <th>Remove</th>
                            </tr>
                        </thead>
                        <?= form_open('wishlist', ['id'=> 'wishlist_form']) ; ?>
                        <tbody id="show_wishlist">
                        
                        </tbody>
                    </table>
                </div>
                <div class="row">
                    <div class="col-lg-4">
                        <div class="cart-buttons">
                            <a href="<?= base_url('shop') ;?>" class="primary-btn continue-shop">Continue shopping</a>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="input-group checkout-form">
                            <select id="balkData" class="custom-select rounded-0">
                                <option value="0" selected>Choose...</option>
                                <option value="1">Add to Cart all</option>
                                <option value="2">Delete All</option>
                            </select>
                            <div class="input-group-append">
                                <button id="btn-action" type="submit" class="btn primary-btn rounded-0" type="button">Action</button>
                            </div>
                        </div>
                        
                    </div>
                    <?= form_close() ;?>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Wishlist Section End -->


<script>
$( function () {
    
    function show_wishlist() {
        let html = '', i ;
        
        $.ajax({
            url     : '<?= base_url('show-wishlist'); ?>',
            method  : 'POST',
            data    : {action : 'show_wishlist'},
            success : function ( response ) {
                data = JSON.parse( response );

                for ( i in data ) {
                    
                    html += `
                        <tr>
                            <td><input class="checkbox" type="checkbox" data-id="${encript(data[i].product_id)}" value="${encript(data[i].product_id)}"></td>
                            <td class="cart-pic first-row"><img src="<?= ASSETS . 'img/products/'; ?>${data[i].product_img}" alt=""></td>
                            <td class=" first-row">
                                <h5>${data[i].product_title}</h5>
                            </td>
                            <td class="p-price first-row">Tk. ${data[i].mrp}</td>
                        
                            <td class=" first-row"><a class="btn btn-warning btn-sm text-white addToCart rounded-0" href="#" id="${encript(data[i].product_id)}"><i class="fa fa-cart-plus" aria-hidden="true"></i></a></td>
                            <td class="close-td first-row "><a class="btn btn-warning btn-sm text-white delete-wishlist rounded-0" id="${encript(data[i].ws_id)}" href="#"><i class="ti-close"></i></a></td>
                        </tr>
                    `;
                }

                $('#show_wishlist').html(html);

            }
        });
    }
    show_wishlist();

    $('body').on('click', '.delete-wishlist', function ( e ) {
        e.preventDefault();
        let id = $(this).attr('id');

        $.ajax({
            url     : '<?= base_url('del-wishlist'); ?>',
            method  : 'POST',
            data    : { action : 'del_wishlist', del_id : id },
            success : function ( response ) {
                data = JSON.parse( response );
                show_wishlist();
                popup( data.status, data.msg );
            }
        });
        
    })

    $('#selectAll').click( function( e ) {
        if ( this.checked ) {
            $('.checkbox').each( function() {
                this.checked = true ;
            });
        } else {
            $('.checkbox').each( function () {
                this.checked = false ;
            });
        }
    });
    


    $('body').on('click', '.addToCart', function ( e ) {
        e.preventDefault();
        let id = $(this).attr('id');
        addToCart( id );
        
    });

    function addToCart( id ) {
        
        $.ajax({
            url     : '<?= base_url('cart/addToCart') ;?>',
            method  : 'POST',
            data    : { action : 'addToCart', product_id : id},
            success : function ( response ) {

                data = JSON.parse( response );
                show_wishlist();
                popup( data.status, data.message );
                
            }
        });

    }

    $('#btn-action').click( function ( e ) {
        e.preventDefault();
        let id = [], balk;

        $('.checkbox').each( function () {
        	if( $(this).is(':checked')) {
        		id.push( $(this).data('id') );
        	}
        });

        balk = $('#balkData').val();

        if ( id == '' || balk == 0 ) {
            popup( 'warning', 'Please checked product!' );
        } 
        else {
        	$.ajax({
                url     : '<?= base_url('wishlist/balk') ;?>',
                method  : 'POST',
                data    : { id : id, balk : balk , action : 'balk'},
                success : function ( response ) {
                    data = JSON.parse( response );
                	show_wishlist();
                	popup( data.status, data.msg );
                    
                }
            });

        }
    });

    
});
</script>