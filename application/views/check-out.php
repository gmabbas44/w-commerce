

    <!-- Breadcrumb Section Begin -->
    <div class="breacrumb-section">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="breadcrumb-text product-more">
                        <a href="./index.html"><i class="fa fa-home"></i> Home</a>
                        <a href="./shop.html">Shop</a>
                        <span>Check Out</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Breadcrumb Section Begin -->

    <!-- Shopping Cart Section Begin -->
    <section class="checkout-section spad">
        <div class="container">

            <?= form_open('checkout', ['class'=> 'checkout-form']) ; ?>
                <div class="row">
                    <div class="col-lg-6">
                        <h4>Biiling Details</h4>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="create-item">
                                    <label for="login-profile">
                                        Same as Profile?
                                        <input type="checkbox" id="login-profile">
                                        <span class="checkmark"></span>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <label for="fullname">Full Name<span>*<?= form_error('shipping_fullname') ; ?></span></label>
                                <input type="text" value="<?= set_value('shipping_fullname') ; ?>" name="shipping_fullname" id="fullname">
                            </div>
                            
                            <div class="col-lg-12">
                                <label for="city">Town / City<span>*<?= form_error('city') ; ?></span></label>
                                <select name="city" id="district">
                                    <option value="">Select District</option>
                                    <?php foreach($getDistricts as $district): ?>
                                        <option value="<?= $district->id; ?>"><?= $district->name; ?></option>
                                    <?php endforeach; ?>
                                </select>
                              
                            </div>
                            <div class="col-lg-6">
                                <label for="email">Email Address<span class="text-danger">*<?= form_error('shipping_email') ; ?></span></label>
                                <input type="email" value="<?= set_value('shipping_email') ; ?>" name="shipping_email" id="email">
                            </div>
                            <div class="col-lg-6">
                                <label for="phone">Phone<span>*<?= form_error('shipping_phone') ; ?></span></label>
                                <input type="number" value="<?= set_value('shipping_phone') ; ?>" name="shipping_phone" id="phone">
                            </div>
                            <div class="col-lg-12">
                                <label for="address">Shepping Address<span>*<?= form_error('shipping_address') ; ?></span></label>
                                <input type="text" value="<?= set_value('shipping_address') ; ?>" name="shipping_address" id="address">
                            </div>
                            
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="place-order">
                            <h4>Your Order</h4>
                            <div class="order-total">

                                <table class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th class="text-left">Product</th>
                                                <th class="text-center">Quatity</th>
                                                <th class="text-right">Total</th>
                                            </tr>
                                        </thead>

                                        <tbody id="order_product">
                                            <!-- ajax reqeust -->
                                        </tbody >

                                        <tfoot id="count_operation" >
                                            <!-- ajax reqeust -->
                                        </tfoot>
                                </table>
                                
                                <div class="payment-check">
                                    <span>* <?= form_error('payment') ; ?></span>
                                    <div class="pc-item">
                                        <label for="pc-cash">
                                            Cash On Delivery
                                            <input type="radio" name="payment" id="pc-cash" value="cash">
                                            <span class="checkmark"></span>
                                        </label>
                                    </div>
                                    <div class="pc-item">
                                        <label for="pc-bKash">
                                            bKash
                                            <input type="radio" name="payment" id="pc-bKash" value="bKash">
                                            <span class="checkmark"></span>
                                        </label>
                                    </div>
                                </div>
                                <div class="order-btn">
                                    <button type="submit" class="site-btn place-btn">Place Order</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?= form_close() ;?>
        </div>
    </section>
    <!-- Shopping Cart Section End -->

<script>
    $("#district").change(function(){
        shipping_cost();
    })

    function shipping_cost () {
        const district_id = $("#district").val();
        
        if(district_id == ""){
            order(data.shipping_cost);
            return;
        }

        $.ajax({
            url: '<?= base_url("cart/getShippingCost");?>',
            method: 'POST',
            data: {district_id: district_id},
            success: function(response){
                data = JSON.parse(response);

                $("#shipping_cost").html(data.shipping_cost);
                order(data.shipping_cost);
            }
        })
    }

    $('#login-profile').change(function(){
        const user_id = '<?= $this->session->userdata('user_id');?>'
        if (this.checked) {
            $.ajax({

                url     : '<?= base_url("register/getCustomerInfo");?>',
                method  : 'POST',
                data    : {user_id : user_id},
                success : function(response) {
                    data = JSON.parse(response);
                    $('#fullname').val(data.fullname);
                    $('#district').val(data.city);
                    $('#email').val(data.email);
                    $('#phone').val(data.mobile);
                    $('#address').val(data.address);
                    shipping_cost();
                }
            });
        } else {
            $('#fullname').val('');
            $('#district').val('');
            $('#email').val('');
            $('#phone').val('');
            $('#address').val('');
            shipping_cost();
        }
    });

    function order( shipping_cost = 0) {

        let order_product = '', 
            count_operation = '',
            sum = 0, vat, total;
        
        $.ajax({
            url     : '<?= base_url("cart/getOrderCart");?>',
            method  : 'POST',
            data    : { get_order : 'get_order_cart' },
            success : function ( response ) {
                data = JSON.parse( response );

                for (let i = 0; i < data.length; i++) {

                    sum += parseFloat(data[i].total_price);
                    order_product += `
                        <tr>
                            <td class="text-left">${data[i].product_title}</td>
                            <td class="text-center">${data[i].qty}</td>
                            <td class="text-right">${data[i].total_price}</td>
                        </tr>
                    `;
                }

                vat = sum * .05 ;
                total = sum + vat + parseInt(shipping_cost);

                count_operation +=`<tr>
                                        <th colspan="2" class="text-left">Subtotal</th>
                                        <th class="text-right">Tk ${sum}</th>
                                    </tr>
                                    <tr>
                                        <th colspan="2" class="text-left">Vat(5%)</th>
                                        <th class="text-right">Tk ${vat}</th>
                                    </tr>
                                    <tr>
                                        <th colspan="2" class="text-left">Shipping Cost</th>
                                        <th class="text-right">Tk ${parseInt(shipping_cost)}</th>
                                    </tr>
                                    <tr>
                                        <th colspan="2" class="text-left">Total</th>
                                        <th class="text-right">Tk ${total}</th>
                                    </tr>`;

                $('#order_product').html(order_product);
                $('#count_operation').html(count_operation);


            }

        });
    }
    order();
</script>




