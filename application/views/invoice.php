    <!-- Breadcrumb Section Begin -->
    <div class="breacrumb-section">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="breadcrumb-text">
                        <a href="#"><i class="fa fa-home"></i> Home</a>
                        <a href="#"><i class="fa fa-shopping-bag" aria-hidden="true"></i> Cart</a>
                        <span> Invoice</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Breadcrumb Form Section Begin -->

    <!-- Invoice Section Begin -->
    <div class="register-login-section">

        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="alert alert-success ">
                        <i class="fa fa-check-circle" aria-hidden="true"></i>
                        <span class="font-weight-bold"> Success! </span> Your order success
                    </div>
                </div>
                <div class="col-md-6">
                    <a href="" class="float-right primary-btn">Download Invoice PDF</a>
                </div>
            </div>
            

            <div class="row">
                <div class="col-md-6">
                    <h4>Customer Information</h4>
                    <table class="table table-bordered table-striped">
                        <tr>
                            <th>Fullname</th>
                            <td><?= $customer_info->fullname ;?></td>
                        </tr>
                        <tr>
                            <th>Email</th>
                            <td><?= $customer_info->email ;?></td>
                        </tr>
                        <tr>
                            <th>Phone</th>
                            <td><?= $customer_info->mobile ;?></td>
                        </tr>
                        <tr>
                            <th>City/ District</th>
                            <td><?= $customer_info->name ;?></td>
                        </tr>
                        <tr>
                            <th>Address</th>
                            <td><?= $customer_info->address ;?></td>
                        </tr>
                    </table>
                </div>
                <div class="col-md-6">
                    <h4>Shipping Information</h4>
                    <table class="table table-bordered table-striped">
                        <tr>
                            <th>Fullname</th>
                            <td><?= $shipping_info->shipping_fullname ;?></td>
                        </tr>
                        <tr>
                            <th>Email</th>
                            <td><?= $shipping_info->shipping_email ;?></td>
                        </tr>
                        <tr>
                            <th>Phone</th>
                            <td><?= $shipping_info->shipping_phone ;?></td>
                        </tr>
                        <tr>
                            <th>City/ District</th>
                            <td><?= $shipping_info->name ;?></td>
                        </tr>
                        <tr>
                            <th>Address</th>
                            <td><?= $shipping_info->shipping_address ;?></td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="row pt-3">
                <div class="col-lg-12 col-md-12">
                    <h4>Order Details</h4>
                    <table class="table table-bordered table-striped table-sm">
                        <thead>
                            <tr class="text-center">
                                <th class="text-left" width="10%">SL</th>
                                <th width="45%">Product Name</th>
                                <th width="15%">Quantity</th>
                                <th width="15%">Unit Price</th>
                                <th width="15%">Amount</th>
                            </tr>
                        </thead>
                        <tbody>

                            <?php
                                $sl = 0;

                                $total = $order_info->total_price + $order_info->vat + $order_info->shipping_cost;

                                foreach ($order_details as $order) :
                                    $sl++;
                            ?>
                            <tr>
                                <td><?= $sl ;?></td>
                                <td><?= $order->title; ?></td>
                                <td class="text-center"><?= $order->qty; ?></td>
                                <td class="text-center">Tk. <?= $order->mrp; ?></td>
                                <td class="text-center">Tk. <?= $order->price; ?></td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <td colspan="3" class="border-0"></td>
                                <th >Sub Total</th>
                                <th class="text-center">Tk. <?= $order_info->total_price ; ?></th>
                            </tr>
                            <tr>
                                <td colspan="3" class="border-0"></td>
                                <th>Vat (5%)</th>
                                <th class="text-center">Tk. <?= $order_info->vat ; ?></th>
                            </tr>
                            <tr>
                                <td colspan="3" class="border-0"></td>
                                <th>Shipping Cost</th>
                                <th class="text-center">Tk. <?= $order_info->shipping_cost ; ?></th>
                            </tr>
                            <tr>
                                <td colspan="3" class="border-0"></td>
                                <th>Total</th>
                                <th class="text-center">Tk. <?= $total ; ?></th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!--Invoice Section End -->