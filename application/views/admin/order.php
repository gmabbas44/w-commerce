<table class="table table-bordered table-sm">

  <thead>
    <tr>
      <th>Id</th>
      <th>Order name</th>
      <th>Shipping name</th>
      <th>Shipping Phone</th>
      <th>Payment type</th>
      <th>Total Price</th>
      <th>Shipping Data</th>
      <th>Order Status</th>
      <th>Action</th>
    </tr>
  </thead>

  <tbody id="orders">
    
  </tbody>
</table>


<script>
  $(function() {
    $('table').DataTable({
      //order : [0, 'desc']
    });

    const orderStatus = (status) => {
      let val
      if(status == 0 ){
        val = '<span class="badge badge-danger">Pending</span>'
      } else if (status == 1) {
        val = '<span class="badge badge-warning">Shipping</span>'
      } else {
        val = '<span class="badge badge-success">Complete</span>'
      }
      return val
    }
    function shpping_status(status, id)  {
      let val 
      if(status == 0 ){
        val = `<a title = "Shipping" href= "<?= base_url()?>orderStaus/${status}/${id}"><i class ="fa fa-shipping-fast"></i></a>|`
      } else if (status == 1) {
        val = `<a title = "Shipping" href= "<?= base_url() ?>orderStaus/${status}/${id}"><i class="fas fa-check"></i></a>|`
      } 
      return val
    }

    const Orders = () => {
      let html = '',
        i, sl = 0,
        status;
      $.ajax({
        url: '<?= base_url('AdminOrder/viewOrder') ?>',
        method: 'POST',
        data: {
          action: "get_all"
        },
        success: function(response) {
          data = JSON.parse(response);
          // console.log(data)
          for (i of data) {
            sl++
            html += `
              <tr>
                <td>${sl}</td>
                <td>${i.fullname}</td>
                <td>${i.shipping_fullname}</td>
                <td>${i.shipping_phone}</td>
                <td>${i.payment_type}</td>
                <td>${i.total_price}</td>
                <td>${i.shipping_date}</td>
              <td>${orderStatus(i.order_status)}</td > <td>
              ${shpping_status(i.order_status, i.ord_id)}
              <a title = "Details" href = "<?= base_url('AdminOrder/viewOrderDetails/')?>${i.ord_id}"> <i class="text-success fa fa-eye"> </i></a>
              | <a title = "Deleted" href = ""> <i class="text-danger fa fa-trash-alt"> </i></a></td></tr>`;
          }
          $('#orders').html(html);
          // console.log( html );
        }
      })
    }

    Orders()

  })
</script>