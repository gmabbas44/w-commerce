<div class="row">
  <div class="col-md-6">
    <div class="card">
      <div class="card-header">
        Customer Information
      </div>
      <div class="card-body p-0">
        <table class="table table-bordered table-sm mb-0">
          <tr>
            <th>Fullname</th>
            <td><?= $customer_info->fullname ?></td>
          </tr>
          <tr>
            <th>Email</th>
            <td><?= $customer_info->email ?></td>
          </tr>
          <tr>
            <th>Phone</th>
            <td><?= $customer_info->mobile ?></td>
          </tr>
          <tr>
            <th>City/ District</th>
            <td><?= $customer_info->name ?></td>
          </tr>
          <tr>
            <th>Address</th>
            <td><?= $customer_info->address ?></td>
          </tr>
        </table>
      </div>
    </div>
  </div>
  <div class="col-md-6">
    <div class="card">
      <div class="card-header">
        Shipping Information
      </div>
      <div class="card-body p-0">
        <table class="table table-bordered table-sm mb-0">
          <tr>
            <th>Fullname</th>
            <td><?= $shipping_info->shipping_fullname ?></td>
          </tr>
          <tr>
            <th>Email</th>
            <td><?= $shipping_info->shipping_email ?></td>
          </tr>
          <tr>
            <th>Phone</th>
            <td><?= $shipping_info->shipping_phone ?></td>
          </tr>
          <tr>
            <th>City/ District</th>
            <td><?= $shipping_info->name ?></td>
          </tr>
          <tr>
            <th>Address</th>
            <td><?= $shipping_info->shipping_address ?></td>
          </tr>
        </table>
      </div>
    </div>
  </div>
  <div class="col-md-12">
    <div class="card">
      <div class="card-header">
        Order Details
      </div>
      <div class="card-body p-0">
        <table class="table table-bordered table-sm mb-0">
          <tr>
            <th>SL</th>
            <th>Product Name</th>
            <th>Quantity</th>
            <th>Unit Price</th>
            <th>Amount</th>
          </tr>
          <tbody>

            <?php
            $sl = 0;

            $total = $order_info->total_price + $order_info->vat + $order_info->shipping_cost;

            foreach ($order_details as $order) :
              $sl++;
            ?>
              <tr>
                <td><?= $sl; ?></td>
                <td><?= $order->title; ?></td>
                <td class="text-center"><?= $order->qty; ?></td>
                <td class="text-center">Tk. <?= $order->mrp; ?></td>
                <td class="text-center">Tk. <?= $order->price; ?></td>
              </tr>
            <?php endforeach; ?>
          <tfoot>
            <tr>
              <td colspan="3" class="border-0"></td>
              <th>Sub Total</th>
              <th class="text-center">Tk. <?= $order_info->total_price; ?></th>
            </tr>
            <tr>
              <td colspan="3" class="border-0"></td>
              <th>Vat (5%)</th>
              <th class="text-center">Tk. <?= $order_info->vat; ?></th>
            </tr>
            <tr>
              <td colspan="3" class="border-0"></td>
              <th>Shipping Cost</th>
              <th class="text-center">Tk. <?= $order_info->shipping_cost; ?></th>
            </tr>
            <tr>
              <td colspan="3" class="border-0"></td>
              <th>Total</th>
              <th class="text-center">Tk. <?= $total; ?></th>
            </tr>
          </tfoot>
        </table>
      </div>
    </div>
  </div>
</div>