<!-- Page Heading -->
<h1 class="h3 mb-4 text-gray-800">Add Product</h1>

<div class="card">
	<div class="card-body">
			<?= form_open_multipart('admin-add-product')?>
			
			<div class="form-row">

				<div class="form-group col-md">
					<label for="product_category">Category</label>
					<select id="product_category" name="category" class="form-control">
						<option selected>Choose...</option>
						<?php foreach ($categories as $category) : ?>
						<option value="<?= $category->cat_id ; ?>"><?= $category->category_name ; ?></option>
						<?php endforeach; ?>
					</select>
				</div>
				<div class="form-group col-md">
					<label for="product_type">Product Type</label>
					<select id="product_type" name="product_type" class="form-control">
						<option>Choose...</option>
						<option value="0">Reguler Product</option>
						<option value="1">New Product</option>
						<option value="2">Feature Product</option>
						<option value="3">Coming Soom Product</option>
					</select>
				</div>

				<div id='publish_time'>
					
				</div>

				<div class="form-group col-md">
					<label for="brand">Brand</label>
					<select id="brand" name="brand" class="form-control">
						<option selected >Choose...</option>
						<?php foreach ($brand as $b) : ?>
						<option value="<?= $b->brand_id ; ?>"><?= $b->brand_name ; ?></option>
						<?php endforeach; ?>
					</select>
				</div>

				<div class="form-group col-md">
					<label for="department">Department</label>
					<select id="department" name="department" class="form-control">
						<option selected >Choose...</option>
						<?php foreach ($department as $d) : ?>
						<option value="<?= $d->dep_id ; ?>"><?= $d->dep_name ; ?></option>
						<?php endforeach; ?>
					</select>
				</div>

			</div>

			<div class="form-row">
				<div class="form-group col-md">
					<label for="department">Collection</label>
					<select id="department" name="collection" class="form-control">
						<option selected >Choose...</option>
						<?php foreach ($collection as $c) : ?>
						<option value="<?= $c->coll_id ; ?>"><?= $c->name ; ?></option>
						<?php endforeach; ?>
					</select>
				</div>
				<div class="form-group col-md">
					<label for="in-stock">In stock</label>
					<input type="number" name="in_stock" id="in-stock" class="form-control" placeholder="In stock">
				</div>
				<div class="form-group col-md">
					<label for="mrp">MRP</label>
					<input type="number" name="mrp" id="mrp" class="form-control" placeholder="MRP">
				</div>
				<div class="form-group col-md">
					<label for="price">Price</label>
					<input type="number" name="price" id="price" class="form-control" placeholder="Price">
				</div>
				<div class="form-group col-md">
					<label for="active-product">Active Product</label>
					<select class="form-control" name="is_active" id="is-active">
						<option value="0">No</option>
						<option value="1">Yes</option>
					</select>
				</div>
			</div>
			<div class="form-row">
				<div class="form-group col-md">
					<label for="title">Product title</label>
					<?= form_input(['name'=>'title', 'class' => 'form-control', 'id' => 'title', 'placeholder' => 'Product Title']) ; ?>
				</div>
				<div class="form-group col-md">
					<label for="short_description">Short Description</label>
					<?= form_input(['name'=>'short_description', 'class' => 'form-control', 'id' => 'short_description', 'placeholder' => 'Short description']) ; ?>
				</div>
			</div>
			
			<div class="form-row">
				<div class="form-group col-md-6">
					<label for="image">Image</label>
					<?= form_upload(['name' => 'userfile', 'id' => 'image', 'class'=> 'form-control-file']) ;?>
				</div>
				<div class="form-group col-md-6">
					<label for="image">Image Gallery</label>
	    			<input type="file" name="gellery_images[]" multiple="" class="form-control-file">
				</div>
			</div>
			<div class="form-row">
				<div class="form-group col-md">
					<label for="description">Description</label>
					<textarea name="description" name="description" class="form-control" id="description" rows="3"  placeholder="Product Description"></textarea>
				</div>
			</div>
			<button type="submit" class="btn btn-primary">Add Product</button>
		<?= form_close() ;?>
	</div>
</div>


<script>
	$( function() {
		$('body').on('change', '#product_type', function () {
			let product_type = $(this).val();
			if (product_type == 3 ) {
				let html = `<div class="form-group col-md">
								<label for="">Publish time</label>
								<input type="datetime-local" class="form-control">
							</div>`;
				$('#publish_time').html(html);
			}else {
				$('#publish_time').html('');
			}
		})
	});
</script>
