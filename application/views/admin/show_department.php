<div class="d-sm-flex align-items-center justify-content-between mb-4">
	<h1 class="h3 mb-0 text-gray-800">Department</h1>
	<a href="#" data-toggle="modal" data-target="#add_department_model" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="far fa-plus-square"></i> Add new Department</a>
</div>

<div class="card-body">
	<table class="table table-bordered table-sm">
		<thead>
			<th>SL</th>
			<th>Title</th>
			<th>Status</th>
			<th class="text-center">Action</th>
		</thead>
		<tbody id="department_view">
			<!-- <tr>
				<td>SL</td>
				<td>Department Name</td>
				<td width='10%' class="text-center align-middle">
					<a href="#" id="" class="btn-update" data-toggle="modal" data-target="#update_department_model"><i title="Edit User Information" data-toggle="tooltip" data-placement="top" class="fas fa-user-edit"></i></a>
					<a href="#" id="" class="text-danger delete-btn" data-toggle="modal" data-target="#update_dapartment_model" title="Delete User Information"><i class="far fa-trash-alt"></i></i></a>
				</td>
			</tr> -->
		</tbody>
	</table>
</div>

<!-- add department model -->
<div class="modal fade" id="add_department_model" tabindex="-1" role="dialog" aria-labelledby="update-box" aria-hidden="true">
	<div class="modal-dialog modal-dialog-scrollable" role="document">
		<div class="modal-content border-primary">
			<div class="modal-header">
				<h5 class="modal-title" id="update-box">Add New Department</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<form id="add_department">
				<div class="modal-body">
					<div class="form-group">
						<label for="dep_name">Department Name</label>
						<input type="text" class="form-control" name="dep_name" id="dep_name" placeholder="Enter department Name">
					</div>
					<div class="form-group">
						<div class="row">
							<div class="col-md">
								<label for="status">Active</label>
							</div>
							<div class="col-md">
								<input type="radio" name="status" value="0" id="no">
								<label for="no">No</label>
							</div>
							<div class="col-md">
								<input type="radio" name="status" value="1" id="yes">
								<label for="yes">Yes</label>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button id="add_btn" type="button" class="btn btn-primary btn-block">Add</button>
				</div>
			</form>
		</div>
	</div>
</div>
<!-- End add Department modal -->

<!-- update department model -->
<div class="modal fade" id="update_department_model" tabindex="-1" role="dialog" aria-labelledby="update-box" aria-hidden="true">
	<div class="modal-dialog modal-dialog-scrollable" role="document">
		<div class="modal-content border-primary">
			<div class="modal-header">
				<h5 class="modal-title" id="update-box">Update Department</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<form id="update-form">
				<div class="modal-body">
					<input type="hidden" id="dep_id" name="dep_id">
					<div class="form-group">
						<label for="update_dep_name">Department Name</label>
						<input type="text" class="form-control" name="dep_name" id="update_dep_name" placeholder="Enter department Name">
					</div>
					<div class="form-group">
						<div class="row">
							<div class="col-md">
								<label for="status">Active</label>
							</div>
							<div class="col-md">
								<input type="radio" name="status" value="0" id="up_no">
								<label for="up_no">No</label>
							</div>
							<div class="col-md">
								<input type="radio" name="status" value="1" id="up_yes">
								<label for="up_yes">Yes</label>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button id="update-btn" type="button" class="btn btn-primary btn-block">Update</button>
				</div>
			</form>
		</div>
	</div>
</div>
<!-- End update Department modal -->


<!-- delete modal -->
<div class="modal fade" id="exampleModal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Department Delete</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        ...
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>
<!-- end delete modal  -->
<script>
	$(function() {
		// $('table').DataTable({
		// 	//order : [0, 'desc']
		// });

		const allDepartment = () => {
			let html = '',
				i, sl = 0,
				status;
			$.ajax({
				url: '<?= base_url('AdminDepartment/depView') ?>',
				method: 'POST',
				data: {
					action: "get_all"
				},
				success: function(response) {
					data = JSON.parse(response);
					// console.log(data)
					for (i in data) {
						sl++
						if (data[i].status == 0) {
							status = '<i title="Inactive" class="text-warning fa fa-times" aria-hidden="true"> In Active</i> ';
						} else if (data[i].status == 1) {
							status = '<i title="Active" class="text-success fa fa-check" aria-hidden="true"> Active</i> ';
						} else {
							status = '<i title="Deleted" class="text-danger far fa-trash-alt"> Deleted <i>';
						}
						html += `
						<tr>
							<td>${sl}</td>
							<td>${data[i].dep_name}</td>
							<td>${status}</td>
							<td width="10%" class="text-center align-middle">
									<a href="#" id="" data-id=${data[i].dep_id} class="btn-update" data-toggle="modal" data-target="#update_department_model" ><i title="Edit"  class="fas fa-user-edit"></i></a>
									<a href="#" id=""  class="text-danger delete-btn" title="Delete  "><i class="far fa-trash-alt"></i></i></a>
							</td>
						</tr>`;
					}
					$('#department_view').html(html);
				}
			})
		}
		allDepartment()
		// add department 
		$('#add_btn').click(function(e) {
			if ($('#add_department')[0].checkValidity()) {
				e.preventDefault();
				$('#add_btn').text('Please wait.......');

				// ajax request for insert data
				$.ajax({
					url: '<?= base_url('AdminDepartment/add_department'); ?>',
					method: 'POST',
					data: $('#add_department').serialize() + "&action=add_department",
					success: function(response) {
						data = JSON.parse(response)
						$('#add_btn').text('Add Department');
						$('#add_department_model').modal('hide')
						$('#add_department')[0].reset()
						allDepartment()
						popup(data.status, data.msg)
					}
				});
			}
		})

		// start get department data form update  
		$('body').on('click', '.btn-update', function(e) {
			let id = $(this).attr('data-id')
			// console.log( id )
			$.ajax({
				url: '<?= base_url('AdminDepartment/getUpdate'); ?>',
				method: 'POST',
				data: {
					id: id,
					action: 'updateDepartment'
				},
				success: function(response) {
					// console.log(response)
					const data = JSON.parse(response)
					$('#dep_id').val(data.dep_id)
					$('#update_dep_name').val(data.dep_name)
					if (data.status == 1) {
						$('#up_yes').attr('checked', true)
					} else {
						$('#up_no').attr('checked', true)
					}
				}
			})
		})
		// end 

		$('#update-btn').click( function(e) {
			e.preventDefault()
			if( $('#update-form')[0].checkValidity() ) {
				$('#update-btn').text('Please wait.......');

				$.ajax({
					url: '<?= base_url('AdminDepartment/update_department'); ?>',
					method: 'POST',
					data: $('#update-form').serialize() + "&action=update_department",
					success: function(response) {
						data = JSON.parse(response)
						// console.log( response )
						$('#update-btn').text('Update');
						$('#update_department_model').modal('hide')
						$('#update-form')[0].reset()
						allDepartment()
						popup(data.status, data.msg)
					}
				});
			}
		})
		
	})
</script>