<!-- Page Heading -->
<h1 class="h3 mb-4 text-gray-800">Update Product</h1>
<?php //var_dump($product) ?>
<div class="card">
	<div class="card-body">
			<?= form_open_multipart('adminProduct/updateProduct')?>
			
			<div class="form-row">

				<div class="form-group col-md">
					<label for="product_category">Category</label>
					<select id="product_category" name="category" class="form-control">
						<option selected>Choose...</option>
						<?php foreach ($categories as $category) : ?>
            <?php  $product->cat_id == $category->cat_id ? $selected = 'selected' :  $selected = '' ?>
						<option <?= $selected ?> value="<?= $category->cat_id ; ?>"><?= $category->category_name ; ?></option>
						<?php endforeach; ?>
					</select>
				</div>
        
				<div class="form-group col-md">
					<label for="product_type">Product Type</label>
					<select id="product_type" name="product_type" class="form-control">
						<option>Choose...</option>
            <?php 
                $types = [ 'Reguler Product', 'New Product', 'Feature Product', 'Coming Soom Product'];
                foreach( $types as $k => $v ) {
                  
                  $k == $product->product_type ? $selected = 'selected' :  $selected = '' ;
                  echo "<option $selected value='$k'>$v</option>";
                }
            ?>
					</select>
				</div>

				<div id='publish_time'>
					
				</div>

				<div class="form-group col-md">
					<label for="brand">Brand</label>
					<select id="brand" name="brand" class="form-control">
						<option >Choose...</option>
						<?php foreach ($brand as $b) : ?>
              <?php  $b->brand_id ==  $product->brand_id  ? $selected = 'selected' :  $selected = '' ?>
						<option <?= $selected ?> value="<?= $b->brand_id ; ?>"><?= $b->brand_name ; ?></option>
						<?php endforeach; ?>
					</select>
				</div>

				<div class="form-group col-md">
					<label for="department">Department</label>
					<select id="department" name="department" class="form-control">
						<option selected >Choose...</option>
						<?php foreach ($department as $d) : ?>
              <?php  $d->dep_id ==  $product->dep_id  ? $selected = 'selected' :  $selected = '' ?>
						<option <?= $selected ?> value="<?= $d->dep_id ; ?>"><?= $d->dep_name ; ?></option>
						<?php endforeach; ?>
					</select>
				</div>

			</div>

			<div class="form-row">
				<div class="form-group col-md">
					<label for="department">Collection</label>
					<select id="department" name="collection" class="form-control">
						<option selected >Choose...</option>
						<?php foreach ($collection as $c) : ?>
              <?php  $c->coll_id ==  $product->coll_id  ? $selected = 'selected' :  $selected = '' ?>
						<option <?= $selected ?> value="<?= $c->coll_id ; ?>"><?= $c->name ; ?></option>
						<?php endforeach; ?>
					</select>
				</div>
				<div class="form-group col-md">
					<label for="in-stock">In stock</label>
					<input type="number" value="<?= $product->in_stock ?>" name="in_stock" id="in-stock" class="form-control" placeholder="In stock">
				</div>
				<div class="form-group col-md">
					<label for="mrp">MRP</label>
					<input type="number" value="<?= $product->mrp ?>" name="mrp" id="mrp" class="form-control" placeholder="MRP">
				</div>
				<div class="form-group col-md">
					<label for="price">Price</label>
					<input type="number" value="<?= $product->price ?>" name="price" id="price" class="form-control" placeholder="Price">
				</div>
				<div class="form-group col-md">
        
					<label for="active-product">Active Product <?php  $product->is_active ?></label>
          
					<select class="form-control" name="is_active" id="is-active">
          <?php 
              if( $product->is_active == 1) {
                echo "
                <option value='0'>No</option>
						    <option selected value='1'>Yes</option>
                ";
              } else {
                echo "
                <option selected value='0'>No</option>
						    <option  value='1'>Yes</option>
                ";
              }
          ?>
						
					</select>
				</div>
			</div>
			<div class="form-row">
				<div class="form-group col-md">
					<label for="title">Product title</label>
					<?= form_input(['name'=>'title', 'class' => 'form-control', 'id' => 'title', 'value' => $product->title ]) ; ?>
				</div>
				<div class="form-group col-md">
					<label for="short_description">Short Description</label>
					<?= form_input(['name'=>'short_description', 'class' => 'form-control', 'id' => 'short_description', 'value' => $product->short_description]) ; ?>
				</div>
			</div>
		
			<div class="form-row">
				<div class="form-group col-md">
					<label for="description">Description</label>
					<textarea name="description" name="description" class="form-control" id="description" rows="3"  ><?= $product->description ?></textarea>
				</div>
			</div>
			<button type="submit" class="btn btn-primary">Update Product</button>
		<?= form_close() ;?>
	</div>
</div>


<script>
	$( function() {
		$('body').on('change', '#product_type', function () {
			let product_type = $(this).val();
			if (product_type == 3 ) {
				let html = `<div class="form-group col-md">
								<label for="">Publish time</label>
								<input type="datetime-local" class="form-control">
							</div>`;
				$('#publish_time').html(html);
			}else {
				$('#publish_time').html('');
			}
		})
	});
</script>
