<style>
	.product_image {
		width: 40px;
	}
	tr td {
		font-size: 14px;
		vertical-align: center;
	}
</style>
<!-- Page Heading -->
<div class="d-sm-flex align-items-center justify-content-between mb-4">
	<h1 class="h3 mb-0 text-gray-800">All Products</h1>
	<a href="<?= base_url('admin-add-product')?>" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="far fa-plus-square"></i> Add new Product</a>
</div>

<div class="table-responsive-md">
	<table class="table table-bordered table-sm">
		<thead>
			<tr>
				<th scope="col"><input type="checkbox" id="selectAll"></th>
				<th scope="col">Title</th>
				<th scope="col">Image</th>
				<th scope="col">Total Sales</th>
				<th scope="col">In Stock</th>
				<th scope="col">Product Type</th>
				<th scope="col">MRP</th>
				<th scope="col">Price</th>
				<th scope="col">Soft Delete</th>
				<th scope="col">Is Active</th>
				<th scope="col">Action</th>
			</tr>
		</thead>
		<tbody>
		<?php foreach ($allProduct as $product) : ?>
			<tr>
				<td><input type="checkbox" value="<?= $product->product_id; ?>" class="checkbox"></td>
				<td><?= $product->title; ?></td>
				<td><img class="product_image" src="<?= ASSETS.'img/products/'. $product->image ;?>" alt=""></td>
				<td><?= $product->total_sales; ?></td>
				<td><?= $product->in_stock; ?></td>
				<td><?= product_type($product->product_type); ?></td>
				<td><?= $product->mrp; ?></td>
				<td><?= $product->price; ?></td>
				<td><?= status( $product->soft_delete ); ?></td>
				<td><?= status( $product->is_active ); ?></td>
				<td>
					<a href="<?= base_url().'adminProduct/updateProduct/'.$product->product_id ?>" id="" class="btn-update" ><i title="Edit"  class="fas fa-user-edit"></i></a>
	        <a onclick="return confirm('Are you sure?')" href="<?= base_url().'adminProduct/deleteProduct/'.$product->product_id ?>" id="" class="text-danger " title="Delete"><i class="far fa-trash-alt"></i></i></a>
				</td>
			</tr>
		<?php endforeach; ?>
		</tbody>
	</table>
</div>

<script>
	$( function () {
		$('table').DataTable({
	        //order : [0, 'desc']
	    });
	})
</script>
