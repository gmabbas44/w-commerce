<div class="d-sm-flex align-items-center justify-content-between mb-4">
	<h1 class="h3 mb-0 text-gray-800">Collection</h1>
	<a href="#" data-toggle="modal" data-target="#add_collection" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="far fa-plus-square"></i> Add new Collection</a>
</div>

<div class="card-body">
    <table class="table table-bordered">
        <thead>
            <th>SL</th>
            <th>Title</th>
            <th>Note</th>
            <th class="text-center">Action</th>
        </thead>
        <tbody>
            <tr>
                <td>SL</td>
                <td>Collection Name</td>
                <td width='10%' class="text-center align-middle">
                    <a href="#" id="" class="text-info info-btn" data-toggle="tooltip" data-placement="left" title="View User Information"><i class="fas fa-info-circle"></i></a>
                    <a href="#" id="" class="btn-update" data-toggle="modal" data-target="#update-box" ><i title="Edit User Information" data-toggle="tooltip" data-placement="top" class="fas fa-user-edit"></i></a>
                    <a href="#" id="" class="text-danger delete-btn" data-toggle="tooltip" data-placement="right" title="Delete User Information"><i class="far fa-trash-alt"></i></i></a>
                </td>
            </tr>
            
        </tbody>
    </table>
</div> 




<!-- update model -->
<div class="modal fade" id="add_collection" tabindex="-1" role="dialog" aria-labelledby="update-box" aria-hidden="true">
    <div class="modal-dialog modal-dialog-scrollable" role="document">
        <div class="modal-content border-primary">
            <div class="modal-header">
                <h5 class="modal-title" id="update-box">Add New Collection</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="add">
                <div class="modal-body">
                    <div class="form-group">
                        <label for="updatTitle">Collection Name</label>
                        <input type="text" class="form-control" name="add_collection" id="addcollection" placeholder="Enter collection Name">
                    </div>
                </div>
                <div class="modal-footer">
                    <button id="update-btn" type="button" class="btn btn-primary btn-block">Add</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- End update modal -->