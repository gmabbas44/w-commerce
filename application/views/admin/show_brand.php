<div class="d-sm-flex align-items-center justify-content-between mb-4">
  <h1 class="h3 mb-0 text-gray-800">Brand</h1>
  <a href="#" data-toggle="modal" data-target="#addBrand" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="far fa-plus-square"></i> Add new Brand</a>
</div>

<div class="card-body">
  <table class="table table-bordered">
    <thead>
      <th>SL</th>
      <th>Title</th>
      <th>Status</th>
      <th class="text-center">Action</th>
    </thead>
    <tbody id="brand_view">
      <tr>
        <td>SL</td>
        <td>Collection Name</td>
        <td>Status</td>
        <td width='10%' class="text-center align-middle">
          <a href="#" id="" class="text-info info-btn" data-toggle="tooltip" data-placement="left" title="View User Information"><i class="fas fa-info-circle"></i></a>
          <a href="#" id="" class="btn-update" data-toggle="modal" data-target="#update-box"><i title="Edit User Information" data-toggle="tooltip" data-placement="top" class="fas fa-user-edit"></i></a>
          <a href="#" id="" class="text-danger delete-btn" data-toggle="tooltip" data-placement="right" title="Delete User Information"><i class="far fa-trash-alt"></i></i></a>
        </td>
      </tr>

    </tbody>
  </table>
</div>




<!-- Add brand model -->
<div class="modal fade" id="addBrand" tabindex="-1" role="dialog" aria-labelledby="update-box" aria-hidden="true">
  <div class="modal-dialog modal-dialog-scrollable" role="document">
    <div class="modal-content border-primary">
      <div class="modal-header">
        <h5 class="modal-title">Add New Brand</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form id="add_brand">
        <div class="modal-body">
          <div class="form-group">
            <label for="brand_name">Brand Name</label>
            <input type="text" class="form-control" name="brand_name" id="brand_name" placeholder="Enter collection Name">
          </div>
          <div class="form-group">
            <div class="row">
              <div class="col-md">
                <label for="status">Active</label>
              </div>
              <div class="col-md">

                <input type="radio" name="status" value="0" id="no">
                <label for="no">No</label>
              </div>
              <div class="col-md">

                <input type="radio" name="status" value="1" id="yes">
                <label for="yes">Yes</label>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button id="add_brand_btn" type="button" class="btn btn-primary btn-block">Add</button>
        </div>
      </form>
    </div>
  </div>
</div>
<!-- End Add Brand modal -->


<!-- Update brand model -->
<div class="modal fade" id="updateBrand" tabindex="-1" role="dialog" aria-labelledby="update-box" aria-hidden="true">
  <div class="modal-dialog modal-dialog-scrollable" role="document">
    <div class="modal-content border-primary">
      <div class="modal-header">
        <h5 class="modal-title">Update Brand</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form id="update_brand">
        <div class="modal-body">
          <input type="hidden" name="brand_id" value="" id="brand_id">
          <div class="form-group">
            <label for="update_brand_name">Brand Name</label>
            <input type="text" class="form-control" name="brand_name" id="update_brand_name" placeholder="Enter collection Name">
          </div>
          <div class="form-group">
            <div class="row">
              <div class="col-md">
                <label for="status">Active</label>
              </div>
              <div class="col-md">
                <input type="radio" name="status" value="0" id="update_no">
                <label for="update_no">No</label>
              </div>
              <div class="col-md">
                <input type="radio" name="status" value="1" id="update_yes">
                <label for="update_yes">Yes</label>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button id="update_brand_btn" type="button" class="btn btn-primary btn-block">Update Brand</button>
        </div>
      </form>
    </div>
  </div>
</div>

<!-- End Update Brand modal -->

<script>
  $(document).ready(function() {

    $('table').DataTable({
      order: [0, 'desc']
    });
    // Show Brand
    function brand_view() {
      let html = '',
        i, sl = 0,
        status;

      $.ajax({
        url: '<?= base_url('AdminBrand/brandView') ?>',
        method: 'POST',
        data: {
          brand_view: "get_all"
        },
        success: function(response) {
          data = JSON.parse(response);
          for (i in data) {
            sl++
            if (data[i].status == 0) {
              status = '<i title="Inactive" class="text-warning fa fa-times" aria-hidden="true"> In Active</i> ';
            } else if (data[i].status == 1) {
              status = '<i title="Active" class="text-success fa fa-check" aria-hidden="true"> Active</i> ';
            } else {
              status = '<i title="Deleted" class="text-danger far fa-trash-alt"> Deleted <i>';
            }

            html += `
              <tr>
                  <td>${sl}</td>
                  <td>${data[i].brand_name}</td>
                  <td>${status}</td>
                  <td width="10%" class="text-center align-middle">
                      <a href="#" id="" data-id=${data[i].brand_id} class="btn-update" data-toggle="modal" data-target="#updateBrand" ><i title="Edit"  class="fas fa-user-edit"></i></a>
                      <a href="#" id=""  class="text-danger delete-btn" title="Delete  "><i class="far fa-trash-alt"></i></i></a>
                  </td>
              </tr>`;
          }
          $('#brand_view').html(html);
        }
      })
    }
    brand_view()

    // get Brand for update
    $('body').on('click', '.btn-update', function(e) {
      e.preventDefault()
      let id = $(this).attr('data-id')
      $.ajax({
        url: '<?= base_url('AdminBrand/getUpdate'); ?>',
        method: 'POST',
        data: {
          id: id,
          action: 'updateBrand'
        },
        success: function(response) {
          // console.log(response)
          const data = JSON.parse(response)
          $('#brand_id').val(data.brand_id)
          $('#update_brand_name').val(data.brand_name)
          if (data.status == 1) {
            $('#update_yes').attr('checked', true)
          } else {
            $('#update_no').attr('checked', true)
          }
        }
      })
    })

    // update Brand 
    $('#update_brand_btn').click(function(e) {
      e.preventDefault();
      if ($('#update_brand')[0].checkValidity()) {
        $('#update_brand_btn').text('Please wait.......')
        $.ajax({
          url: '<?= base_url('AdminBrand/updateBrand'); ?>',
          method: 'POST',
          data: $('#update_brand').serialize() + "&action=update_brand",
          success: function(response) {
            // console.log( response )
            const data = JSON.parse(response)
            $('#updateBrand').modal('hide')
            $('#update_brand')[0].reset()
            $('#update_brand_btn').text('Update Brand')
            brand_view();
            popup(data.status, data.msg);
          }
        })
      }
    })

    // add brand btn
    $('#add_brand_btn').click(function(e) {
      if ($('#add_brand')[0].checkValidity()) {
        e.preventDefault();
        $('#add_brand_btn').text('Please wait.......');

        // ajax request for insert data
        $.ajax({
          url: '<?= base_url('AdminBrand/add_brand'); ?>',
          method: 'POST',
          data: $('#add_brand').serialize() + "&action=add_brand",
          success: function(response) {
            data = JSON.parse(response);
            $('#addBrand').modal('hide');
            $('#add_brand')[0].reset();
            brand_view();
            popup(data.status, data.msg);
          }
        });
      }
    });


  })
</script>