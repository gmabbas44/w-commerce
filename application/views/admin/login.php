
<!DOCTYPE html>
<html lang="en">

<head>
<!-- css directory -->

<!-- Custom styles for this template-->
<link href="<?= ASSETS ?>fonts/fontawesome/css/all.min.css" rel="stylesheet" type="text/css">
<link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

<!-- Custom styles for this template-->
<link href="<?= ASSETS ?>css/admin_panel.min.css" rel="stylesheet">
<style>
	html, body {
	  height: 100%;
	}
	.bg {
		background: url('<?= ASSETS?>img/web-img/1.jpg') no-repeat;
		background-size: cover;
	}
	.login-box {
		width: 20%;
		margin: 0 auto;
	}
	.border-danger {
		background: transparent;
	}
	.head{
		border-bottom: 1px solid #e74a3b!important;
		width: 80px;
		margin-bottom: 30px;
	}
	.form-control {
		background: transparent;
		color: #252525;
	}
	.form-control:focus {
	    color: #e74a3b;
	    background: transparent;
	    border-color: #e74a3b;
	    outline: 0;
	    box-shadow: 0 0 0 0.2rem rgba(116,1,0,.25);
	}
	.btn-danger{
		background: transparent;
	}
</style>

</head>

<body class="bg">
<div class="container h-75">
	<div class="row h-75 align-items-center justify-content-center">
		<div class="col-lg-5">
			<div class="card border-danger">
				<div class="card-body">
					<h2 class="text-center text-white head">Login</h2>
					<?= form_open('admin-login') ; ?>
						<div id="ErrAlert"></div>
						<div class="form-group">
							<input class="form-control" type="email" name="email" id="email" placeholder="Enter your email" autofocus required>
							<small id="existAdmin"></small>
						</div>
						<div class="form-group">
							<input class="form-control" type="password" name="password" id="password" placeholder="Enter your password" required>
						</div>
						<button class="btn btn-danger btn-block" name="submit-btn" id="admin-login-btn" class="btn btn-danger">Login</button>
					<?= form_close();?>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- Bootstrap core JavaScript-->


</body>

</html>
