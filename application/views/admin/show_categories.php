<div class="d-sm-flex align-items-center justify-content-between mb-4">
	<h1 class="h3 mb-0 text-gray-800">Categories</h1>
	<a href="#" data-toggle="modal" data-target="#add_Categories" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="far fa-plus-square"></i> Add new Categories</a>
</div>

<div class="card-body">
    <table class="table table-bordered">
        <thead>
            <th>SL</th>
            <th>Title</th>
            <th>status</th>
            <th class="text-center">Action</th>
        </thead>
        <tbody id="cat_view">
            
        </tbody>
    </table>
</div> 




<!-- add category model -->
<div class="modal fade" id="add_Categories" tabindex="-1" role="dialog" aria-labelledby="add-box" aria-hidden="true">
    <div class="modal-dialog modal-dialog-scrollable" role="document">
        <div class="modal-content border-primary">
            <div class="modal-header">
                <h5 class="modal-title" id="add-box">Add New Categories</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="add-category-form">
                <div class="modal-body">
                    <div class="form-group">
                        <label for="addCategories">Categories Name</label>
                        <input type="text" class="form-control" name="add_Categories" id="addCategories" placeholder="Enter Categories Name" required="">
                    </div>
                </div>
                <div class="modal-footer">
                    <button id="add-category" type="button" class="btn btn-primary btn-block">Add</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- End add modal -->

<script>
$( document ).ready(function() {
    
    function cat_view() {
        let html = '', i, sl = 0, status;
    	
    	$.ajax({
    		url 	: '<?= base_url('cat-views')?>',
    		method 	: 'POST',
    		data 	: { get_cat : "get_all"}, 
    		success : function ( response ) {

                data = JSON.parse( response );

                for ( i in data ) {

                    sl++;
                    if ( data[0].status == 0 ) {
                        status = '<i title="Inactive" class="text-warning fa fa-times" aria-hidden="true"> Inactive</i> ';
                    } else if ( data[0].status == 1 ) {
                        status = '<i title="Active" class="text-success fa fa-check" aria-hidden="true"> Active</i> ';
                    } else {
                        status = '<i title="Deleted" class="text-danger far fa-trash-alt"> Deleted <i>';
                    }

                    html += `
                    <tr>
                        <td>${sl}</td>
                        <td>${data[i].category_name}</td>
                        <td>${status}</td>
                        <td width="10%" class="text-center align-middle">
                            <a href="#" id="" class="btn-update" data-toggle="modal" data-target="#update-box" ><i title="Edit"  class="fas fa-user-edit"></i></a>
                            <a href="#" id="" class="text-danger delete-btn" title="Delete  "><i class="far fa-trash-alt"></i></i></a>
                        </td>
                    </tr>`;

                }

                $('#cat_view').html( html );

    		}
    	})
    }
    cat_view();

    // for add category 
    $('#add-category').click( function ( e ) {
        if ($('#add-category-form')[0].checkValidity()) {
            e.preventDefault();
            $('#add-category').text('Please wait.......');

            // ajax request for insert data

            $.ajax({
                url     : '<?= base_url('Admin_categories/insert_category') ;?>',
                method  : 'POST', 
                data    : $('#add-category-form').serialize()+"&action=add_category",
                success : function ( response ) {
                    data = JSON.parse( response );
                    $('#add_Categories').modal('hide');
                    $('#add-category-form')[0].reset();
                    cat_view();
                    popup( data.status, data.msg );
                    // console.log(data.status);
                }
            });
        }
    });


});

   
$('table').DataTable({
    order : [0, 'desc']
});


</script>