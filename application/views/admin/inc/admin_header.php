<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Admin<?= $site_title; ?> || Wcommerce</title>

  <!-- Custom fonts for this template-->
  <link href="<?= ASSETS ?>fonts/fontawesome/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.10.20/datatables.min.css" />

  <!-- Custom styles for this template-->
  <link href="<?= ASSETS ?>css/admin_panel.min.css" rel="stylesheet">

  <style>
    /*------------------style for custom alert------------------- */
    .alert-coustom {
      padding: 20px 40px;
      min-width: 380px;
      position: fixed;
      right: 0px;
      bottom: 40px;
      z-index: 10000;
      border-radius: 4px;
      overflow: hidden;
      opacity: 0;
      pointer-events: none;
    }

    .alert-coustom.showAlert {
      opacity: 1;
      pointer-events: auto;
    }

    .alert-coustom .fa {
      position: absolute;
      left: 20px;
      top: 33px;
      transform: translateY(-50%);
      font-size: 30px;
    }


    /*for warning*/
    .warning {
      background: #ffdb9b;
      border-left: 8px solid #ffa502;
    }

    .warning i {
      color: #ce8500;
    }

    .warning .msg {
      color: #ce8500;
    }

    .warning .close-btn {
      background: #ffd080;
      color: #ce8500;
    }

    .warning .close-btn:hover {
      background: #ffc766;
    }

    /*for warning end*/

    /*for success*/
    .success {
      background: #d4edda;
      border-left: 8px solid #c3e6cb;
    }

    .success i {
      color: #689873;
    }

    .success .msg {
      color: #155724;
    }

    .success .close-btn {
      background: #c3e4ca;
      color: #87a98e;
    }

    .success .close-btn:hover {
      background: #b4dcbd;
    }

    /*for status end*/

    /*for status*/
    .status {
      background: #ffdb9b;
      border-left: 8px solid #ffa502;
    }

    .status i {
      color: #ce8500;
    }

    .status .msg {
      color: #ce8500;
    }

    .status .close-btn {
      background: #ffd080;
      color: #ce8500;
    }

    .status .close-btn:hover {
      background: #ffc766;
    }

    /*for status end*/

    .alert-coustom .msg {
      padding: 0 20px;
      font-size: 18px;
    }

    .alert-coustom .close-btn {
      position: absolute;
      right: 0px;
      top: 50%;
      transform: translateY(-50%);
      font-size: 30px;
      padding: 11px 18px;
      cursor: pointer;
    }

    .alert-coustom.show {
      animation: show_slide 1s ease forwards;
    }

    @keyframes show_slide {
      0% {
        transform: translateX(100%);
      }

      40% {
        transform: translateX(-10%);
      }

      80% {
        transform: translateX(0%);
      }

      100% {
        transform: translateX(-10%);
      }
    }

    .alert-coustom.hide {
      animation: hide_slide 1s ease forwards;
    }

    @keyframes hide_slide {
      0% {
        transform: translateX(-10%);
      }

      40% {
        transform: translateX(-0%);
      }

      80% {
        transform: translateX(-10%);
      }

      100% {
        transform: translateX(100%);
      }
    }

    .alert-coustom.hide {
      display: none;
    }


    /*-----------------------style for custom alert----------------- */
  </style>

  <!-- Bootstrap core JavaScript-->
  <script src="<?= ASSETS ?>js/jquery.min.js"></script>
  <script defer="" src="<?= ASSETS ?>js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script defer="" src="<?= ASSETS ?>js/jquery.easing.min.js"></script>

  <!-- for datatable -->
  <script type="text/javascript" src="https://cdn.datatables.net/v/bs4/dt-1.10.20/datatables.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script defer="" src="<?= ASSETS ?>js/admin_panel.min.js"></script>
  <script defer="" src="<?= ASSETS ?>js/function.js"></script>
  <script defer="" src="<?= ASSETS ?>js/coustom-alert.js"></script>


</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <!-- Sidebar -->
    <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

      <!-- Sidebar - Brand -->
      <a class="sidebar-brand d-flex align-items-center justify-content-center" href="<?= base_url('admin') ?>">
        <div class="sidebar-brand-icon rotate-n-15">
          <i class="fas fa-laugh-wink"></i>
        </div>
        <div class="sidebar-brand-text mx-3">ShopBD.</div>
      </a>

      <!-- Divider -->
      <hr class="sidebar-divider my-0">



      <!-- Nav Item - Dashboard -->
      <li class="nav-item">
        <a class="nav-link" href="<?= base_url('admin'); ?>">
          <i class="fas fa-fw fa-tachometer-alt"></i>
          <span>Dashboard</span></a>
      </li>


      <!-- Divider -->
      <hr class="sidebar-divider">

      <!-- Nav Item Categories -->
      <li class="nav-item">
        <a class="nav-link" href="<?= base_url('admin-categories') ?>">
          <i class="fas fa-sort-amount-down-alt"></i>
          <span>Categories</span></a>
      </li>


      <!-- Nav Item - Product Collapse Menu -->
      <!-- Nav Item Brand -->
      <li class="nav-item">
        <a class="nav-link" href="<?= base_url('AdminBrand') ?>">
          <i class="fas fa-sort-amount-down-alt"></i>
          <span>Brand</span></a>
      </li>

      <!-- Nav Item Department -->
      <li class="nav-item">
        <a class="nav-link" href="<?= base_url('admin-department') ?>">
          <i class="fas fa-building"></i>
          <span>Department</span></a>
      </li>

      <!-- Nav Item Product -->
      <li class="nav-item">
        <a class="nav-link" href="<?= base_url('adminProduct') ?>">
          <i class="fab fa-product-hunt"></i>
          <span>Products</span></a>
      </li>

      <!-- Nav Item Order -->
      <li class="nav-item">
        <a class="nav-link" href="<?= base_url('AdminOrder') ?>">
          <i class="fab fa-product-hunt"></i>
          <span>Order</span></a>
      </li>



      <!-- Nav Item Collection -->
      <!-- <li class="nav-item">
        <a class="nav-link" href="<?= base_url('admin-collection') ?>">
          <i class="fas fa-sort-amount-down-alt"></i>
          <span>Collection</span></a>
      </li> -->



      <!-- Divider -->
      <hr class="sidebar-divider d-none d-md-block">

      <!-- Sidebar Toggler (Sidebar) -->
      <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
      </div>

    </ul>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->
        <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

          <!-- Sidebar Toggle (Topbar) -->
          <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
            <i class="fa fa-bars"></i>
          </button>

          <!-- Topbar Navbar -->
          <ul class="navbar-nav ml-auto">

            <!-- Nav Item - Alerts -->
            <li class="nav-item dropdown no-arrow mx-1">
              <a class="nav-link dropdown-toggle" href="#" id="alertsDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-bell fa-fw"></i>
                <!-- Counter - Alerts -->
                <span class="badge badge-danger badge-counter">3+</span>
              </a>
              <!-- Dropdown - Alerts -->
              <div class="dropdown-list dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="alertsDropdown">
                <h6 class="dropdown-header">
                  Alerts Center
                </h6>

                <a class="dropdown-item d-flex align-items-center" href="#">
                  <div class="mr-3">
                    <div class="icon-circle bg-primary">
                      <i class="fas fa-file-alt text-white"></i>
                    </div>
                  </div>
                  <div>
                    <div class="small text-gray-500">December 12, 2019</div>
                    <span class="font-weight-bold">A new monthly report is ready to download!</span>
                  </div>
                </a>


                <a class="dropdown-item text-center small text-gray-500" href="#">Show All Alerts</a>
              </div>
            </li>

            <!-- Nav Item - Messages -->
            <li class="nav-item dropdown no-arrow mx-1">
              <a class="nav-link dropdown-toggle" href="#" id="messagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-envelope fa-fw"></i>
                <!-- Counter - Messages -->
                <span class="badge badge-danger badge-counter">7</span>
              </a>
              <!-- Dropdown - Messages -->
              <div class="dropdown-list dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="messagesDropdown">
                <h6 class="dropdown-header">
                  Message Center
                </h6>
                <a class="dropdown-item d-flex align-items-center" href="#">
                  <div class="dropdown-list-image mr-3">
                    <img class="rounded-circle" src="https://source.unsplash.com/fn_BT9fwg_E/60x60" alt="">
                    <div class="status-indicator bg-success"></div>
                  </div>
                  <div class="font-weight-bold">
                    <div class="text-truncate">Hi there! I am wondering if you can help me with a problem I've been having.</div>
                    <div class="small text-gray-500">Emily Fowler · 58m</div>
                  </div>
                </a>
                <a class="dropdown-item text-center small text-gray-500" href="#">Read More Messages</a>
              </div>
            </li>

            <div class="topbar-divider d-none d-sm-block"></div>

            <!-- Nav Item - User Information -->
            <li class="nav-item dropdown no-arrow">
              <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="mr-2 d-none d-lg-inline text-gray-600 small"><?= $this->session->userdata('admin_name'); ?></span>
                <img class="img-profile rounded-circle" src="https://source.unsplash.com/QAB-WJcbgJk/60x60">
              </a>
              <!-- Dropdown - User Information -->
              <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                <a class="dropdown-item" href="#">
                  <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                  Profile
                </a>
                <a class="dropdown-item" href="#">
                  <i class="fas fa-cogs fa-sm fa-fw mr-2 text-gray-400"></i>
                  Settings
                </a>
                <a class="dropdown-item" href="#">
                  <i class="fas fa-list fa-sm fa-fw mr-2 text-gray-400"></i>
                  Activity Log
                </a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">
                  <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                  Logout
                </a>
              </div>
            </li>

          </ul>

        </nav>
        <!-- End of Topbar -->

        <!-- Begin Page Content -->
        <div class="container-fluid">