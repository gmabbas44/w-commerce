
    <!-- Breadcrumb Section Begin -->
    <div class="breacrumb-section">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="breadcrumb-text">
                        <a href="#"><i class="fa fa-home"></i> Home</a>
                        <span>Collection</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Breadcrumb Section Begin -->

    <!-- Product Shop Section Begin -->
    <section class="product-shop spad">
        <div class="container">
            <div class="row">
                
               

                <div class="col-lg-12 order-1 order-lg-2">
                    <div class="product-show-option">
                        <div class="row justify-content-end">
                            <div class="col-lg-5 col-md-5 text-right">
                                <p>Show 01- 09 Of 36 Product</p>
                            </div>
                        </div>
                    </div>
                    <div class="product-list">
                        <div class="row">
                        <?php foreach ( $allProducts as $product ) :?> 
                            <div class="col-lg-3 col-sm-6">
                                <div class="product-item">
                                    <div class="pi-pic">
                                        <a href="<?= base_url('product-details/'.encript_id($product->product_id ))?>"><img src="<?= ASSETS ;?>img/products/<?= $product->img_name?>" alt=""></a>
                                        <div class="sale pp-sale">Sale</div>
                                        <div class="icon wishlist" id="<?= encript_id($product->product_id ) ;?>">
                                            <i class="icon_heart_alt"></i>
                                        </div>
                                        <ul>
                                            <li class="w-icon active"><a 
                                        productId="<?= encript_id($product->product_id ) ;?>" class="addToCart" href="#"><i class="icon_bag_alt"></i></a></li>
                                            <li class="quick-view" id="<?= encript_id($product->product_id ) ;?>"><a href="#" data-toggle="modal" data-target="#product_details">+ Quick View</a></li>
                                            <li class="w-icon"><a title="Add to compare" class="compare" href="<?= base_url('compare/'.encript_id($product->cat_id))?>"><i class="fa fa-random"></i></a></li>
                                        </ul>
                                    </div>
                                    <div class="pi-text">
                                        <div class="catagory-name"><?= $product->category_name ;?></div>
                                        <a href="#">
                                            <h5><?= $product->title ; ?></h5>
                                        </a>
                                        <div class="product-price">
                                            $<?= $product->mrp ; ?>
                                            <span>$<?= $product->price ; ?></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach ;?>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Product Shop Section End -->

