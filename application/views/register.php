

    <!-- Breadcrumb Section Begin -->
    <div class="breacrumb-section">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="breadcrumb-text">
                        <a href="#"><i class="fa fa-home"></i> Home</a>
                        <span>Register</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Breadcrumb Form Section Begin -->
    <!-- for check error -->
    <?php 
        $msg = $this->session->userdata( 'singup' );
        if ( $msg ) :
    ?>
    <div class="container">
        <div class="alert alert-success text-center">
            <?= $msg ; ?>
        </div>
    </div>
    <?php endif; ?>

    <?php 
        $failure = $this->session->userdata( 'failure' );
        if ( $failure ) :
    ?>
    <div class="container">
        <div class="alert alert-success text-center">
            <?= $failure ; ?>
        </div>
    </div>
    <?php endif; ?>
    <!--/ for check error -->
    
    <!-- Register Section Begin -->
    <div class="register-login-section spad">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 offset-lg-3">
                    <div class="register-form">
                        <h2>Register</h2>
                        <!-- <form action="#"> -->
                        <?= form_open('register/singup') ; ?>
                            <input type="hidden" name="<?= $this->security->get_csrf_token_name() ; ?>" value="<?= $this->security->get_csrf_hash() ; ?>">
                            <div class="group-input">
                                <label for="username">Username *</label>
                                <?= form_input(['type'=>'text', 'name' => 'username', 'id' => 'username', 'placeholder' => 'Enter your Username', 'required' => ''], set_value('username')); ?>
                                <small class="text-danger"><?= form_error('username') ;?></small>
                            </div>
                            <div class="group-input">
                                <label for="email">Email address *</label>
                                <?= form_input(['type'=>'email', 'name' => 'email', 'id' => 'email', 'placeholder' => 'Enter your Email', 'required' => ''], set_value('email')); ?>
                                <small class="text-danger"><?= form_error('email'); ?></small>
                            </div>
                            <div class="group-input">
                                <label for="mobile">Mobile *</label>
                                <?= form_input(['type'=>'number', 'name' => 'mobile', 'id' => 'mobile', 'placeholder' => 'Enter your Mobile NO', 'required' => ''], set_value('mobile')); ?>
                                <small class="text-danger"><?= form_error('mobile'); ?></small>
                            </div>
                            <div class="group-input">
                                <label for="pass">Password *</label>
                                <?= form_password(['name' => 'password', 'id' => 'pass', 'placeholder' => 'Enter your password', 'required' => '']); ?>
                                <small class="text-danger"><?= form_error('password'); ?></small>
                            </div>
                            <div class="group-input">
                                <label for="conf_pass">Confirm Password *</label>
                                <?= form_password(['name' => 'conf_pass', 'id' => 'conf_pass', 'placeholder' => 'Enter your confirm password', 'required' => '']); ?>
                                <small class="text-danger"><?= form_error('conf_pass'); ?></small>
                            </div>
                            
                            
                            <button type="submit" class="site-btn register-btn">REGISTER</button>
                        </form>
                        <div class="switch-login">
                            <a href="<?= base_url('login') ; ?>" class="or-login">Or Login</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Register Form Section End -->
    
