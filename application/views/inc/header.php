<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="Fashi Template">
    <meta name="keywords" content="Fashi, unica, creative, html">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" href="<?= ASSETS ?>img/site_logo.png" type="image/png" sizes="16x16">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?= $site_title == '' ? 'w-Commerce' : "$site_title || w-Commerce" ; ?> </title>

    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css?family=Muli:300,400,500,600,700,800,900&display=swap" rel="stylesheet">


    <!-- Css Styles -->
    <?=link_tag('assets/css/bootstrap.min.css');?>
    <?=link_tag('assets/css/font-awesome.min.css');?>
    <?=link_tag('assets/css/themify-icons.css');?>
    <?=link_tag('assets/css/elegant-icons.css');?>
    <?=link_tag('assets/css/owl.carousel.min.css');?>
    <?=link_tag('assets/css/nice-select.css');?>
    <?=link_tag('assets/css/jquery-ui.min.css');?>
    <?=link_tag('assets/css/slicknav.min.css');?>
    <?=link_tag('assets/css/style.css');?>
    <script  src="<?= ASSETS ?>js/function.js"></script>
    <script  src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script  src="<?= ASSETS ?>js/coustom-alert.js"></script>
    <?=link_tag('assets/css/sweetalert.css');?>
    <script defer src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>

</head>

<body>
    <!-- Page Preloder -->
    <div id="preloder">
        <div class="loader"></div>
    </div>

    <!-- Header Section Begin -->
    <header class="header-section">
        <div class="header-top">
            <div class="container">
                <div class="ht-left">
                    <div class="mail-service">
                        <i class=" fa fa-envelope"></i>
                        gmabbas44@gmail.com
                    </div>
                    <div class="phone-service">
                        <i class=" fa fa-user"></i>
                        Welcome! 
                        <?=  $this->session->userdata('username') ? "<a href=". base_url('customer-profile').">".$this->session->userdata('username')."</a>" : 'User'; ?>
                        
                    </div>
                    <div class="phone-service">
                        <i class=" fa fa-phone"></i>
                        +880 1845 10 90 44
                    </div>
                    
                </div>
                <div class="ht-right">
                <?php  if ( $this->session->userdata('username')) : ?>
                    <?= anchor('logout', '<i class="fa fa-sign-out"></i>Logout', ['class'=> 'login-panel']); ?>
                <?php else : ?>
                    <?= anchor('login', '<i class="fa fa-user" aria-hidden="true"></i>Login', ['class'=> 'login-panel']); ?>
                <?php endif; ?>
                    <div class="lan-selector">
                        <select class="language_drop" name="countries" id="countries" style="width:300px;">
                            <option value='yt' data-image="<?= ASSETS ?>img/flag-1.jpg" data-imagecss="flag yt"
                                data-title="English">English</option>
                            <!-- <option value='yu' data-image="<?= ASSETS ?>img/BD-Flag.png" data-imagecss="flag yu"
                                data-title="Bangladesh">BD</option> -->
                        </select>
                    </div>
                    <div class="top-social">
                        <a href="#"><i class="ti-facebook"></i></a>
                        <a href="#"><i class="ti-twitter-alt"></i></a>
                        <a href="#"><i class="ti-linkedin"></i></a>
                        <a href="#"><i class="ti-pinterest"></i></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="inner-header">
            <div class="container">
                <div class="row">
                    <div class="col-lg-2 col-md-2">
                        <div class="logo">
                            <a href="<?= base_url() ; ?>">
                                <img src="<?= ASSETS ?>img/logo.png" alt="">
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-7 col-md-7">
                        <div class="advanced-search">
                            <button type="button" class="category-btn">All Categories</button>
                            <div class="input-group">
                                <input type="text" placeholder="What do you need?">
                                <button type="button"><i class="ti-search"></i></button>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 text-right col-md-3">
                        <ul class="nav-right">
                        <?php if ( $this->session->userdata('user_id')) :?>
                            <li class="heart-icon">
                                <a href="<?= base_url('wishlist'); ?>">
                                    <i class="icon_heart_alt"></i>
                                    <span id="wishlistCount">0</span>
                                </a>
                            </li>
                        <?php endif; ?>
                            <li class="cart-icon">
                                <a href="#">
                                    <i class="icon_bag_alt"></i>
                                    <span id="countCartProduct">0</span>
                                </a>
                                <div class="cart-hover">
                                    <div class="select-items">
                                        <table>
                                            <tbody id="top_cart">
                                                
                                               
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="select-total">
                                        <span>total:</span>
                                        <h5 id="cart_total">$0</h5>
                                    </div>
                                    <div class="select-button">
                                        <a href="<?= base_url('cart') ;?>" class="primary-btn view-card">VIEW CART</a>
                                        <a href="<?= base_url('cart/checkout') ;?>" class="primary-btn checkout-btn">CHECK OUT</a>
                                    </div>
                                </div>
                            </li>
                            <li class="cart-price"></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="nav-item">
            <div class="container">
                <div class="nav-depart">
                    <div class="depart-btn">
                        <i class="ti-menu"></i>
                        <span>All departments</span>
                        <ul class="depart-hover">
                            <!-- <li class="active"><a href="#">Women’s Clothing</a></li> -->
                            <?php foreach ( $getDepartment as $department ): ?>
                            <li><a href="<?= base_url('department/').encript_id($department->dep_id) ;?>"><?= $department->dep_name ; ?></a></li>
                            <?php endforeach;?>
                            
                        </ul>
                    </div>
                </div>
                <nav class="nav-menu mobile-menu">
                    <ul>
                        <!-- <li class="active"><a href="<?= base_url() ; ?>">Home</a></li> -->
                        <li class="active"><?= anchor('', 'Home'); ?></li>
                        <li><?= anchor('shop', 'Shop'); ?></li>
                        <li class="mobile-nav"><a href="#">Departments</a>
                            <ul class="dropdown">

                                <?php foreach ( $getDepartment as $department ): ?>
                                <li><a href="<?= base_url('department/').encript_id($department->dep_id) ;?>"><?= $department->dep_name ; ?></a></li>
                                <?php endforeach;?>

                            </ul>
                        </li>
                        <li><a href="#">Collection</a>
                            <ul class="dropdown">

                                <?php foreach( $getCollection as $collection ) :?>
                                    <li><a href="<?= base_url('collection/'.encript_id($collection->coll_id) ) ;?>"><?= $collection->name; ?></a></li>
                                <?php endforeach;?>

                            </ul>
                        </li>
                        <li><?= anchor('contact', 'Contact'); ?></li>
                    </ul>
                </nav>
                <div id="mobile-menu-wrap"></div>
            </div>
        </div>
    </header>
    <!-- Header End -->