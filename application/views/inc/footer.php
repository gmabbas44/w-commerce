<!-- Partner Logo Section Begin -->
<div class="partner-logo">
  <div class="container">
    <div class="logo-carousel owl-carousel">
      <div class="logo-item">
        <div class="tablecell-inner">
          <img src="<?= ASSETS ?>img/logo-carousel/logo-1.png" alt="">
        </div>
      </div>
      <div class="logo-item">
        <div class="tablecell-inner">
          <img src="<?= ASSETS ?>img/logo-carousel/logo-2.png" alt="">
        </div>
      </div>
      <div class="logo-item">
        <div class="tablecell-inner">
          <img src="<?= ASSETS ?>img/logo-carousel/logo-3.png" alt="">
        </div>
      </div>
      <div class="logo-item">
        <div class="tablecell-inner">
          <img src="<?= ASSETS ?>img/logo-carousel/logo-4.png" alt="">
        </div>
      </div>
      <div class="logo-item">
        <div class="tablecell-inner">
          <img src="<?= ASSETS ?>img/logo-carousel/logo-5.png" alt="">
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Partner Logo Section End -->

<!-- Footer Section Begin -->
<footer class="footer-section">
  <div class="container">
    <div class="row">
      <div class="col-lg-3">
        <div class="footer-left">
          <div class="footer-logo">
            <a href="#"><img src="<?= ASSETS ?>img/footer-logo.png" alt=""></a>
          </div>
          <ul>
            <li>Address: Chattogram</li>
            <li>Phone: +880 1845 10 90 44/li>
            <li>Email: gmabbas44@gmail.com</li>
          </ul>
          <div class="footer-social">
            <a href="#"><i class="fa fa-facebook"></i></a>
            <a href="#"><i class="fa fa-instagram"></i></a>
            <a href="#"><i class="fa fa-twitter"></i></a>
            <a href="#"><i class="fa fa-pinterest"></i></a>
          </div>
        </div>
      </div>
      <div class="col-lg-2 offset-lg-1">
        <div class="footer-widget">
          <h5>Information</h5>
          <ul>
            <li><a href="#">About Us</a></li>
            <li><a href="#">Checkout</a></li>
            <li><a href="#">Contact</a></li>
            <li><a href="#">Serivius</a></li>
          </ul>
        </div>
      </div>
      <div class="col-lg-2">
        <div class="footer-widget">
          <h5>My Account</h5>
          <ul>
            <li><a href="#">My Account</a></li>
            <li><a href="#">Contact</a></li>
            <li><a href="#">Shopping Cart</a></li>
            <li><a href="#">Shop</a></li>
          </ul>
        </div>
      </div>
      <div class="col-lg-4">
        <div class="newslatter-item">
          <h5>Join Our Newsletter Now</h5>
          <p>Get E-mail updates about our latest shop and special offers.</p>
          <form action="#" class="subscribe-form">
            <input type="text" placeholder="Enter Your Mail">
            <button type="button">Subscribe</button>
          </form>
        </div>
      </div>
    </div>
  </div>
  <div class="copyright-reserved">
    <div class="container">
      <div class="row">
        <div class="col-lg-12">
          <div class="copyright-text">
            <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
            Copyright &copy;
            <script>
              document.write(new Date().getFullYear());
            </script> 
            <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
          </div>
          <div class="payment-pic">
            <img src="<?= ASSETS ?>img/payment-method.png" alt="">
          </div>
        </div>
      </div>
    </div>
  </div>
</footer>
<!-- Footer Section End -->


<!-- model for prodect details -->
<div class="modal fade" id="product_details" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg modal-custom">
    <div class="modal-content">
      <a href="#" class="model-close" data-dismiss="modal">Close</a>
      <div class="row">
        <div class="col-xs-12 col-sm-5 col-md-5">
          <div class="product-carousel">
            <!--Carousel Wrapper-->
            <div id="carousel-thumb" class="carousel slide carousel-fade carousel-thumbnails carousel-slide" data-ride="carousel">
              <!--Slides-->
              <div class="carousel-inner carousel-inner-img" role="listbox">
                <!-- <div class="carousel-item active">
                                <img class="d-block" src="<?= ASSETS . 'img/products/man-3.jpg' ?>" alt="First slide">
                            </div>
                            <div class="carousel-item">
                                <img class="d-block" src="<?= ASSETS . 'img/products/man-4.jpg' ?>" alt="Second slide">
                            </div>
                            <div class="carousel-item">
                                <img class="d-block" src="<?= ASSETS . 'img/products/man-5.jpg' ?>" alt="Third slide">
                            </div> -->
              </div>
              <!--/.Slides-->

              <!--Controls-->
              <a class="carousel-control-prev" href="#carousel-thumb" role="button" data-slide="prev">
                <span class="bg-warning carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
              </a>
              <a class="carousel-control-next" href="#carousel-thumb" role="button" data-slide="next">
                <span class="bg-warning carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
              </a>
              <!--/.Controls-->
              <ol class="carousel-indicators carousel-indicators-position">
                <!-- <li data-target="#carousel-thumb" data-slide-to="0" class="active"> <img class="d-block w-100" src="<?= ASSETS . 'img/products/man-3.jpg' ?>"
                            class="img-fluid"></li>
                            <li data-target="#carousel-thumb" data-slide-to="1"><img class="d-block w-100" src="<?= ASSETS . 'img/products/man-4.jpg' ?>"
                            class="img-fluid"></li>
                            <li data-target="#carousel-thumb" data-slide-to="2"><img class="d-block w-100" src="<?= ASSETS . 'img/products/man-5.jpg' ?>"
                            class="img-fluid"></li> -->
              </ol>
            </div>
            <!--/.Carousel Wrapper-->
          </div>

        </div>
        <div class="col-xs-12 col-sm-7 col-md-7">
          <div class="product-details">

            <h2 class="product-title"></h2>
            <ul>
              <li><span class="fa fa-star"></span></li>
              <li><span class="fa fa-star"></span></li>
              <li><span class="fa fa-star"></span></li>
              <li><span class="fa fa-star-half-o"></span></li>
              <li><span class="fa fa-star-o"></span></li>
            </ul>
            <h2 class="product-price">TK <span class="mrp"></span> <span class="del-price">Tk. <del class="price"></del></span></h2>
            <p class="prodect-description"></p>

            <form action="" class="order-form">
              <div class="row">
                <!-- <div class="col-md-4">
                                <div class="choose-color">
                                    <h3>Color</h3>
                                    <div class="color-item">
                                        <input  type="radio" name="color" value="black" id="black">
                                        <label for="black"></label>
                                    </div>
                                    <div class="color-item">
                                        <input  type="radio" name="color" value="yellow" id="yellow">
                                        <label for="yellow"></label>
                                    </div>
                                    <div class="color-item">
                                        <input  type="radio" name="color" value="blue" id="blue">
                                        <label for="blue"></label>
                                    </div>
                                </div>
                            </div>  -->
                <!-- <div class="col-md-4">
                                <div class="choose-size">
                                    <h3>Size</h3>
                                    <div class="size-item">
                                        <input  type="radio" name=size" value="s" id="s">
                                        <label for="s"></label>
                                    </div>
                                    <div class="size-item">
                                        <input  type="radio" name=size" value="m" id="sm">
                                        <label for="m"></label>
                                    </div>
                                    <div class="size-item">
                                        <input  type="radio" name=size" value="l" id="l">
                                        <label for="l"></label>
                                    </div>
                                    <div class="size-item">
                                        <input  type="radio" name="size" value="xl" id="xl">
                                        <label for="xl"></label>
                                    </div>
                                </div>
                            </div>  -->
                <div class="col-md-4">
                  <div class="qty">
                    <label>Quantity</label>
                    <input id="p-qry" type="number" name="p-qry">

                  </div>
                </div>

                <div class="col-md-4">
                  <div class="submit-btn">
                    <button id="add-to-cart">Add To Cart</button>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>

    </div>
  </div>
</div>

<!-- model for prodect details end -->


<!-- coustom alert -->

<div class="alert-coustom ">
  <span class="i">

  </span>
  <span class="msg">
  </span>
  <span class="close-btn">
    &times;
  </span>
</div>

<!-- coustom alert end -->


<!-- Js Plugins -->

<!-- <script src="<?= ASSETS ?>js/jquery-3.3.1.min.js"></script> -->

<script src="<?= ASSETS ?>js/bootstrap.min.js"></script>
<script src="<?= ASSETS ?>js/jquery-ui.min.js"></script>
<script src="<?= ASSETS ?>js/jquery.countdown.min.js"></script>
<script src="<?= ASSETS ?>js/jquery.nice-select.min.js"></script>
<script src="<?= ASSETS ?>js/jquery.zoom.min.js"></script>
<script src="<?= ASSETS ?>js/jquery.dd.min.js"></script>
<script src="<?= ASSETS ?>js/jquery.slicknav.js"></script>
<script src="<?= ASSETS ?>js/owl.carousel.min.js"></script>
<script src="<?= ASSETS ?>js/main.js"></script>
<script src="<?= ASSETS ?>js/coustom-alert.js"></script>


<script>
  //popup('warning', 'product add successfully');

  $(document).ready(function() {

    let csrfName = '<?php echo $this->security->get_csrf_token_name(); ?>',
      csrfHash = '<?php echo $this->security->get_csrf_hash(); ?>';

    $('body').on('click', '.addToCart', function(e) {
      e.preventDefault();
      let product_id = $(this).attr('productId');

      addToCart(product_id, qty = 1);
      countProductAndPrice();
      topCart();

    });

    function countProductAndPrice() {
      $.ajax({
        url: '<?= base_url('cart/countQtyPrice') ?>',
        method: 'POST',
        data: {
          data_action: 'count',
          [csrfName]: csrfHash
        },
        success: function(response) {
          data = JSON.parse(response);
          csrfName = data.csrfName;
          csrfHash = data.csrfHash;
          $('#countCartProduct').text(data.qty);
          $('.cart-price').text('$' + parseFloat(data.total_price));
        }
      });
    }
    countProductAndPrice();


    function topCart() {
      let html = '',
        total = 0,
        i;
      $.ajax({
        url: '<?= base_url('cart/action'); ?>',
        method: 'POST',
        data: {
          data_action: 'fetch_cart'
        },

        success: function(response) {
          data = JSON.parse(response);
          if (data == 0) {
            html += `<tr><td class="text-danger text-center" colspan="3">Cart Empty</td></tr>`;
          } else {
            for (i in data) {
              total = parseFloat(total) + parseFloat(data[i].total_price);
              html += `
                    <tr>
                        <td class="si-pic"><img src="<?= ASSETS ?>img/products/${data[i].picture}" alt=""></td>
                        <td class="si-text">
                            <div class="product-selected">
                                <p>$${data[i].unite_price} x ${data[i].qty}</p>
                                <h6>Kabino Bedside Table</h6>
                            </div>
                        </td>
                        <td class="si-close">
                            <i class="ti-close"></i>
                        </td>
                    </tr>
                    `;
            }
            //$('#top_cart').html(html);
            $('#cart_total').text('$' + total);
          }
          $('#top_cart').html(html);
        }

      });
    }
    topCart();

    // function for quick view
    $('body').on('click', '.quick-view', function(e) {

      e.preventDefault();
      let product_id = $(this).attr('id');

      let carousel_inner = '',
        carousel_indicators = '',
        active = '';

      $.ajax({
        url: "<?= base_url('product/quickView') ?>",
        method: "POST",
        data: {
          product_id: product_id,
          action: 'quickView'
        },
        success: function(response) {
          data = JSON.parse(response);

          $('.product-title').text(data.getProBYId.title);
          $('.mrp').html(data.getProBYId.mrp);
          $('.price').html(data.getProBYId.price);
          $('.prodect-description').html(data.getProBYId.short_description);

          for (let i in data.prodeuctImg) {

            if (i == 0) {
              active = 'active';
            }

            carousel_inner += `
                        <div class="carousel-item ${active}">
                            <img class="d-block" src="<?= ASSETS . 'img/products/${data.prodeuctImg[i].img_name}' ?>" alt="">
                        </div>
                        `;

            carousel_indicators += `
                            <li data-target="#carousel-thumb" data-slide-to="${i}" class=""> <img class="d-block w-100" src="<?= ASSETS . 'img/products/${data.prodeuctImg[i].img_name}' ?>"
                            class="img-fluid"></li>`;

          }
          $('.carousel-inner').html(carousel_inner);
          $('.carousel-indicators').html(carousel_indicators);

        }
      });
    });

    // wishlist product

    $('body').on('click', '.wishlist', function(e) {
      e.preventDefault();
      let id = $(this).attr('id');

      $.ajax({
        url: '<?= base_url('add-wishlist') ?>',
        method: 'POST',
        data: {
          id: id,
          action: 'wishlist'
        },
        success: function(response) {
          data = JSON.parse(response);
          if (data.status == 'nologin') {
            window.location = '<?= base_url('login') ?>';
          } else {
            popup(data.status, data.message);
            wishlistCount();
          }
        }
      });

    });

    function wishlistCount() {

      $.ajax({
        url: '<?= base_url('wishlistCount'); ?>',
        method: 'POST',
        data: {
          action: 'wishlistCount'
        },
        success: function(response) {
          //console.log(response);
          $('#wishlistCount').text(response);
        }
      });
    }
    wishlistCount();

    function addToCart(id, qty = 1) {
      $.ajax({
        url: '<?= base_url('cart/addToCart'); ?>',
        method: 'POST',
        data: {
          action: 'addToCart',
          product_id: id,
          qty : qty
        },
        success: function(response) {

          data = JSON.parse(response);
          popup(data.status, data.message);

        }
      });
    }

    $('body').on('click', '.addtocart', function(e) {
      e.preventDefault()
      const product_id = $(this).attr('proudct_id')
      const qty = $('#qty').val()

      addToCart(product_id, qty);
      countProductAndPrice();
      topCart();

    })




  });
</script>

</body>

</html>