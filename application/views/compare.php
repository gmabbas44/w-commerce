<!-- Breadcrumb Section Begin -->
<div class="breacrumb-section">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="breadcrumb-text product-more">
                    <a href="<?= base_url(''); ?>"><i class="fa fa-home"></i> Home</a>
                    <a href="./shop.html">Compare</a>
                    <span>View</span>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Breadcrumb Section Begin --> 

<!-- Compare Section Begin -->
<section class="shopping-cart spad">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="cart-table">
                    <table>
                        <thead>
                            <tr>
                                <th>Image</th>
                                <th >Product Name</th>
                                <th>Price</th>
                                <th>Add to Cart</th>
                            </tr>
                        </thead>
                        
                        <tbody>
                            <?php foreach ( $category_products as $category_product ) : ?>
                                <tr>
                                    <td class="cart-pic first-row"><img src="<?= ASSETS . 'img/products/'. $category_product->img_name; ?>" alt=""></td>
                                    <td class=" first-row">
                                        <h5><?= $category_product->title; ?></h5>
                                    </td>
                                    <td class="p-price first-row">Tk. <?= $category_product->mrp ; ?></td>
                                
                                    <td class="first-row"><a class="btn btn-warning btn-sm text-white addToCart rounded-0" href="#" productId="<?= encript_id( $category_product->product_id ); ?>"><i class="fa fa-cart-plus" aria-hidden="true"></i></a></td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Compare Section End -->


<script>
    $( function () {
        
        

        
    });
</script>