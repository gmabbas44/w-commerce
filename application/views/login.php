    <!-- Breadcrumb Section Begin -->
    <div class="breacrumb-section">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="breadcrumb-text">
                        <a href="#"><i class="fa fa-home"></i> Home</a>
                        <span>Login</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Breadcrumb Form Section Begin -->

    <!-- Register Section Begin -->
    <div class="register-login-section spad">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 offset-lg-3">
                    <div class="login-form">
                        <h2>Login</h2>
                        <?= form_open('register/login') ; ?>
                            <input type="hidden" name="<?= $this->security->get_csrf_token_name() ; ?>" value="<?= $this->security->get_csrf_hash() ; ?>">
                            <div class="group-input">
                                <label for="user">Username or email address *</label>
                                <?= form_input(['type'=>'text', 'name' => 'user', 'id' => 'user', 'placeholder' => 'Enter your Username or email', 'required' => ''], set_value('user')); ?>
                                <small class="text-danger"><?= form_error( 'user' ) ;?></small>
                            </div>
                            <div class="group-input">
                                <label for="pass">Password *</label>
                                <?= form_password(['name' => 'password', 'id' => 'pass', 'placeholder' => 'Enter your password', 'required' => '']); ?>
                                <small class="text-danger"><?= form_error( 'password' ) ;?></small>
                            </div>
                            <div class="group-input gi-check">
                                <div class="gi-more">
                                    <label for="save-pass">
                                        Save Password
                                        <input type="checkbox" id="save-pass">
                                        <span class="checkmark"></span>
                                    </label>
                                    <a href="#" class="forget-pass">Forget your Password</a>
                                </div>
                            </div>
                            <button type="submit" name="login" class="site-btn login-btn">Sign In</button>
                        <?= form_close() ;?>
                        <div class="switch-login">
                            <a href="<?= base_url('register') ; ?>" class="or-login">Or Create An Account</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Register Form Section End -->

