<?php

class AdminOrder_model extends CI_Model
{
  public function getOrder()
  {
    return $this->db->select('
      orders.ord_id,
      orders.user_id,
      user.fullname, 
      shipping_info.shipping_fullname, 
      shipping_info.shipping_phone, 
      orders.payment_type, 
      orders.total_price, 
      orders.order_status,
      orders.shipping_date'
    )
    ->from('orders')
    ->join('user', 'user.user_id=orders.user_id')
    ->join('shipping_info', 'orders.shipping_id=shipping_info.shipping_id')
    ->order_by('order_status', "desc")
    ->get()
    ->result();
  }

  public function coustomer($id)
  {
    return $this->db->where('user_id', $id)
                    ->get('user')
                    ->result();
  }
  public function getOrderById($ord_id)
  {
    return $this->db->where('ord_id', $ord_id)
                    ->get('orders')
                    ->row();
  }

  
}