<?php 

class Wishlist_model extends CI_Model
{
	
	public function check_wishlist( $product_id, $customer_id )
	{
		return $this->db->select('product_id')
						->from('wishlist')
						->where(['product_id' => $product_id, 'user_id' => $customer_id ])
						->get()
						->row();
	}
	public function add_wishlist( $data )
	{
		$this->db->insert( 'wishlist', $data );
		return $this->db->insert_id();
	}
	public function getCustomerWishlist( $customer_id )
	{
		return $this->db->where('user_id', $customer_id )
						->get('wishlist')
						->result();
	}
	public function wishlistCount( $customer_id )
	{
		return $this->db->where('user_id', $customer_id )
						->get('wishlist')
						->num_rows();
	}
	public function del_wishlist( $del_id  )
	{
		return $this->db->where(['ws_id' => $del_id])
						->delete('wishlist');
	}
	public function getProductinfo($product_id)
	{
		return $this->db->query("SELECT p.product_id,  p.title, p.mrp, i.img_name FROM products as p JOIN `product_image` AS i ON i.product_id = p.product_id WHERE p.product_id = $product_id group by p.product_id")->row();
	}

	private function delete( $data = [], $table )
	{
		return $this->db->where( $data )
						->delete($table);
	} 
	public function del_wish( $customer_id, $product_id)
	{
		$this->delete(['user_id' => $customer_id, 'product_id' => $product_id], 'wishlist');
	}

}


 ?>