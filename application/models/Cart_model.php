<?php 

class Cart_model extends CI_Model
{
	
	public function add( $data )
	{
		$this->db->insert( 'carts', $data );
		return $this->db->insert_id();
	}

	public function getCartPro( $sid )
	{
		$this->db->where( 'sid', $sid ) ;
		return $this->db->get('carts')->result();
	}

	public function getDistricts()
	{
		return $this->db->get('districts')->result();
	}

	public function getShippingCost($id){
		return $this->db->select('shipping_cost')
						->where('id', $id)
						->get('districts')
						->row();
	}

	public function checkaddedPro( $sid , $product_id)
	{
		$this->db->where([ 'sid' => $sid, 'product_id' => $product_id ]);
		return $this->db->get('carts')->row();
	}

	public function countCartPro( $sid )
	{
		$this->db->select('qty, total_price')
				 ->where([ 'sid' => $sid ]);
		return $this->db->get('carts')->result();
	}
	public function checkInStock( $product_id )
	{
		$this->db->select('in_stock, mrp')
				 ->where( ['product_id' => $product_id] );
		return $this->db->get( 'products' )->row();
	}

	public function updateQty( $product_id, $qty, $mrp )
	{
		$total_price = $mrp * $qty ;
		$this->db->where( 'product_id',$product_id );
        $this->db->update( 'carts', ['qty'=> $qty, 'total_price' => $total_price]) ;
	}

	public function removePro( $cart_id )
	{
		$this->db->where( 'cart_id', $cart_id ) ;
		return $this->db->delete('carts');
	}

	public function shipping_info( $s )
	{
		$this->db->insert('shipping_info', $s);
		return $this->db->insert_id();
	}
	public function order( $order  )
	{
		$this->db->insert('orders', $order);
		return $this->db->insert_id();
	}

	public function sum( $id )
	{
		return $this->db->select_sum('total_price')
						->where(['sid'=> $id])
						->get('carts')
						->row();
	}

	public function order_details( $data )
	{
		$this->db->insert( 'order_details', $data );
		return $this->db->insert_id();
	}

	public function deleteCartProduct($sid)
	{
		$this->db->where('sid',$sid);
        $this->db->delete('carts');
        return true;
	}
	public function get_customer_info( $sid )
	{
		return $this->db->select('*')
					->from('user')
				 	->join('districts','districts.id=user.city')
				 	->get()
				 	->row();
	}
	public function order_info( $ord_id )
	{
		return $this->db
					->where(['order_status'=>'0', 'ord_id'=> $ord_id])
				 	->get('orders')
				 	->row();
	}
	public function get_shipping_info($shipping_id)
	{
		return $this->db->select('*')
					->from('shipping_info')
					->join('districts','districts.id=shipping_info.districts_id')
					->where('shipping_id', $shipping_id)
				 	->get()
				 	->row();
	}
	public function get_order_details( $ord_id )
	{
		return $this->db->select('title, mrp, qty, order_details.price')
					->from('order_details')
					->join('products','products.product_id=order_details.product_id')
					->where(['order_id' => $ord_id])
				 	->get()
				 	->result();
	}

	public function getProductInfo( $product_id )
	{
		// return $this->db->select('p.product_id, p.title, p.mrp, i.img_name')
		// 		 		->from('products as p')
		// 		 		->join('product_image AS i', 'i.product_id = p.product_id')
		// 		 		->where('p.product_id', $product_id)
		// 		 		->group_by('p.product_id')
		// 		 		->row();
		return $this->db->query("SELECT p.product_id,  p.title, p.mrp, i.img_name FROM products as p JOIN `product_image` AS i ON i.product_id = p.product_id WHERE p.product_id = $product_id group by p.product_id")->row();

	}

	public function del_wishlist( $product_id, $customer_id )
	{
		return $this->db->where(['product_id' => $product_id, 'user_id' => $customer_id ])
				 		->delete('wishlist');
	}
	public function update_product_qty( $product_id, $qty )
	{
		$pro_qry = $this->db->select('in_stock, total_sales')
					->from('products')
					->where(['product_id' => $product_id] )
					->get()
					->row();
		$updateQty = $pro_qry->in_stock - $qty;
		$updateSale = $pro_qry->total_sales + $qty;

		return $this->db->where(['product_id'=> $product_id])
						->update('products', ['in_stock' => $updateQty, 'total_sales' => $updateSale]);
	}

}


 ?>