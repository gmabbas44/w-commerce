<?php 

class Shop_model extends CI_Model
{
	
	public function gelAllProduct()
	{
		return $this->db->query("SELECT p.product_id, c.category_name, p.title, p.cost, p.mrp, p.price, i.img_name, p.cat_id FROM products as p JOIN `product_image` AS i ON i.product_id = p.product_id JOIN categories AS c ON c.cat_id = p.cat_id  group by p.product_id")->result();
	}
	public function gelProduct( $coll_id, $brand_id ) {
		return $this->db->select('p.product_id, c.category_name, p.title, p.cost, p.mrp, p.price, i.img_name, p.cat_id')
				 		->from('products as p')
				 		->join('product_image AS i', 'i.product_id = p.product_id')
				 		->join('categories AS c', 'c.cat_id = p.cat_id')
				 		->where(['coll_id' => $coll_id, 'brand_id' => $brand_id])
				 		->group_by('p.product_id')
				 		->result();
	}
	
}




