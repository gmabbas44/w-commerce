<?php

class AdminDepartment_model extends CI_Model
{
  public function getAll()
  {
    return $this->db->get('department')->result();
  }
  public function add($data)
  {
    return $this->db->insert('department', $data);
  }
  public function getUpdateData($id)
  {
    $this->db->where('dep_id', $id);
    return $this->db->get('department')->row();
  }
  public function update($id, $data)
  {
    $this->db->where('dep_id', $id);
    return $this->db->update('department',$data);
  }
}