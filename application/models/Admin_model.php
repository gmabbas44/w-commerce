<?php


class Admin_model extends CI_Model
{
	
	public function admin_login( $email, $password )
	{
		$this->db->select( ' * ' );
		$this->db->from('admin');
		$this->db->where('email', $email);
		$this->db->where('password', $password);
		return $this->db->get()->row();
	}

	public function get_all( $table )
	{
		// $this->db->where('status', $status);
		return $this->db->get($table)->result();
	}
}