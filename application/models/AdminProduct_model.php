<?php

class AdminProduct_model extends CI_Model
{
	public function all($table_name)
	{
		return $this->db->get($table_name)
			->result();
	}
	public function allProduct()
	{
		return $this->all('products');
	}
	public function add_product($data)
	{
		$this->db->insert('products', $data);

		return $this->db->insert_id();
	}
	public function gellery_image($data)
	{
		$this->db->insert('product_image', $data);
	}
	public function getProductById($id)
	{
		$this->db->where('product_id', $id);
		return $this->db->get('products')->row();
	}

	public function updateProduct($id, $data)
    {
        $this->db->where('product_id',$id);
        $this->db->update('products', $data);
    }

}
