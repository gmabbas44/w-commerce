<?php

class AdminBrand_model extends CI_Model
{
  public function getAll()
  {
    return $this->db->get('brand')->result();
  }

  public function add($data)
  {
    return $this->db->insert('brand', $data);
  }
  public function getUpdateData($id)
  {
    $this->db->where('brand_id', $id);
    return $this->db->get('brand')->row();
  }
  public function update($id, $data)
  {
    $this->db->where('brand_id', $id);
    return $this->db->update('brand',$data);
  }

}