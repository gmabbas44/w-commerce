<?php


class Home_model extends CI_Model
{
	public function getWomenProducts()
	{
		return $this->db->query("SELECT p.product_id, p.cat_id, p.title, c.category_name, p.cost, p.mrp, p.price, i.img_name FROM products as p JOIN `product_image` AS i ON i.product_id = p.product_id JOIN categories AS c ON c.cat_id = p.cat_id WHERE p.coll_id = 2 AND i.is_active = 1 group by p.product_id")->result();
	}

	public function getMenProducts()
	{
		return $this->db->query("SELECT p.product_id, p.cat_id,  p.title, c.category_name, p.cost, p.mrp, p.price, i.img_name FROM products as p JOIN `product_image` AS i ON i.product_id = p.product_id JOIN categories AS c ON c.cat_id = p.cat_id WHERE p.coll_id = 1 AND i.is_active = 1 AND p.product_type = 1 group by p.product_id")->result();
	}

	public function getCategories($collection_type)
	{
		return $this->db->select('category_name')
						->from('categories')
						->where(['status'=> 1, 'collection_type' => 0])
						->or_where(['collection_type' => $collection_type])
						->get()
						->result();
	}

	public function bestSeller()
	{
		return $this->db->query("SELECT p.product_id, p.cat_id,  p.title, c.category_name, p.cost, p.mrp, p.price, i.img_name FROM products as p JOIN `product_image` AS i ON i.product_id = p.product_id JOIN categories AS c ON c.cat_id = p.cat_id WHERE i.is_active = 1 AND total_sales >= 10 AND product_type != 3 group by p.product_id")->result();
	}

	public function coming_soon_product()
	{
		return $this->db->query("SELECT p.product_id, p.short_description, c.category_name, p.cost, p.mrp, p.coming_soon_time, i.img_name FROM products as p JOIN `product_image` AS i ON i.product_id = p.product_id JOIN categories AS c ON c.cat_id = p.cat_id WHERE p.product_type = 3 group by p.product_id limit 1")->row();
	}

	public function update_coming_soon( $product_id )
	{
		return $this->db->where(['product_id' => $product_id])
					->update('products',['product_type' => 1, 'coming_soon_time' => '']);
	}



}

?>