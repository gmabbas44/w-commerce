<?php

class HF_model extends CI_Model
{
	public function getCollection()
	{
		return $this->get('collection');
	}

	public function getDepartment()
	{
		return $this->get('department');
	}

	public function getBrand()
	{
		return $this->get('brand');
	}
	
	private function get( $table ) {
		return $this->db->where('status', '1')
						->get( $table )
						->result();
	}
	
}