<?php

// for assets
define('ASSETS', '/w-commerce/assets/');

function dd($data)
{
	echo "<pre>";
	var_dump($data);
	echo "</pre>";
	die;
}

function encript_id($id)
{
	return base64_encode($id);
}

function decript_id($id)
{
	return base64_decode($id);
}

function discount( $price, $mrp )
{
    $discount_price = $price - $mrp;

    $dis =  ( 100 * $discount_price ) / $price;

    return ceil($dis);
}

function data_format( $data )
{
	return date('M d, Y',strtotime( $data ));
}

function strReplace( $data )
{
	return str_replace(' ', '_', $data);
}

function product_type( $value )
{
	$values = ['0' => 'Reguler', '1' => 'New', '2' => 'Feature', '3'=> 'Coming Soom'];

	foreach ($values as $k => $v ) {
		if ( $k == $value) {
			return $v;
		}
	}
}

function status( $value )
{
	if ($value == 0) {
		return '<span class="text-danger">No</span>';
	}else {
		return '<span class="text-success">Yes</span>';
	}
}

?>