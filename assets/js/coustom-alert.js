/*------------------
    coutom alert
--------------------*/
function popup(status, msg) {

    if( status == 'success' )
    {
        $('.i').html('<i class="fa fa-check-circle" aria-hidden="true"></i>');
        pop( status, msg);
    } else if ( status == 'warning' ) 
    {
        $('.i').html('<i class="fa fa-exclamation-circle" aria-hidden="true"></i>');
        pop(status, msg);
    }
    $('.close-btn').click(function(){
        $('.alert-coustom').addClass('hide').removeClass('show').removeClass('showAlert').delay(5000).fadeOut("slow");
    });
    function pop(status, msg){
        $('.msg').text(msg);
        $('.alert-coustom').addClass(status).addClass('showAlert').addClass('show').removeClass('hide');
        setTimeout(function(){
            $('.alert-coustom').addClass('hide').removeClass('show').removeClass('showAlert').removeClass(status);
        },5000);
    }

}